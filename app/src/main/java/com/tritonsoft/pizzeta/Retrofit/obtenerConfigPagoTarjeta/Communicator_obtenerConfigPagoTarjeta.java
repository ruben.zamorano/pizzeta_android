package com.tritonsoft.pizzeta.Retrofit.obtenerConfigPagoTarjeta;


import android.support.annotation.NonNull;
import android.util.Log;

import com.tritonsoft.pizzeta.Retrofit.obtenerConfigPagoTarjeta.Events.ErrorEvent;
import com.tritonsoft.pizzeta.Retrofit.obtenerConfigPagoTarjeta.Events.Event_obtenerConfigPagoTarjeta;
import com.tritonsoft.pizzeta.Retrofit.obtenerConfigPagoTarjeta.Response.Response_obtenerConfigPagoTarjeta;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * ruben zamorano
 */

public class Communicator_obtenerConfigPagoTarjeta {
    private static final String TAG = "CommunicatorBusqueda";
    private static final String SERVER_URL = "http://www.pizzeta.com.mx";
    private static final String url_path = "/WSPizzeta/api/obtenerConfigPagoTarjeta";

    public void Communicator_obtenerConfigPagoTarjeta(int importe, final Callback_obtenerConfigPagoTarjeta callback) {


        Log.d("obtenerConfigPago","constructor");
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true);

        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(SERVER_URL)
                .build();


        Interface_obtenerConfigPagoTarjeta service = retrofit.create(Interface_obtenerConfigPagoTarjeta.class);

        Call<Response_obtenerConfigPagoTarjeta> call = service.get(url_path + "/" +  importe);

        call.enqueue(new Callback<Response_obtenerConfigPagoTarjeta>() {
            @Override
            public void onResponse(@NonNull Call<Response_obtenerConfigPagoTarjeta> call, @NonNull Response<Response_obtenerConfigPagoTarjeta> response) {
                BusProvider.getInstance().post(new Event_obtenerConfigPagoTarjeta(response.body(), callback,0));
            }

            @Override
            public void onFailure(@NonNull Call<Response_obtenerConfigPagoTarjeta> call, @NonNull Throwable t) {
                String message = "Error";
                if (t.getMessage() != null)
                    message = t.getMessage();
                BusProvider.getInstance().post(new ErrorEvent(-2, message,callback));
            }
        });
    }
}
