package com.tritonsoft.pizzeta.Retrofit.GetDetallePizza.Response;

/**
 * Created by rubenzamorano on 15/07/17.
 */

public class Response_GetDetallePizza {

    public com.tritonsoft.pizzeta.Retrofit.GetDetallePizza.Response.data data;
    public com.tritonsoft.pizzeta.Retrofit.GetDetallePizza.Response.meta meta;

    public Response_GetDetallePizza(com.tritonsoft.pizzeta.Retrofit.GetDetallePizza.Response.data data, com.tritonsoft.pizzeta.Retrofit.GetDetallePizza.Response.meta meta) {
        this.data = data;
        this.meta = meta;
    }

    public com.tritonsoft.pizzeta.Retrofit.GetDetallePizza.Response.data getData() {
        return data;
    }

    public void setData(com.tritonsoft.pizzeta.Retrofit.GetDetallePizza.Response.data data) {
        this.data = data;
    }

    public com.tritonsoft.pizzeta.Retrofit.GetDetallePizza.Response.meta getMeta() {
        return meta;
    }

    public void setMeta(com.tritonsoft.pizzeta.Retrofit.GetDetallePizza.Response.meta meta) {
        this.meta = meta;
    }
}










