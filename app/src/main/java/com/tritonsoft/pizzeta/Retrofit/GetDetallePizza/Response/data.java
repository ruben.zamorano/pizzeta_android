package com.tritonsoft.pizzeta.Retrofit.GetDetallePizza.Response;

import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.producto;

import java.util.List;

/**
 * Created by rubenzamorano on 06/03/18.
 */

public class data
{
    public String idu_tipoproducto;
    public String des_tipoproducto;
    public List<producto> productos;

    public data(String idu_tipoproducto, String des_tipoproducto, List<producto> productos) {
        this.idu_tipoproducto = idu_tipoproducto;
        this.des_tipoproducto = des_tipoproducto;
        this.productos = productos;
    }

    public String getIdu_tipoproducto() {
        return idu_tipoproducto;
    }

    public void setIdu_tipoproducto(String idu_tipoproducto) {
        this.idu_tipoproducto = idu_tipoproducto;
    }

    public String getDes_tipoproducto() {
        return des_tipoproducto;
    }

    public void setDes_tipoproducto(String des_tipoproducto) {
        this.des_tipoproducto = des_tipoproducto;
    }

    public List<producto> getProductos() {
        return productos;
    }

    public void setProductos(List<producto> productos) {
        this.productos = productos;
    }
}

//        "idu_tipoproducto": "1",
//        "des_tipoproducto": "PIZZAS",