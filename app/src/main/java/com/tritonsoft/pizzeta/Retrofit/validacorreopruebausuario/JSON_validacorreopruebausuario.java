package com.tritonsoft.pizzeta.Retrofit.validacorreopruebausuario;

public class JSON_validacorreopruebausuario {
    public String des_correo;
    public int idu_origenpedido;

    public JSON_validacorreopruebausuario(String des_correo) {
        this.des_correo = des_correo;
        this.idu_origenpedido = 2;
    }

    public String getDes_correo() {
        return des_correo;
    }

    public void setDes_correo(String des_correo) {
        this.des_correo = des_correo;
    }

    public int getIdu_origenpedido() {
        return idu_origenpedido;
    }

    public void setIdu_origenpedido(int idu_origenpedido) {
        this.idu_origenpedido = idu_origenpedido;
    }
}


//{"des_correo": "josecarlos_182_6@hotmail.com", "idu_origenpedido": 1}