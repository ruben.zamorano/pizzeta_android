package com.tritonsoft.pizzeta.Retrofit.GetSucursales.Response;

import java.util.List;

public class data {
    List<marker> response;

    public data(List<marker> response) {
        this.response = response;
    }

    public List<marker> getResponse() {
        return response;
    }

    public void setResponse(List<marker> response) {
        this.response = response;
    }
}
