package com.tritonsoft.pizzeta.Retrofit.DetallePedido.Events;


import com.tritonsoft.pizzeta.Retrofit.DetallePedido.Callback_DetallePedido;
import com.tritonsoft.pizzeta.Retrofit.DetallePedido.Response.Response_DetallePedido;

/**
 * Created by ruben on 12/02/17.
 */

public class Event_DetallePedido {

    private Response_DetallePedido serverResponse;

    public Event_DetallePedido(Response_DetallePedido serverResponse, final Callback_DetallePedido callback) {
        this.serverResponse = serverResponse;
        callback.onSuccess_DetallePedido(serverResponse);
    }

    public Response_DetallePedido getServerResponse() {
        return serverResponse;
    }

    public void setServerResponse( Response_DetallePedido serverResponse) {
        this.serverResponse = serverResponse;
    }


}
