package com.tritonsoft.pizzeta.Retrofit.validacorreopruebausuario.Response;

public class response {
    public boolean opc_descuento;
    public boolean opc_puedecomprar;
    public String des_mensaje;
    public String des_correo;
    public int pje_descuento;
    public int num_limiteproductos;

    public response(boolean opc_descuento, boolean opc_puedecomprar, String des_mensaje, String des_correo, int pje_descuento, int num_limiteproductos) {
        this.opc_descuento = opc_descuento;
        this.opc_puedecomprar = opc_puedecomprar;
        this.des_mensaje = des_mensaje;
        this.des_correo = des_correo;
        this.pje_descuento = pje_descuento;
        this.num_limiteproductos = num_limiteproductos;
    }

    public boolean isOpc_descuento() {
        return opc_descuento;
    }

    public void setOpc_descuento(boolean opc_descuento) {
        this.opc_descuento = opc_descuento;
    }

    public boolean isOpc_puedecomprar() {
        return opc_puedecomprar;
    }

    public void setOpc_puedecomprar(boolean opc_puedecomprar) {
        this.opc_puedecomprar = opc_puedecomprar;
    }

    public String getDes_mensaje() {
        return des_mensaje;
    }

    public void setDes_mensaje(String des_mensaje) {
        this.des_mensaje = des_mensaje;
    }

    public String getDes_correo() {
        return des_correo;
    }

    public void setDes_correo(String des_correo) {
        this.des_correo = des_correo;
    }

    public int getPje_descuento() {
        return pje_descuento;
    }

    public void setPje_descuento(int pje_descuento) {
        this.pje_descuento = pje_descuento;
    }

    public int getNum_limiteproductos() {
        return num_limiteproductos;
    }

    public void setNum_limiteproductos(int num_limiteproductos) {
        this.num_limiteproductos = num_limiteproductos;
    }
}


//        "opc_puedecomprar": true,                                // VALIDA QUE EL CLIENTE ES DE LOS 60 CANDIDATOS Y PUEDE COMPRAR
//        "opc_descuento": true,                                   // VALIDA QUE SE LE APLICARA EL DECUENTO POR LA PRUEBA
//        "des_correo": "josecarlos_182_6@hotmail.com",            // CORREO DEL CLIENTE
//        "pje_descuento": 60,                                     // PORCENTAJE DEL DESCUENTO QUE SE LE APLICARRA
//        "num_limiteproductos": 5,                                // LIMITE DE PRODUCTOS PARA EL PEDIDO
//        "des_mensaje": "Aplicar descuento por prueba de usuario" // MENSAJE DE RESPUEST