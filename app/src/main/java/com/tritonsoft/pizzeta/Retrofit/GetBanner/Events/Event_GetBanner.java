package com.tritonsoft.pizzeta.Retrofit.GetBanner.Events;


import com.tritonsoft.pizzeta.Retrofit.GetBanner.Callback_GetBanner;
import com.tritonsoft.pizzeta.Retrofit.GetBanner.Response.Response_GetBanner;

/**
 * Created by ruben on 12/02/17.
 */

public class Event_GetBanner {

    private Response_GetBanner serverResponse;

    public Event_GetBanner(Response_GetBanner serverResponse, final Callback_GetBanner callback, int nTipoProducto) {
        this.serverResponse = serverResponse;
        callback.onSuccess_GetBanner(serverResponse,nTipoProducto);
    }

    public Response_GetBanner getServerResponse() {
        return serverResponse;
    }

    public void setServerResponse(Response_GetBanner serverResponse) {
        this.serverResponse = serverResponse;
    }


}
