package com.tritonsoft.pizzeta.Retrofit.addCard.Response;

/**
 * Created by rubenzamorano on 15/07/17.
 */

public class Response_addCard {
    public String id;
    public String type;
    public String brand;
    public String address;
    public String card_number;
    public String holder_name;
    public String expiration_year;
    public String expiration_month;
    public boolean allows_charges;
    public boolean allows_payouts;
    public String creation_date;
    public String bank_name;
    public String customer_id;
    public String bank_code;

    public Response_addCard(String id, String type, String brand, String address, String card_number, String holder_name, String expiration_year, String expiration_month, boolean allows_charges, boolean allows_payouts, String creation_date, String bank_name, String customer_id, String bank_code) {
        this.id = id;
        this.type = type;
        this.brand = brand;
        this.address = address;
        this.card_number = card_number;
        this.holder_name = holder_name;
        this.expiration_year = expiration_year;
        this.expiration_month = expiration_month;
        this.allows_charges = allows_charges;
        this.allows_payouts = allows_payouts;
        this.creation_date = creation_date;
        this.bank_name = bank_name;
        this.customer_id = customer_id;
        this.bank_code = bank_code;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCard_number() {
        return card_number;
    }

    public void setCard_number(String card_number) {
        this.card_number = card_number;
    }

    public String getHolder_name() {
        return holder_name;
    }

    public void setHolder_name(String holder_name) {
        this.holder_name = holder_name;
    }

    public String getExpiration_year() {
        return expiration_year;
    }

    public void setExpiration_year(String expiration_year) {
        this.expiration_year = expiration_year;
    }

    public String getExpiration_month() {
        return expiration_month;
    }

    public void setExpiration_month(String expiration_month) {
        this.expiration_month = expiration_month;
    }

    public boolean isAllows_charges() {
        return allows_charges;
    }

    public void setAllows_charges(boolean allows_charges) {
        this.allows_charges = allows_charges;
    }

    public boolean isAllows_payouts() {
        return allows_payouts;
    }

    public void setAllows_payouts(boolean allows_payouts) {
        this.allows_payouts = allows_payouts;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getBank_code() {
        return bank_code;
    }

    public void setBank_code(String bank_code) {
        this.bank_code = bank_code;
    }
}


//
//{       id: 'kvmpci5agd9ftyewrzci',
//        type: 'debit',
//        brand: 'visa',
//        address: null,
//        card_number: '411111XXXXXX1111',
//        holder_name: 'Juan Perez Ramirez',
//        expiration_year: '20',
//        expiration_month: '12',
//        allows_charges: true,
//        allows_payouts: true,
//        creation_date: '2019-03-15T21:01:08-06:00',
//        bank_name: 'Banamex',
//        customer_id: 'aeq6t6rso5yzlqvbvely',
//        bank_code: '002' }










