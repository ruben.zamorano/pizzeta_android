package com.tritonsoft.pizzeta.Retrofit.GetBanner.Response;

import java.util.List;

/**
 * Created by rubenzamorano on 06/03/18.
 */

public class data
{
    public List<toda> todas;
    public List<toda> vigentes;
    public List<toda> inactivas;

    public data(List<toda> todas, List<toda> vigentes, List<toda> inactivas) {
        this.todas = todas;
        this.vigentes = vigentes;
        this.inactivas = inactivas;
    }

    public List<toda> getTodas() {
        return todas;
    }

    public void setTodas(List<toda> todas) {
        this.todas = todas;
    }

    public List<toda> getVigentes() {
        return vigentes;
    }

    public void setVigentes(List<toda> vigentes) {
        this.vigentes = vigentes;
    }

    public List<toda> getInactivas() {
        return inactivas;
    }

    public void setInactivas(List<toda> inactivas) {
        this.inactivas = inactivas;
    }
}

//        "idu_tipoproducto": "1",
//        "des_tipoproducto": "PIZZAS",