package com.tritonsoft.pizzeta.Retrofit.GetPizzas;


import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.Response_GetPizzas;

/**
 * Created by ruben on 02/02/17.
 */

public interface Callback_GetPizzas {

    void onSuccess_GetPizzas(Response_GetPizzas result, int nTipoProducto);

    void onFail_GetPizzas(String result);
}
