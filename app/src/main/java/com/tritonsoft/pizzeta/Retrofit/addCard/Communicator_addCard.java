package com.tritonsoft.pizzeta.Retrofit.addCard;


import android.support.annotation.NonNull;

import com.tritonsoft.pizzeta.Helpers.AddHeaderInterceptor;
import com.tritonsoft.pizzeta.Helpers.BasicAddHeaderInterceptor;
import com.tritonsoft.pizzeta.Retrofit.addCard.Events.ErrorEvent;
import com.tritonsoft.pizzeta.Retrofit.addCard.Events.Event_addCard;
import com.tritonsoft.pizzeta.Retrofit.addCard.Response.JSON_addCard;
import com.tritonsoft.pizzeta.Retrofit.addCard.Response.Response_addCard;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * ruben zamorano
 */

public class Communicator_addCard {
    private static final String TAG = "CommunicatorBusqueda";
    private static final String SERVER_URL = "https://sandbox-api.openpay.mx";
    private static final String url_path = "/v1/m2hjgoktenbixd5mn6fp/customers/{nIdCliente}/cards";

    public void addCardServer(String nIdCliente, JSON_addCard body, final Callback_addCard callback) {


        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true);

        httpClient.addInterceptor(logging);



        String varToken = "sk_7458377847814288b05f380cb5ff6265";
        BasicAddHeaderInterceptor authorization = new BasicAddHeaderInterceptor(varToken);
        httpClient.addNetworkInterceptor(authorization);

        Retrofit retrofit = new Retrofit.Builder()
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(SERVER_URL)
                .build();


        Interface_addCard service = retrofit.create(Interface_addCard.class);

        Call<Response_addCard> call = service.post(SERVER_URL +url_path.replace("{nIdCliente}",nIdCliente), body);

        call.enqueue(new Callback<Response_addCard>() {
            @Override
            public void onResponse(@NonNull Call<Response_addCard> call, @NonNull Response<Response_addCard> response) {
                BusProvider.getInstance().post(new Event_addCard(response.body(), callback));
            }

            @Override
            public void onFailure(@NonNull Call<Response_addCard> call, @NonNull Throwable t) {
                String message = "Error";
                if (t.getMessage() != null)
                    message = t.getMessage();
                BusProvider.getInstance().post(new ErrorEvent(-2, message,callback));
            }
        });
    }
}
