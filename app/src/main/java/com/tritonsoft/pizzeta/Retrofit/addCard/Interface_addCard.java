package com.tritonsoft.pizzeta.Retrofit.addCard;


import com.tritonsoft.pizzeta.Retrofit.addCard.Response.JSON_addCard;
import com.tritonsoft.pizzeta.Retrofit.addCard.Response.Response_addCard;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Url;

/**
 * Created by Ruben on 12/28/2017.
 */

public interface Interface_addCard {


    @POST
    Call<Response_addCard> post(@Url String url, @Body JSON_addCard body);
}