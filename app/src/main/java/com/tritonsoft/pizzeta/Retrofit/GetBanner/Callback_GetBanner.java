package com.tritonsoft.pizzeta.Retrofit.GetBanner;


import com.tritonsoft.pizzeta.Retrofit.GetBanner.Response.Response_GetBanner;

/**
 * Created by ruben on 02/02/17.
 */

public interface Callback_GetBanner {

    void onSuccess_GetBanner(Response_GetBanner result, int nTipoProducto);

    void onFail_GetBanner(String result);
}
