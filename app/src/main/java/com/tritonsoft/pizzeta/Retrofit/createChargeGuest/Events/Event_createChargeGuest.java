package com.tritonsoft.pizzeta.Retrofit.createChargeGuest.Events;


import com.tritonsoft.pizzeta.Retrofit.createChargeGuest.Callback_createChargeGuest;
import com.tritonsoft.pizzeta.Retrofit.createChargeGuest.Response.Response_createChargeGuest;

/**
 * Created by ruben on 12/02/17.
 */

public class Event_createChargeGuest {

    private Response_createChargeGuest serverResponse;

    public Event_createChargeGuest(Response_createChargeGuest serverResponse, final Callback_createChargeGuest callback) {
        this.serverResponse = serverResponse;
        callback.onSuccess_createChargeGuest(serverResponse);
    }

    public Response_createChargeGuest getServerResponse() {
        return serverResponse;
    }

    public void setServerResponse( Response_createChargeGuest serverResponse) {
        this.serverResponse = serverResponse;
    }


}
