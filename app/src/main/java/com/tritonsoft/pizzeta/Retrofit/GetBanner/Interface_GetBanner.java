package com.tritonsoft.pizzeta.Retrofit.GetBanner;


import com.tritonsoft.pizzeta.Retrofit.GetBanner.Response.Response_GetBanner;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by Ruben on 12/28/2017.
 */

public interface Interface_GetBanner {


    @GET
//    @Aut
    Call<Response_GetBanner> get(@Url String url);
}