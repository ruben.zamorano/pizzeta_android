package com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response;

public class JSON_GetPizzas {
    public int idu_tipoproducto;
    public int idu_sucursal;

    public JSON_GetPizzas(int idu_tipoproducto) {
        this.idu_tipoproducto = idu_tipoproducto;
    }

    public JSON_GetPizzas(int idu_tipoproducto, int idu_sucursal) {
        this.idu_tipoproducto = idu_tipoproducto;
        this.idu_sucursal = idu_sucursal;
    }

    public int getIdu_tipoproducto() {
        return idu_tipoproducto;
    }

    public void setIdu_tipoproducto(int idu_tipoproducto) {
        this.idu_tipoproducto = idu_tipoproducto;
    }

    public int getIdu_sucursal() {
        return idu_sucursal;
    }

    public void setIdu_sucursal(int idu_sucursal) {
        this.idu_sucursal = idu_sucursal;
    }
}


//{
//        "idu_tipoproducto": 1,
//        "idu_sucursal": null
//        }
