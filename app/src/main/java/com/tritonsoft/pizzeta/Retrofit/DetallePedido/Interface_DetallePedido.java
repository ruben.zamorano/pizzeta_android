package com.tritonsoft.pizzeta.Retrofit.DetallePedido;


import com.tritonsoft.pizzeta.Retrofit.DetallePedido.Response.Response_DetallePedido;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by Ruben on 12/28/2017.
 */

public interface Interface_DetallePedido {


    @GET
    Call<Response_DetallePedido> get(@Url String url);
}