package com.tritonsoft.pizzeta.Retrofit.obtenerConfigPagoTarjeta;


import com.tritonsoft.pizzeta.Retrofit.obtenerConfigPagoTarjeta.Response.Response_obtenerConfigPagoTarjeta;

/**
 * Created by ruben on 02/02/17.
 */

public interface Callback_obtenerConfigPagoTarjeta {

    void onSuccess_obtenerConfigPagoTarjeta(Response_obtenerConfigPagoTarjeta result, int nTipoProducto);

    void onFail_obtenerConfigPagoTarjeta(String result);
}
