package com.tritonsoft.pizzeta.Retrofit.createChargeGuest;

public class JSON_createChargeGuest {
    public String source_id;
    public String method;
    public int amount;
    public String currency;
    public String description;
    public String order_id;
    public String device_session_id;
    public customer customer;

    public JSON_createChargeGuest(String source_id, String method, int amount, String currency, String description, String order_id, String device_session_id, com.tritonsoft.pizzeta.Retrofit.createChargeGuest.customer customer) {
        this.source_id = source_id;
        this.method = method;
        this.amount = amount;
        this.currency = currency;
        this.description = description;
        this.order_id = order_id;
        this.device_session_id = device_session_id;
        this.customer = customer;
    }

    public String getSource_id() {
        return source_id;
    }

    public void setSource_id(String source_id) {
        this.source_id = source_id;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getDevice_session_id() {
        return device_session_id;
    }

    public void setDevice_session_id(String device_session_id) {
        this.device_session_id = device_session_id;
    }

    public com.tritonsoft.pizzeta.Retrofit.createChargeGuest.customer getCustomer() {
        return customer;
    }

    public void setCustomer(com.tritonsoft.pizzeta.Retrofit.createChargeGuest.customer customer) {
        this.customer = customer;
    }
}


//{
//        "source_id" : "kzymqdbaqtkyjlwmcxoe",
//        "method" : "card",
//        "amount" : 100,
//        "currency" : "MXN",
//        "description" : "Cargo inicial a mi cuenta",
//        "order_id" : "oid-0002241",
//        "device_session_id" : "kR1MiQhz2otdIuUlQkbEyitIqVMiI16f",
//        "customer" : {
//        "name" : "ruben",
//        "last_name" : "zamorano",
//        "phone_number" : "6672556612",
//        "email" : "ruben.zamorano@outlook.com"
//        }
//        }
