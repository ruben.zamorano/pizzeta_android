package com.tritonsoft.pizzeta.Retrofit.validacorreopruebausuario.Events;


import com.tritonsoft.pizzeta.Retrofit.validacorreopruebausuario.Callback_validacorreopruebausuario;
import com.tritonsoft.pizzeta.Retrofit.validacorreopruebausuario.Response.Response_validacorreopruebausuario;

/**
 * Created by ruben on 12/02/17.
 */

public class Event_validacorreopruebausuario {

    private Response_validacorreopruebausuario serverResponse;

    public Event_validacorreopruebausuario(Response_validacorreopruebausuario serverResponse, final Callback_validacorreopruebausuario callback) {
        this.serverResponse = serverResponse;
        callback.onSuccess_validacorreopruebausuario(serverResponse);
    }

    public Response_validacorreopruebausuario getServerResponse() {
        return serverResponse;
    }

    public void setServerResponse( Response_validacorreopruebausuario serverResponse) {
        this.serverResponse = serverResponse;
    }


}
