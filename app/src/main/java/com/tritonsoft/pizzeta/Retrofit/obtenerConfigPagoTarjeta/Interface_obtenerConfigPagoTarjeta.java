package com.tritonsoft.pizzeta.Retrofit.obtenerConfigPagoTarjeta;


import com.tritonsoft.pizzeta.Retrofit.obtenerConfigPagoTarjeta.Response.Response_obtenerConfigPagoTarjeta;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by Ruben on 12/28/2017.
 */

public interface Interface_obtenerConfigPagoTarjeta {


    @GET
//    @Aut
    Call<Response_obtenerConfigPagoTarjeta> get(@Url String url);
}