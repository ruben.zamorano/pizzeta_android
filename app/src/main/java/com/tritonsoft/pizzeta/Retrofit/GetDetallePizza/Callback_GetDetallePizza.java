package com.tritonsoft.pizzeta.Retrofit.GetDetallePizza;


import com.tritonsoft.pizzeta.Retrofit.GetDetallePizza.Response.Response_GetDetallePizza;

/**
 * Created by ruben on 02/02/17.
 */

public interface Callback_GetDetallePizza {

    void onSuccess_GetDetallePizza(Response_GetDetallePizza result, int nTipoProducto);

    void onFail_GetDetallePizza(String result);
}
