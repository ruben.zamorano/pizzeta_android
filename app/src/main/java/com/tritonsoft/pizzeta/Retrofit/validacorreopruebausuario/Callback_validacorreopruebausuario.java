package com.tritonsoft.pizzeta.Retrofit.validacorreopruebausuario;


import com.tritonsoft.pizzeta.Retrofit.validacorreopruebausuario.Response.Response_validacorreopruebausuario;

/**
 * Created by ruben on 02/02/17.
 */

public interface Callback_validacorreopruebausuario {

    void onSuccess_validacorreopruebausuario(Response_validacorreopruebausuario result);

    void onFail_validacorreopruebausuario(String result);
}
