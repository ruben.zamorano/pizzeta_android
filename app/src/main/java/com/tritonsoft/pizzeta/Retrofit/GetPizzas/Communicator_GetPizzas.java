package com.tritonsoft.pizzeta.Retrofit.GetPizzas;


import android.support.annotation.NonNull;


import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Events.ErrorEvent;
import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Events.Event_GetPizzas;
import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.JSON_GetPizzas;
import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.Response_GetPizzas;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * ruben zamorano
 */

public class Communicator_GetPizzas {
    private static final String TAG = "CommunicatorBusqueda";
    private static final String SERVER_URL = "http://www.pizzeta.com.mx";
    private static final String url_path = "/WSPizzeta/api/obtenerMenuPorTipo/";

    public void BuscarProductos(int nTipoProducto, final Callback_GetPizzas callback) {


        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true);

        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(SERVER_URL)
                .build();


        Interface_GetPizzas service = retrofit.create(Interface_GetPizzas.class);

//        Call<Response_GetPizzas> call = service.get(url_path + nTipoProducto);
        JSON_GetPizzas body = new JSON_GetPizzas(nTipoProducto);
        Call<Response_GetPizzas> call = service.post(body);

        call.enqueue(new Callback<Response_GetPizzas>() {
            @Override
            public void onResponse(@NonNull Call<Response_GetPizzas> call, @NonNull Response<Response_GetPizzas> response) {
                BusProvider.getInstance().post(new Event_GetPizzas(response.body(), callback,nTipoProducto));
            }

            @Override
            public void onFailure(@NonNull Call<Response_GetPizzas> call, @NonNull Throwable t) {
                String message = "Error";
                if (t.getMessage() != null)
                    message = t.getMessage();
                BusProvider.getInstance().post(new ErrorEvent(-2, message,callback));
            }
        });
    }
}
