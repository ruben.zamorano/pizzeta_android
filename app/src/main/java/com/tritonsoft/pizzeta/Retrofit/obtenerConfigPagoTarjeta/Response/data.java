package com.tritonsoft.pizzeta.Retrofit.obtenerConfigPagoTarjeta.Response;

import java.util.List;

/**
 * Created by rubenzamorano on 06/03/18.
 */

public class data
{
    public String Ds_Merchant_Order;

    public data(String ds_Merchant_Order) {
        Ds_Merchant_Order = ds_Merchant_Order;
    }

    public String getDs_Merchant_Order() {
        return Ds_Merchant_Order;
    }

    public void setDs_Merchant_Order(String ds_Merchant_Order) {
        Ds_Merchant_Order = ds_Merchant_Order;
    }
}

//{
//        "des_urlbanco": "https://ecom.eglobal.com.mx/VPBridgeWeb/servlets/TransactionStartBridge",
//        "Ds_Merchant_Amount": "12300",
//        "Ds_Merchant_Currency": "484",
//        "Ds_Merchant_ConsumerLanguage": "SP",
//        "Ds_Merchant_Order": "000000000505",
//        "Ds_Merchant_ProductDescription": "Consumo Alimentos",
//        "Ds_Merchant_MerchantCode": "4163565",
//        "Ds_Merchant_MerchantName": "Grupo Pizzeta SA de CV",
//        "Ds_Merchant_Terminal": "01",
//        "Ds_Merchant_TransactionType": "0",
//        "Ds_Merchant_MerchantURL": "https://www.pizzeta.com.mx/WSPizzeta/api/guardarRespuestaTransaccionBanco",
//        "Ds_Merchant_UrlOK": "https://new.pizzeta.com.mx/pedidos/confirmar-datos/pago-tarjeta/aceptado/000000000505",
//        "Ds_Merchant_UrlKO": "https://new.pizzeta.com.mx/pedidos/confirmar-datos/pago-tarjeta/declinado/000000000505",
//        "Ds_Merchant_MerchantSignature": "af49225992737a778769fb8552eb788007c2ed99",
//        "Ds_Merchant_MerchantData": "Orden: 000000000505 Importe: 123"
//        }
