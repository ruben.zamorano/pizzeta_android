package com.tritonsoft.pizzeta.Retrofit.guardarPedidoEnLinea;


import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tritonsoft.pizzeta.DTO.DTO_Carrito;
import com.tritonsoft.pizzeta.Retrofit.guardarPedidoEnLinea.Events.ErrorEvent;
import com.tritonsoft.pizzeta.Retrofit.guardarPedidoEnLinea.Events.Event_guardarPedidoEnLinea;
import com.tritonsoft.pizzeta.Retrofit.guardarPedidoEnLinea.Response.Response_guardarPedidoEnLinea;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * ruben zamorano
 */

public class Communicator_guardarPedidoEnLinea {
    private static final String TAG = "CommunicatorBusqueda";
    private static final String SERVER_URL = "http://www.pizzeta.com.mx/";

    public void GuardarPedido(DTO_Carrito body, final Callback_guardarPedidoEnLinea callback) {


        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true);

        httpClient.addInterceptor(logging);

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
//                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(SERVER_URL)
                .build();


        Interface_guardarPedidoEnLinea service = retrofit.create(Interface_guardarPedidoEnLinea.class);

        Call<Response_guardarPedidoEnLinea> call = service.post(body);

        call.enqueue(new Callback<Response_guardarPedidoEnLinea>() {
            @Override
            public void onResponse(@NonNull Call<Response_guardarPedidoEnLinea> call, @NonNull Response<Response_guardarPedidoEnLinea> response) {
                BusProvider.getInstance().post(new Event_guardarPedidoEnLinea(response.body(), callback));
            }

            @Override
            public void onFailure(@NonNull Call<Response_guardarPedidoEnLinea> call, @NonNull Throwable t) {
                String message = "Error";
                if (t.getMessage() != null)
                    message = t.getMessage();
                BusProvider.getInstance().post(new ErrorEvent(-2, message,callback));
            }
        });
    }
}
