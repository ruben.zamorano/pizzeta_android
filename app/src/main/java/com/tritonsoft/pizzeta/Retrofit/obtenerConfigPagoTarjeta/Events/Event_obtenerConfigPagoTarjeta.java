package com.tritonsoft.pizzeta.Retrofit.obtenerConfigPagoTarjeta.Events;


import com.tritonsoft.pizzeta.Retrofit.obtenerConfigPagoTarjeta.Callback_obtenerConfigPagoTarjeta;
import com.tritonsoft.pizzeta.Retrofit.obtenerConfigPagoTarjeta.Response.Response_obtenerConfigPagoTarjeta;

/**
 * Created by ruben on 12/02/17.
 */

public class Event_obtenerConfigPagoTarjeta {

    private Response_obtenerConfigPagoTarjeta serverResponse;

    public Event_obtenerConfigPagoTarjeta(Response_obtenerConfigPagoTarjeta serverResponse, final Callback_obtenerConfigPagoTarjeta callback, int nTipoProducto) {
        this.serverResponse = serverResponse;
        callback.onSuccess_obtenerConfigPagoTarjeta(serverResponse,nTipoProducto);
    }

    public Response_obtenerConfigPagoTarjeta getServerResponse() {
        return serverResponse;
    }

    public void setServerResponse(Response_obtenerConfigPagoTarjeta serverResponse) {
        this.serverResponse = serverResponse;
    }


}
