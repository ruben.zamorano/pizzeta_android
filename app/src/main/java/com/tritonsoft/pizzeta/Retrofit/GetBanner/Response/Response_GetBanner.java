package com.tritonsoft.pizzeta.Retrofit.GetBanner.Response;

/**
 * Created by rubenzamorano on 15/07/17.
 */

public class Response_GetBanner {

    public com.tritonsoft.pizzeta.Retrofit.GetBanner.Response.data data;
    public com.tritonsoft.pizzeta.Retrofit.GetBanner.Response.meta meta;

    public Response_GetBanner(com.tritonsoft.pizzeta.Retrofit.GetBanner.Response.data data, com.tritonsoft.pizzeta.Retrofit.GetBanner.Response.meta meta) {
        this.data = data;
        this.meta = meta;
    }

    public com.tritonsoft.pizzeta.Retrofit.GetBanner.Response.data getData() {
        return data;
    }

    public void setData(com.tritonsoft.pizzeta.Retrofit.GetBanner.Response.data data) {
        this.data = data;
    }

    public com.tritonsoft.pizzeta.Retrofit.GetBanner.Response.meta getMeta() {
        return meta;
    }

    public void setMeta(com.tritonsoft.pizzeta.Retrofit.GetBanner.Response.meta meta) {
        this.meta = meta;
    }
}










