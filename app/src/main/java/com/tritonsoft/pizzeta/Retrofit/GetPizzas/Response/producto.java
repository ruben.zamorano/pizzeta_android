package com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response;

import com.tritonsoft.pizzeta.Retrofit.GetDetallePizza.Response.ingrediente;

import java.util.List;

public class producto {
    public String idu_tipoproducto;
    public String idu_agrupador;
    public String nom_producto;
    public String des_producto;
    public String des_img;
    public String idu_categoria;
    public String nom_categoria;
    public List<tamano> tamanios;
    public List<ingrediente> ingredientes;

    public producto(String idu_tipoproducto, String idu_agrupador, String nom_producto, String des_producto, String des_img, String idu_categoria, String nom_categoria, List<tamano> tamanios, List<ingrediente> ingredientes) {
        this.idu_tipoproducto = idu_tipoproducto;
        this.idu_agrupador = idu_agrupador;
        this.nom_producto = nom_producto;
        this.des_producto = des_producto;
        this.des_img = des_img;
        this.idu_categoria = idu_categoria;
        this.nom_categoria = nom_categoria;
        this.tamanios = tamanios;
        this.ingredientes = ingredientes;
    }

    public String getIdu_tipoproducto() {
        return idu_tipoproducto;
    }

    public void setIdu_tipoproducto(String idu_tipoproducto) {
        this.idu_tipoproducto = idu_tipoproducto;
    }

    public String getIdu_agrupador() {
        return idu_agrupador;
    }

    public void setIdu_agrupador(String idu_agrupador) {
        this.idu_agrupador = idu_agrupador;
    }

    public String getNom_producto() {
        return nom_producto;
    }

    public void setNom_producto(String nom_producto) {
        this.nom_producto = nom_producto;
    }

    public String getDes_producto() {
        return des_producto;
    }

    public void setDes_producto(String des_producto) {
        this.des_producto = des_producto;
    }

    public String getDes_img() {
        return des_img;
    }

    public void setDes_img(String des_img) {
        this.des_img = des_img;
    }

    public String getIdu_categoria() {
        return idu_categoria;
    }

    public void setIdu_categoria(String idu_categoria) {
        this.idu_categoria = idu_categoria;
    }

    public String getNom_categoria() {
        return nom_categoria;
    }

    public void setNom_categoria(String nom_categoria) {
        this.nom_categoria = nom_categoria;
    }

    public List<tamano> getTamanios() {
        return tamanios;
    }

    public void setTamanios(List<tamano> tamanios) {
        this.tamanios = tamanios;
    }

    public List<ingrediente> getIngredientes() {
        return ingredientes;
    }

    public void setIngredientes(List<ingrediente> ingredientes) {
        this.ingredientes = ingredientes;
    }
}

//        "idu_tipoproducto": "1",
//        "idu_agrupador": "1",
//        "nom_producto": "Champiñón",
//        "des_producto": "",
//        "des_img": "img_1_1.jpg",
//        "idu_categoria": "1",
//        "nom_categoria": "1 ING",
//        "tamanos": []
