package com.tritonsoft.pizzeta.Retrofit.DetallePedido.Response;

import java.util.List;

/**
 * Created by rubenzamorano on 15/07/17.
 */
public class Response_DetallePedido {
    public com.tritonsoft.pizzeta.Retrofit.DetallePedido.Response.meta meta;
    public List<com.tritonsoft.pizzeta.Retrofit.DetallePedido.Response.data> data;

    public Response_DetallePedido(com.tritonsoft.pizzeta.Retrofit.DetallePedido.Response.meta meta, List<com.tritonsoft.pizzeta.Retrofit.DetallePedido.Response.data> data) {
        this.meta = meta;
        this.data = data;
    }

    public com.tritonsoft.pizzeta.Retrofit.DetallePedido.Response.meta getMeta() {
        return meta;
    }

    public void setMeta(com.tritonsoft.pizzeta.Retrofit.DetallePedido.Response.meta meta) {
        this.meta = meta;
    }

    public List<com.tritonsoft.pizzeta.Retrofit.DetallePedido.Response.data> getData() {
        return data;
    }

    public void setData(List<com.tritonsoft.pizzeta.Retrofit.DetallePedido.Response.data> data) {
        this.data = data;
    }
}










