package com.tritonsoft.pizzeta.Retrofit.GetDetallePizza.Events;


import com.tritonsoft.pizzeta.Retrofit.GetDetallePizza.Callback_GetDetallePizza;
import com.tritonsoft.pizzeta.Retrofit.GetDetallePizza.Response.Response_GetDetallePizza;

/**
 * Created by ruben on 12/02/17.
 */

public class Event_GetDetallePizza {

    private Response_GetDetallePizza serverResponse;

    public Event_GetDetallePizza(Response_GetDetallePizza serverResponse, final Callback_GetDetallePizza callback, int nTipoProducto) {
        this.serverResponse = serverResponse;
        callback.onSuccess_GetDetallePizza(serverResponse,nTipoProducto);
    }

    public Response_GetDetallePizza getServerResponse() {
        return serverResponse;
    }

    public void setServerResponse(Response_GetDetallePizza serverResponse) {
        this.serverResponse = serverResponse;
    }


}
