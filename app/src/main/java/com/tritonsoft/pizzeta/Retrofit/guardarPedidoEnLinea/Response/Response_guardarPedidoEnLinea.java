package com.tritonsoft.pizzeta.Retrofit.guardarPedidoEnLinea.Response;

/**
 * Created by rubenzamorano on 15/07/17.
 */
public class Response_guardarPedidoEnLinea {
    public com.tritonsoft.pizzeta.Retrofit.guardarPedidoEnLinea.Response.meta meta;
    public com.tritonsoft.pizzeta.Retrofit.guardarPedidoEnLinea.Response.data data;

    public Response_guardarPedidoEnLinea(com.tritonsoft.pizzeta.Retrofit.guardarPedidoEnLinea.Response.meta meta, com.tritonsoft.pizzeta.Retrofit.guardarPedidoEnLinea.Response.data data) {
        this.meta = meta;
        this.data = data;
    }

    public com.tritonsoft.pizzeta.Retrofit.guardarPedidoEnLinea.Response.meta getMeta() {
        return meta;
    }

    public void setMeta(com.tritonsoft.pizzeta.Retrofit.guardarPedidoEnLinea.Response.meta meta) {
        this.meta = meta;
    }

    public com.tritonsoft.pizzeta.Retrofit.guardarPedidoEnLinea.Response.data getData() {
        return data;
    }

    public void setData(com.tritonsoft.pizzeta.Retrofit.guardarPedidoEnLinea.Response.data data) {
        this.data = data;
    }
}










