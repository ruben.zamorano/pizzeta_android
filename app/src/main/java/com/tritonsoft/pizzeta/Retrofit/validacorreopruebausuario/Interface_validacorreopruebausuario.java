package com.tritonsoft.pizzeta.Retrofit.validacorreopruebausuario;


import com.tritonsoft.pizzeta.Retrofit.validacorreopruebausuario.Response.Response_validacorreopruebausuario;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Ruben on 12/28/2017.
 */

public interface Interface_validacorreopruebausuario {


    @POST("WSPizzeta/api/validacorreopruebausuario/")
    Call<Response_validacorreopruebausuario> post(@Body JSON_validacorreopruebausuario body);
}