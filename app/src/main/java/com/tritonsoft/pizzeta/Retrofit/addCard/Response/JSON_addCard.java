package com.tritonsoft.pizzeta.Retrofit.addCard.Response;

public class JSON_addCard {
    public String card_number;
    public String holder_name;
    public String expiration_year;
    public String expiration_month;
    public String cvv2;
    public String device_session_id;

    public JSON_addCard(String card_number, String holder_name, String expiration_year, String expiration_month, String cvv2, String device_session_id) {
        this.card_number = card_number;
        this.holder_name = holder_name;
        this.expiration_year = expiration_year;
        this.expiration_month = expiration_month;
        this.cvv2 = cvv2;
        this.device_session_id = device_session_id;
    }

    public String getCard_number() {
        return card_number;
    }

    public void setCard_number(String card_number) {
        this.card_number = card_number;
    }

    public String getHolder_name() {
        return holder_name;
    }

    public void setHolder_name(String holder_name) {
        this.holder_name = holder_name;
    }

    public String getExpiration_year() {
        return expiration_year;
    }

    public void setExpiration_year(String expiration_year) {
        this.expiration_year = expiration_year;
    }

    public String getExpiration_month() {
        return expiration_month;
    }

    public void setExpiration_month(String expiration_month) {
        this.expiration_month = expiration_month;
    }

    public String getCvv2() {
        return cvv2;
    }

    public void setCvv2(String cvv2) {
        this.cvv2 = cvv2;
    }

    public String getDevice_session_id() {
        return device_session_id;
    }

    public void setDevice_session_id(String device_session_id) {
        this.device_session_id = device_session_id;
    }
}


//        'card_number': '4111111111111111',
//        'holder_name': 'Juan Perez Ramirez',
//        'expiration_year': '20',
//        'expiration_month': '12',
//        'cvv2': '110',
//        'device_session_id': 'kR1MiQhz2otdIuUlQkbEyitIqVMiI16f'