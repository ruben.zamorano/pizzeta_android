package com.tritonsoft.pizzeta.Retrofit.GetPizzas.Events;


import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Callback_GetPizzas;
import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.Response_GetPizzas;

/**
 * Created by ruben on 12/02/17.
 */

public class Event_GetPizzas {

    private Response_GetPizzas serverResponse;

    public Event_GetPizzas(Response_GetPizzas serverResponse, final Callback_GetPizzas callback, int nTipoProducto) {
        this.serverResponse = serverResponse;
        callback.onSuccess_GetPizzas(serverResponse,nTipoProducto);
    }

    public Response_GetPizzas getServerResponse() {
        return serverResponse;
    }

    public void setServerResponse(Response_GetPizzas serverResponse) {
        this.serverResponse = serverResponse;
    }


}
