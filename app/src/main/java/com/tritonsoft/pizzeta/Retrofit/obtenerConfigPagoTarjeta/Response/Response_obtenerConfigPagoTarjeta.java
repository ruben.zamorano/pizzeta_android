package com.tritonsoft.pizzeta.Retrofit.obtenerConfigPagoTarjeta.Response;

import java.util.List;

/**
 * Created by rubenzamorano on 15/07/17.
 */

public class Response_obtenerConfigPagoTarjeta {

    public List<data> data;
    public com.tritonsoft.pizzeta.Retrofit.obtenerConfigPagoTarjeta.Response.meta meta;

    public List<com.tritonsoft.pizzeta.Retrofit.obtenerConfigPagoTarjeta.Response.data> getData() {
        return data;
    }

    public void setData(List<com.tritonsoft.pizzeta.Retrofit.obtenerConfigPagoTarjeta.Response.data> data) {
        this.data = data;
    }

    public com.tritonsoft.pizzeta.Retrofit.obtenerConfigPagoTarjeta.Response.meta getMeta() {
        return meta;
    }

    public void setMeta(com.tritonsoft.pizzeta.Retrofit.obtenerConfigPagoTarjeta.Response.meta meta) {
        this.meta = meta;
    }

    public Response_obtenerConfigPagoTarjeta(List<com.tritonsoft.pizzeta.Retrofit.obtenerConfigPagoTarjeta.Response.data> data, com.tritonsoft.pizzeta.Retrofit.obtenerConfigPagoTarjeta.Response.meta meta) {
        this.data = data;
        this.meta = meta;
    }
}










