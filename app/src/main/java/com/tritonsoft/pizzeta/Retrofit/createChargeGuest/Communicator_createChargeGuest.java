package com.tritonsoft.pizzeta.Retrofit.createChargeGuest;


import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tritonsoft.pizzeta.DTO.DTO_Carrito;
import com.tritonsoft.pizzeta.Retrofit.createChargeGuest.Events.ErrorEvent;
import com.tritonsoft.pizzeta.Retrofit.createChargeGuest.Events.Event_createChargeGuest;
import com.tritonsoft.pizzeta.Retrofit.createChargeGuest.Response.Response_createChargeGuest;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * ruben zamorano
 */

public class Communicator_createChargeGuest {
    private static final String TAG = "CommunicatorBusqueda";
    private static final String SERVER_URL = "http://104.196.157.87/";

    public void GuardarPedido(JSON_createChargeGuest body, final Callback_createChargeGuest callback) {


        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true);

        httpClient.addInterceptor(logging);

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
//                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(SERVER_URL)
                .build();


        Interface_createChargeGuest service = retrofit.create(Interface_createChargeGuest.class);

        Call<Response_createChargeGuest> call = service.post(body);

        call.enqueue(new Callback<Response_createChargeGuest>() {
            @Override
            public void onResponse(@NonNull Call<Response_createChargeGuest> call, @NonNull Response<Response_createChargeGuest> response) {
                BusProvider.getInstance().post(new Event_createChargeGuest(response.body(), callback));
            }

            @Override
            public void onFailure(@NonNull Call<Response_createChargeGuest> call, @NonNull Throwable t) {
                String message = "Error";
                if (t.getMessage() != null)
                    message = t.getMessage();
                BusProvider.getInstance().post(new ErrorEvent(-2, message,callback));
            }
        });
    }
}
