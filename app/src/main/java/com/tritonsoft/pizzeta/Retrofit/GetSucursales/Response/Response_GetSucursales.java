package com.tritonsoft.pizzeta.Retrofit.GetSucursales.Response;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by rubenzamorano on 15/07/17.
 */
public class Response_GetSucursales {
    public meta meta;
    public data data;

    public Response_GetSucursales(com.tritonsoft.pizzeta.Retrofit.GetSucursales.Response.meta meta, com.tritonsoft.pizzeta.Retrofit.GetSucursales.Response.data data) {
        this.meta = meta;
        this.data = data;
    }

    public com.tritonsoft.pizzeta.Retrofit.GetSucursales.Response.meta getMeta() {
        return meta;
    }

    public void setMeta(com.tritonsoft.pizzeta.Retrofit.GetSucursales.Response.meta meta) {
        this.meta = meta;
    }

    public com.tritonsoft.pizzeta.Retrofit.GetSucursales.Response.data getData() {
        return data;
    }

    public void setData(com.tritonsoft.pizzeta.Retrofit.GetSucursales.Response.data data) {
        this.data = data;
    }
}










