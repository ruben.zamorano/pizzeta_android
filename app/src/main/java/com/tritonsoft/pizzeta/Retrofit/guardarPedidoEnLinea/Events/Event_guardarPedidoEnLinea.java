package com.tritonsoft.pizzeta.Retrofit.guardarPedidoEnLinea.Events;


import com.tritonsoft.pizzeta.Retrofit.guardarPedidoEnLinea.Callback_guardarPedidoEnLinea;
import com.tritonsoft.pizzeta.Retrofit.guardarPedidoEnLinea.Response.Response_guardarPedidoEnLinea;

/**
 * Created by ruben on 12/02/17.
 */

public class Event_guardarPedidoEnLinea {

    private Response_guardarPedidoEnLinea serverResponse;

    public Event_guardarPedidoEnLinea(Response_guardarPedidoEnLinea serverResponse, final Callback_guardarPedidoEnLinea callback) {
        this.serverResponse = serverResponse;
        callback.onSuccess_guardarPedidoEnLinea(serverResponse);
    }

    public Response_guardarPedidoEnLinea getServerResponse() {
        return serverResponse;
    }

    public void setServerResponse( Response_guardarPedidoEnLinea serverResponse) {
        this.serverResponse = serverResponse;
    }


}
