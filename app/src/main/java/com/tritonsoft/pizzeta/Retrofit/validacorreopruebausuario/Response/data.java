package com.tritonsoft.pizzeta.Retrofit.validacorreopruebausuario.Response;

public class data {
    public response response;

    public data(com.tritonsoft.pizzeta.Retrofit.validacorreopruebausuario.Response.response response) {
        this.response = response;
    }

    public com.tritonsoft.pizzeta.Retrofit.validacorreopruebausuario.Response.response getResponse() {
        return response;
    }

    public void setResponse(com.tritonsoft.pizzeta.Retrofit.validacorreopruebausuario.Response.response response) {
        this.response = response;
    }
}

//"data": {
//        "id": "trqzftfjsplnaumzseeg",
//        "authorization": "801585",
//        "operation_type": "in",
//        "method": "card",
//        "transaction_type": "charge",
//        "card": {
//        "type": "debit",
//        "brand": "visa",
//        "address": null,
//        "card_number": "415231XXXXXX8558",
//        "holder_name": "ruben zamorano",
//        "expiration_year": "23",
//        "expiration_month": "08",
//        "allows_charges": true,
//        "allows_payouts": true,
//        "bank_name": "BANCOMER",
//        "bank_code": "012"
//        },