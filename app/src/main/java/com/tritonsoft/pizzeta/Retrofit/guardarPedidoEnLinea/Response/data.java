package com.tritonsoft.pizzeta.Retrofit.guardarPedidoEnLinea.Response;

import java.util.List;

public class data {
    public String idu_pedidoEnLinea;
    public boolean opc_guardapedido;
    public String des_mensaje;

    public data(String idu_pedidoEnLinea, boolean opc_guardapedido, String des_mensaje) {
        this.idu_pedidoEnLinea = idu_pedidoEnLinea;
        this.opc_guardapedido = opc_guardapedido;
        this.des_mensaje = des_mensaje;
    }

    public String getIdu_pedidoEnLinea() {
        return idu_pedidoEnLinea;
    }

    public void setIdu_pedidoEnLinea(String idu_pedidoEnLinea) {
        this.idu_pedidoEnLinea = idu_pedidoEnLinea;
    }

    public boolean isOpc_guardapedido() {
        return opc_guardapedido;
    }

    public void setOpc_guardapedido(boolean opc_guardapedido) {
        this.opc_guardapedido = opc_guardapedido;
    }

    public String getDes_mensaje() {
        return des_mensaje;
    }

    public void setDes_mensaje(String des_mensaje) {
        this.des_mensaje = des_mensaje;
    }
}

//
//"idu_pedidoEnLinea": "1103",
//        "opc_guardapedido": true,
//        "des_mensaje": "Pedido 1103 guardado."
