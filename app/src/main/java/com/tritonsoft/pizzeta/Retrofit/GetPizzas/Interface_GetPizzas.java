package com.tritonsoft.pizzeta.Retrofit.GetPizzas;


import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.JSON_GetPizzas;
import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.Response_GetPizzas;
import com.tritonsoft.pizzeta.Retrofit.createChargeGuest.JSON_createChargeGuest;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Url;

/**
 * Created by Ruben on 12/28/2017.
 */

public interface Interface_GetPizzas {


    @POST("WSPizzeta/api/obtenerMenu/")
    Call<Response_GetPizzas> post(@Body JSON_GetPizzas body);
}