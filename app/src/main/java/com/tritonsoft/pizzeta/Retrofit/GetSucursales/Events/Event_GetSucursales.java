package com.tritonsoft.pizzeta.Retrofit.GetSucursales.Events;


import com.tritonsoft.pizzeta.Retrofit.GetSucursales.Callback_GetSucursales;
import com.tritonsoft.pizzeta.Retrofit.GetSucursales.Response.Response_GetSucursales;
import com.tritonsoft.pizzeta.Retrofit.GetSucursales.Response.marker;

import java.util.List;

/**
 * Created by ruben on 12/02/17.
 */

public class Event_GetSucursales {

    private Response_GetSucursales serverResponse;

    public Event_GetSucursales(Response_GetSucursales serverResponse, final Callback_GetSucursales callback) {
        this.serverResponse = serverResponse;
        callback.onSuccess_GetSucursales(serverResponse);
    }

    public Response_GetSucursales getServerResponse() {
        return serverResponse;
    }

    public void setServerResponse( Response_GetSucursales serverResponse) {
        this.serverResponse = serverResponse;
    }


}
