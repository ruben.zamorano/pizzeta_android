package com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response;

import java.util.List;

/**
 * Created by rubenzamorano on 06/03/18.
 */

public class data
{
//    public String idu_tipoproducto;
//    public String des_tipoproducto;
    public List<producto> menu;

    public data(List<producto> menu) {
        this.menu = menu;
    }

    public List<producto> getMenu() {
        return menu;
    }

    public void setMenu(List<producto> menu) {
        this.menu = menu;
    }
}

//        "idu_tipoproducto": "1",
//        "des_tipoproducto": "PIZZAS",