package com.tritonsoft.pizzeta.Retrofit.GetBanner.Response;

public class toda {
    public String des_ruta_l;
    public String des_ruta_m;
    public String des_ruta_s;

    public toda(String des_ruta_l, String des_ruta_m, String des_ruta_s) {
        this.des_ruta_l = des_ruta_l;
        this.des_ruta_m = des_ruta_m;
        this.des_ruta_s = des_ruta_s;
    }

    public String getDes_ruta_l() {
        return des_ruta_l;
    }

    public void setDes_ruta_l(String des_ruta_l) {
        this.des_ruta_l = des_ruta_l;
    }

    public String getDes_ruta_m() {
        return des_ruta_m;
    }

    public void setDes_ruta_m(String des_ruta_m) {
        this.des_ruta_m = des_ruta_m;
    }

    public String getDes_ruta_s() {
        return des_ruta_s;
    }

    public void setDes_ruta_s(String des_ruta_s) {
        this.des_ruta_s = des_ruta_s;
    }
}

//
//        {
//            "idu_img": "11",
//            "des_img": "slider_11.jpg",
//            "des_descripcion": "familiar al instante comics",
//            "fec_inicio": "2018-11-01",
//            "fec_expira": "2019-12-31",
//            "des_linkto": null,
//            "num_orden": "1",
//            "opc_vigente": true,
//            "num_diasvence": "355",
//            "des_ruta_l": "assets/images/home/slider/large/slider_11.jpg",
//            "des_ruta_m": "assets/images/home/slider/medium/slider_11.jpg",
//            "des_ruta_s": "assets/images/home/slider/small/slider_11.jpg"
//        },