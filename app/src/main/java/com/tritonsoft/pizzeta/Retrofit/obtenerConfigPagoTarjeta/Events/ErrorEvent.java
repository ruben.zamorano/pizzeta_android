package com.tritonsoft.pizzeta.Retrofit.obtenerConfigPagoTarjeta.Events;


import com.tritonsoft.pizzeta.Retrofit.obtenerConfigPagoTarjeta.Callback_obtenerConfigPagoTarjeta;

/**
 * Created by Ruben Zamorano on 2/11/2016.
 */
public class ErrorEvent {
    private int errorCode;
    private String errorMsg;

    public ErrorEvent(int errorCode, String errorMsg, final Callback_obtenerConfigPagoTarjeta callback) {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
        callback.onFail_obtenerConfigPagoTarjeta(errorMsg);
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
