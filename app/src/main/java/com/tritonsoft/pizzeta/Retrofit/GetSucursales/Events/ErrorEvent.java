package com.tritonsoft.pizzeta.Retrofit.GetSucursales.Events;


import com.tritonsoft.pizzeta.Retrofit.GetSucursales.Callback_GetSucursales;

/**
 * Created by Ruben Zamorano on 2/11/2016.
 */
public class ErrorEvent {
    private int errorCode;
    private String errorMsg;

    public ErrorEvent(int errorCode, String errorMsg, final Callback_GetSucursales callback) {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
        callback.onFail_GetSucursales(errorMsg);
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
