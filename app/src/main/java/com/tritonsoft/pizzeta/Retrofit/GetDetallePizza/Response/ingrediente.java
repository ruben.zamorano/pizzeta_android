package com.tritonsoft.pizzeta.Retrofit.GetDetallePizza.Response;

public class ingrediente {
    public String idu_ingrediente;
    public String nom_ingrediente;

    public ingrediente(String idu_ingrediente, String nom_ingrediente) {
        this.idu_ingrediente = idu_ingrediente;
        this.nom_ingrediente = nom_ingrediente;
    }

    public String getIdu_ingrediente() {
        return idu_ingrediente;
    }

    public void setIdu_ingrediente(String idu_ingrediente) {
        this.idu_ingrediente = idu_ingrediente;
    }

    public String getNom_ingrediente() {
        return nom_ingrediente;
    }

    public void setNom_ingrediente(String nom_ingrediente) {
        this.nom_ingrediente = nom_ingrediente;
    }
}
