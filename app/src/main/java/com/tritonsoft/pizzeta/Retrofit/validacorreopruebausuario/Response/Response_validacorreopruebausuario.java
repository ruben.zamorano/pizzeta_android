package com.tritonsoft.pizzeta.Retrofit.validacorreopruebausuario.Response;

/**
 * Created by rubenzamorano on 15/07/17.
 */
public class Response_validacorreopruebausuario {
    public meta meta;
    public data data;

    public Response_validacorreopruebausuario(com.tritonsoft.pizzeta.Retrofit.validacorreopruebausuario.Response.meta meta, com.tritonsoft.pizzeta.Retrofit.validacorreopruebausuario.Response.data data) {
        this.meta = meta;
        this.data = data;
    }

    public com.tritonsoft.pizzeta.Retrofit.validacorreopruebausuario.Response.meta getMeta() {
        return meta;
    }

    public void setMeta(com.tritonsoft.pizzeta.Retrofit.validacorreopruebausuario.Response.meta meta) {
        this.meta = meta;
    }

    public com.tritonsoft.pizzeta.Retrofit.validacorreopruebausuario.Response.data getData() {
        return data;
    }

    public void setData(com.tritonsoft.pizzeta.Retrofit.validacorreopruebausuario.Response.data data) {
        this.data = data;
    }
}


//
//{
//        "bError": false,
//        "data": {
//        "id": "trqzftfjsplnaumzseeg",
//        "authorization": "801585",
//        "operation_type": "in",
//        "method": "card",
//        "transaction_type": "charge",
//        "card": {
//        "type": "debit",
//        "brand": "visa",
//        "address": null,
//        "card_number": "415231XXXXXX8558",
//        "holder_name": "ruben zamorano",
//        "expiration_year": "23",
//        "expiration_month": "08",
//        "allows_charges": true,
//        "allows_payouts": true,
//        "bank_name": "BANCOMER",
//        "bank_code": "012"
//        },
//        "status": "completed",
//        "conciliated": false,
//        "creation_date": "2019-04-14T13:15:16-05:00",
//        "operation_date": "2019-04-14T13:15:17-05:00",
//        "description": "Cargo inicial a mi cuenta",
//        "error_message": null,
//        "order_id": "oid-0002241",
//        "currency": "MXN",
//        "amount": 100,
//        "customer": {
//        "name": "ruben",
//        "last_name": "zamorano",
//        "email": "ruben.zamorano@outlook.com",
//        "phone_number": "6672556612",
//        "address": null,
//        "creation_date": "2019-04-14T13:15:15-05:00",
//        "external_id": null,
//        "clabe": null
//        },
//        "fee": {
//        "amount": 5.4,
//        "tax": 0.864,
//        "currency": "MXN"
//        }
//        },
//        "cMensaje": "cargo generado"
//        }








