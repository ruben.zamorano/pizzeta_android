package com.tritonsoft.pizzeta.Retrofit.DetallePedido.Response;

public class marker {
    public String id;
    public String name;
    public String address;
    public String lat;
    public String lng;
    public String distance;
    public String tel;
    public String horario;
    public String serdom;
    public String sucabierta;
    public String impserdom;


    public marker(String id, String name, String address, String lat, String lng, String distance, String tel, String horario, String serdom, String sucabierta, String impserdom) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.lat = lat;
        this.lng = lng;
        this.distance = distance;
        this.tel = tel;
        this.horario = horario;
        this.serdom = serdom;
        this.sucabierta = sucabierta;
        this.impserdom = impserdom;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public String getSerdom() {
        return serdom;
    }

    public void setSerdom(String serdom) {
        this.serdom = serdom;
    }

    public String getSucabierta() {
        return sucabierta;
    }

    public void setSucabierta(String sucabierta) {
        this.sucabierta = sucabierta;
    }

    public String getImpserdom() {
        return impserdom;
    }

    public void setImpserdom(String impserdom) {
        this.impserdom = impserdom;
    }
}


//<marker
//    id="45"
//    name="La Conquista"
//    address="Boulevard Conquistadores No. 1260 Local 11 y 12 Fracc. La Conquista C.P. 80058"
//    lat="24.811779"
//    lng="-107.439651"
//    distance="2.87202774225678"
//    tel="(667) 7237070"
//    horario="Todos los días de 10am a 10pm."
//    serdom="0"
//    sucabierta="1"
//    impserdom="0"/>