package com.tritonsoft.pizzeta.Retrofit.GetSucursales;


import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tritonsoft.pizzeta.Retrofit.GetSucursales.Events.ErrorEvent;
import com.tritonsoft.pizzeta.Retrofit.GetSucursales.Events.Event_GetSucursales;
import com.tritonsoft.pizzeta.Retrofit.GetSucursales.Response.Response_GetSucursales;
import com.tritonsoft.pizzeta.Retrofit.GetSucursales.Response.marker;

import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * ruben zamorano
 */

public class Communicator_GetSucursales {
    private static final String TAG = "CommunicatorBusqueda";
    private static final String SERVER_URL = "http://www.pizzeta.com.mx";
    private static final String url_path = "/WSPizzeta/api/obtenerSucPorCoordenadasApp/";

    public void BuscarSucursales(String lat, String lng, String radio, final Callback_GetSucursales callback) {


        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true);

        httpClient.addInterceptor(logging);

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
//                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(SERVER_URL)
                .build();


        Interface_GetSucursales service = retrofit.create(Interface_GetSucursales.class);

        Call<Response_GetSucursales> call = service.get(url_path + lat + "/" + lng + "/" + radio);

        call.enqueue(new Callback<Response_GetSucursales>() {
            @Override
            public void onResponse(@NonNull Call<Response_GetSucursales> call, @NonNull Response<Response_GetSucursales> response) {
                BusProvider.getInstance().post(new Event_GetSucursales(response.body(), callback));
            }

            @Override
            public void onFailure(@NonNull Call<Response_GetSucursales> call, @NonNull Throwable t) {
                String message = "Error";
                if (t.getMessage() != null)
                    message = t.getMessage();
                BusProvider.getInstance().post(new ErrorEvent(-2, message,callback));
            }
        });
    }
}
