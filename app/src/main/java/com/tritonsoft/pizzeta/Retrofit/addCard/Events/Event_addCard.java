package com.tritonsoft.pizzeta.Retrofit.addCard.Events;


import com.tritonsoft.pizzeta.Retrofit.addCard.Callback_addCard;
import com.tritonsoft.pizzeta.Retrofit.addCard.Response.Response_addCard;

/**
 * Created by ruben on 12/02/17.
 */

public class Event_addCard {

    private Response_addCard serverResponse;

    public Event_addCard(Response_addCard serverResponse, final Callback_addCard callback) {
        this.serverResponse = serverResponse;
        callback.onSuccess_addCard(serverResponse);
    }

    public Response_addCard getServerResponse() {
        return serverResponse;
    }

    public void setServerResponse(Response_addCard serverResponse) {
        this.serverResponse = serverResponse;
    }


}
