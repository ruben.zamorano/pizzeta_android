package com.tritonsoft.pizzeta.Retrofit.validacorreopruebausuario.Events;


import com.tritonsoft.pizzeta.Retrofit.validacorreopruebausuario.Callback_validacorreopruebausuario;

/**
 * Created by Ruben Zamorano on 2/11/2016.
 */
public class ErrorEvent {
    private int errorCode;
    private String errorMsg;

    public ErrorEvent(int errorCode, String errorMsg, final Callback_validacorreopruebausuario callback) {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
        callback.onFail_validacorreopruebausuario(errorMsg);
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
