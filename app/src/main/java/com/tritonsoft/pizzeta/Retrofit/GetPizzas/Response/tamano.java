package com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response;

public class tamano {
    public String idu_producto;
    public String idu_pro;
    public String idu_tamano;
    public String nom_tamano;
    public String des_tamano;
    public String imp_precio;
    public int num_cantidad;
    public int imp_importe;
    public int imp_precioMod;
    public int imp_total;

    public String idu_modificador;
    public int idu_opcion;

    public tamano(String idu_producto, String idu_pro, String idu_tamano, String nom_tamano, String des_tamano, String imp_precio, int num_cantidad, int imp_importe, int imp_precioMod, int imp_total, String idu_modificador, int idu_opcion) {
        this.idu_producto = idu_producto;
        this.idu_pro = idu_pro;
        this.idu_tamano = idu_tamano;
        this.nom_tamano = nom_tamano;
        this.des_tamano = des_tamano;
        this.imp_precio = imp_precio;
        this.num_cantidad = num_cantidad;
        this.imp_importe = imp_importe;
        this.imp_precioMod = imp_precioMod;
        this.imp_total = imp_total;
        this.idu_modificador = idu_modificador;
        this.idu_opcion = idu_opcion;
    }

    public String getIdu_producto() {
        return idu_producto;
    }

    public void setIdu_producto(String idu_producto) {
        this.idu_producto = idu_producto;
    }

    public String getIdu_pro() {
        return idu_pro;
    }

    public void setIdu_pro(String idu_pro) {
        this.idu_pro = idu_pro;
    }

    public String getIdu_tamano() {
        return idu_tamano;
    }

    public void setIdu_tamano(String idu_tamano) {
        this.idu_tamano = idu_tamano;
    }

    public String getNom_tamano() {
        return nom_tamano;
    }

    public void setNom_tamano(String nom_tamano) {
        this.nom_tamano = nom_tamano;
    }

    public String getDes_tamano() {
        return des_tamano;
    }

    public void setDes_tamano(String des_tamano) {
        this.des_tamano = des_tamano;
    }

    public String getImp_precio() {
        return imp_precio;
    }

    public void setImp_precio(String imp_precio) {
        this.imp_precio = imp_precio;
    }

    public int getNum_cantidad() {
        return num_cantidad;
    }

    public void setNum_cantidad(int num_cantidad) {
        this.num_cantidad = num_cantidad;
    }

    public int getImp_importe() {
        return imp_importe;
    }

    public void setImp_importe(int imp_importe) {
        this.imp_importe = imp_importe;
    }

    public int getImp_precioMod() {
        return imp_precioMod;
    }

    public void setImp_precioMod(int imp_precioMod) {
        this.imp_precioMod = imp_precioMod;
    }

    public int getImp_total() {
        return imp_total;
    }

    public void setImp_total(int imp_total) {
        this.imp_total = imp_total;
    }

    public String getIdu_modificador() {
        return idu_modificador;
    }

    public void setIdu_modificador(String idu_modificador) {
        this.idu_modificador = idu_modificador;
    }

    public int getIdu_opcion() {
        return idu_opcion;
    }

    public void setIdu_opcion(int idu_opcion) {
        this.idu_opcion = idu_opcion;
    }
}

//        "idu_producto": "1",
//        "idu_pro": "1584",
//        "idu_tamano": "1",
//        "nom_tamano": "Compañera",
//        "des_tamano": "30 cm de diámetro,  ideal para dos o tres personas",
//        "imp_precio": "95"


//        "idu_producto": "78",
//        "idu_tamano": "2",
//        "nom_tamano": "Normal",
//        "des_tamano": "Porción estandar",
//        "imp_precio": "56"
//
//
//        "idu_producto": "1",
//        "idu_tamano": "1",
//        "nom_tamano": "Compañera",
//        "des_tamano": "30 cm de diámetro,  ideal para dos o tres personas",
//        "imp_precio": "95"
//
//
//        "idu_producto": "1",
//        "idu_tamanio": "1",
//        "imp_precio": "95",
//        "nom_tamanio": "Compañera",
//        "des_tamano": "30 cm de diámetro,  ideal para dos o tres personas",
//
//        "num_cantidad": 1
//        "imp_importe": 0,
//        "imp_precioMod": 0,
//        "imp_total": 95,