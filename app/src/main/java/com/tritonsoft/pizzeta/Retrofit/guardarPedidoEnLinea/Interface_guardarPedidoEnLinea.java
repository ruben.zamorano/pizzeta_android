package com.tritonsoft.pizzeta.Retrofit.guardarPedidoEnLinea;


import com.tritonsoft.pizzeta.DTO.DTO_Carrito;
import com.tritonsoft.pizzeta.Retrofit.guardarPedidoEnLinea.Response.Response_guardarPedidoEnLinea;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Url;

/**
 * Created by Ruben on 12/28/2017.
 */

public interface Interface_guardarPedidoEnLinea {


    @POST("WSPizzeta/api/guardarPedidoEnLinea/")
    Call<Response_guardarPedidoEnLinea> post(@Body DTO_Carrito body);
}