package com.tritonsoft.pizzeta.Retrofit.createChargeGuest;


import com.tritonsoft.pizzeta.Retrofit.createChargeGuest.Response.Response_createChargeGuest;

/**
 * Created by ruben on 02/02/17.
 */

public interface Callback_createChargeGuest {

    void onSuccess_createChargeGuest(Response_createChargeGuest result);

    void onFail_createChargeGuest(String result);
}
