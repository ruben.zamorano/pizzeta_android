package com.tritonsoft.pizzeta.Retrofit.GetBanner;


import android.support.annotation.NonNull;

import com.tritonsoft.pizzeta.Retrofit.GetBanner.Events.ErrorEvent;
import com.tritonsoft.pizzeta.Retrofit.GetBanner.Events.Event_GetBanner;
import com.tritonsoft.pizzeta.Retrofit.GetBanner.Response.Response_GetBanner;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * ruben zamorano
 */

public class Communicator_GetBanner {
    private static final String TAG = "CommunicatorBusqueda";
    private static final String SERVER_URL = "http://www.pizzeta.com.mx";
    private static final String url_path = "/WSPizzeta/api/obtenerImgSlider";

    public void BuscarBanner(final Callback_GetBanner callback) {


        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true);

        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(SERVER_URL)
                .build();


        Interface_GetBanner service = retrofit.create(Interface_GetBanner.class);

        Call<Response_GetBanner> call = service.get(url_path);

        call.enqueue(new Callback<Response_GetBanner>() {
            @Override
            public void onResponse(@NonNull Call<Response_GetBanner> call, @NonNull Response<Response_GetBanner> response) {
                BusProvider.getInstance().post(new Event_GetBanner(response.body(), callback,0));
            }

            @Override
            public void onFailure(@NonNull Call<Response_GetBanner> call, @NonNull Throwable t) {
                String message = "Error";
                if (t.getMessage() != null)
                    message = t.getMessage();
                BusProvider.getInstance().post(new ErrorEvent(-2, message,callback));
            }
        });
    }
}
