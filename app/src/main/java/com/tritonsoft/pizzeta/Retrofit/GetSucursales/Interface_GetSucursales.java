package com.tritonsoft.pizzeta.Retrofit.GetSucursales;


import com.tritonsoft.pizzeta.Retrofit.GetSucursales.Response.Response_GetSucursales;
import com.tritonsoft.pizzeta.Retrofit.GetSucursales.Response.marker;

import org.simpleframework.xml.ElementList;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by Ruben on 12/28/2017.
 */

public interface Interface_GetSucursales {


    @GET
    Call<Response_GetSucursales> get(@Url String url);
}