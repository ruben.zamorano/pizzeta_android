package com.tritonsoft.pizzeta.Retrofit.GetSucursales;


import com.tritonsoft.pizzeta.Retrofit.GetSucursales.Response.Response_GetSucursales;
import com.tritonsoft.pizzeta.Retrofit.GetSucursales.Response.marker;

import java.util.List;

/**
 * Created by ruben on 02/02/17.
 */

public interface Callback_GetSucursales {

    void onSuccess_GetSucursales( Response_GetSucursales result);

    void onFail_GetSucursales(String result);
}
