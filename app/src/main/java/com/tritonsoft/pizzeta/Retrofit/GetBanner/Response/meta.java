package com.tritonsoft.pizzeta.Retrofit.GetBanner.Response;

/**
 * Created by rubenzamorano on 06/03/18.
 */

public class meta
{
    public int count;
    public String status;

    public meta(int count, String status) {
        this.count = count;
        this.status = status;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}