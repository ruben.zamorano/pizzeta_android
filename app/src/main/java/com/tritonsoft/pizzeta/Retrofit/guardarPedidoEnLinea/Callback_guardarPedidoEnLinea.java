package com.tritonsoft.pizzeta.Retrofit.guardarPedidoEnLinea;


import com.tritonsoft.pizzeta.Retrofit.guardarPedidoEnLinea.Response.Response_guardarPedidoEnLinea;

/**
 * Created by ruben on 02/02/17.
 */

public interface Callback_guardarPedidoEnLinea {

    void onSuccess_guardarPedidoEnLinea(Response_guardarPedidoEnLinea result);

    void onFail_guardarPedidoEnLinea(String result);
}
