package com.tritonsoft.pizzeta.Retrofit.createChargeGuest;


import com.tritonsoft.pizzeta.DTO.DTO_Carrito;
import com.tritonsoft.pizzeta.Retrofit.createChargeGuest.Response.Response_createChargeGuest;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Ruben on 12/28/2017.
 */

public interface Interface_createChargeGuest {


    @POST("openpay/pizzeta/customers/createChargeGuest/")
    Call<Response_createChargeGuest> post(@Body JSON_createChargeGuest body);
}