package com.tritonsoft.pizzeta.Retrofit.GetDetallePizza;


import android.support.annotation.NonNull;

import com.tritonsoft.pizzeta.Retrofit.GetDetallePizza.Events.ErrorEvent;
import com.tritonsoft.pizzeta.Retrofit.GetDetallePizza.Events.Event_GetDetallePizza;
import com.tritonsoft.pizzeta.Retrofit.GetDetallePizza.Response.Response_GetDetallePizza;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * ruben zamorano
 */

public class Communicator_GetDetallePizza {
    private static final String TAG = "CommunicatorBusqueda";
    private static final String SERVER_URL = "http://www.pizzeta.com.mx";
    private static final String url_path = "/WSPizzeta/api/obtenerDetalleProducto/";

    public void BuscarDetallePizza(String nTipoProducto, String nAgrupador, final Callback_GetDetallePizza callback) {


        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true);

        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(SERVER_URL)
                .build();


        Interface_GetDetallePizza service = retrofit.create(Interface_GetDetallePizza.class);

        Call<Response_GetDetallePizza> call = service.get(url_path + nTipoProducto + "/" + nAgrupador);

        call.enqueue(new Callback<Response_GetDetallePizza>() {
            @Override
            public void onResponse(@NonNull Call<Response_GetDetallePizza> call, @NonNull Response<Response_GetDetallePizza> response) {
                BusProvider.getInstance().post(new Event_GetDetallePizza(response.body(), callback,0));
            }

            @Override
            public void onFailure(@NonNull Call<Response_GetDetallePizza> call, @NonNull Throwable t) {
                String message = "Error";
                if (t.getMessage() != null)
                    message = t.getMessage();
                BusProvider.getInstance().post(new ErrorEvent(-2, message,callback));
            }
        });
    }
}
