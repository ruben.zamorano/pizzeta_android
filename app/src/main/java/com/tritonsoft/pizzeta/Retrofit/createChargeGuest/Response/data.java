package com.tritonsoft.pizzeta.Retrofit.createChargeGuest.Response;

public class data {
    public String id;
    public String authorization;
    public String method;

    public data(String id, String authorization, String method) {
        this.id = id;
        this.authorization = authorization;
        this.method = method;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAuthorization() {
        return authorization;
    }

    public void setAuthorization(String authorization) {
        this.authorization = authorization;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }
}

//"data": {
//        "id": "trqzftfjsplnaumzseeg",
//        "authorization": "801585",
//        "operation_type": "in",
//        "method": "card",
//        "transaction_type": "charge",
//        "card": {
//        "type": "debit",
//        "brand": "visa",
//        "address": null,
//        "card_number": "415231XXXXXX8558",
//        "holder_name": "ruben zamorano",
//        "expiration_year": "23",
//        "expiration_month": "08",
//        "allows_charges": true,
//        "allows_payouts": true,
//        "bank_name": "BANCOMER",
//        "bank_code": "012"
//        },