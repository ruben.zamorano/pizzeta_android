package com.tritonsoft.pizzeta.Retrofit.GetBanner;

/**
 * Created by Ruben Zamorano on 2/11/2016.
 */

import com.squareup.otto.Bus;

public class BusProvider {

    private static final Bus BUS = new Bus();

    public static Bus getInstance() {
        return BUS;
    }

    public BusProvider() {
    }
}

