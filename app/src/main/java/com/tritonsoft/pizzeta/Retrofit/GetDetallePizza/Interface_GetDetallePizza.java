package com.tritonsoft.pizzeta.Retrofit.GetDetallePizza;


import com.tritonsoft.pizzeta.Retrofit.GetDetallePizza.Response.Response_GetDetallePizza;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by Ruben on 12/28/2017.
 */

public interface Interface_GetDetallePizza {


    @GET
    Call<Response_GetDetallePizza> get(@Url String url);
}