package com.tritonsoft.pizzeta.Retrofit.DetallePedido.Response;

import java.util.List;

public class data {
    public String idu_estatuspedido;

    public data(String idu_estatuspedido) {
        this.idu_estatuspedido = idu_estatuspedido;
    }

    public String getIdu_estatuspedido() {
        return idu_estatuspedido;
    }

    public void setIdu_estatuspedido(String idu_estatuspedido) {
        this.idu_estatuspedido = idu_estatuspedido;
    }
}

//{
//        "idu_pedido": "1120",
//        "idu_origenpedido": "2",
//        "des_origenpedido": "APP",
//        "idu_sucursal": "40",
//        "nom_sucursal": "San Isidro",
//        "num_telefono": "(667) 7452222",
//        "nom_municipio": "Culiacan",
//        "idu_tipopedido": "2",
//        "des_tipopedido": "DOMICILIO",
//        "idu_formapagopedido": "1",
//        "des_formapagopedido": "EFECTIVO",
//        "imp_totalpedido": "158",
//        "idu_estatuspedido": "2",
//        "des_estatuspedido": "Recibido En Sucursal",
//        "fec_pedido": "2019-01-31 01:09:07",
//        "opc_cancelado": "0",
//        "datospedido": {
//        "idu_pedido": "1120",
//        "nom_cliente": "ruben",
//        "num_telefono": "",
//        "des_correo": "6672556612",
//        "des_calle": "AV. KIKI MURILLO 103",
//        "num_ext": "103",
//        "num_int": null,
//        "nom_colonia": "La Primavera",
//        "num_codigopostal": "80199",
//        "nom_ciudad": "Culiacán Rosales",
//        "nom_estado": "Sinaloa",
//        "des_linkmap": null,
//        "des_domicilio": "AV. KIKI MURILLO 103 103  La Primavera 80199 Culiacán Rosales Sinaloa"
//        },
//        "productos": []
//        }