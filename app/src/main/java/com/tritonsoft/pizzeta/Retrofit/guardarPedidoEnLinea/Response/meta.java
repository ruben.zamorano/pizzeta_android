package com.tritonsoft.pizzeta.Retrofit.guardarPedidoEnLinea.Response;

public class meta {
    public String status;
    public int count;

    public meta(String status, int count) {
        this.status = status;
        this.count = count;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
