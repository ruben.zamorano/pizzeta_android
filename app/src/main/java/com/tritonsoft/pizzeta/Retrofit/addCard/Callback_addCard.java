package com.tritonsoft.pizzeta.Retrofit.addCard;


import com.tritonsoft.pizzeta.Retrofit.addCard.Response.Response_addCard;

/**
 * Created by ruben on 02/02/17.
 */

public interface Callback_addCard {

    void onSuccess_addCard(Response_addCard result);

    void onFail_addCard(String result);
}
