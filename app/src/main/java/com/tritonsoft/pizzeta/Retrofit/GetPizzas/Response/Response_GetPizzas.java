package com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response;

/**
 * Created by rubenzamorano on 15/07/17.
 */

public class Response_GetPizzas {

    public data data;
    public meta meta;

    public Response_GetPizzas(com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.data data, com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.meta meta) {
        this.data = data;
        this.meta = meta;
    }

    public com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.data getData() {
        return data;
    }

    public void setData(com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.data data) {
        this.data = data;
    }

    public com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.meta getMeta() {
        return meta;
    }

    public void setMeta(com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.meta meta) {
        this.meta = meta;
    }
}










