package com.tritonsoft.pizzeta.Retrofit.DetallePedido;


import com.tritonsoft.pizzeta.Retrofit.DetallePedido.Response.Response_DetallePedido;

/**
 * Created by ruben on 02/02/17.
 */

public interface Callback_DetallePedido {

    void onSuccess_DetallePedido(Response_DetallePedido result);

    void onFail_DetallePedido(String result);
}
