package com.tritonsoft.pizzeta.Retrofit.createChargeGuest;

public class customer {
    public String name;
    public String last_name;
    public String phone_number;
    public String email;

    public customer(String name, String last_name, String phone_number, String email) {
        this.name = name;
        this.last_name = last_name;
        this.phone_number = phone_number;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}


//         "name" : "ruben",
//        "last_name" : "zamorano",
//        "phone_number" : "6672556612",
//        "email" : "ruben.zamorano@outlook.com"