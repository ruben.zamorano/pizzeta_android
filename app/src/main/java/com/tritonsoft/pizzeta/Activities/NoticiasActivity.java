package com.tritonsoft.pizzeta.Activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.tritonsoft.pizzeta.R;

public class NoticiasActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_noticias);

        ImageView btn_cerrar = findViewById(R.id.btn_cerrar);
        btn_cerrar.setOnClickListener(v -> {
            finish();
        });

        ImageView b2 = findViewById(R.id.img_b2);
        Glide.with(this)
                .load("https://new.pizzeta.com.mx/assets/images/home/banners/large/banner_2.jpg")
                .thumbnail(0.1f)
                .into(b2);
    }

}
