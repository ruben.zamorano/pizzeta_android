package com.tritonsoft.pizzeta.Activities;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.tritonsoft.pizzeta.Adapters.Adapter_carrito;
import com.tritonsoft.pizzeta.Adapters.Adapter_sucursales;
import com.tritonsoft.pizzeta.Helpers.GPSTracker;
import com.tritonsoft.pizzeta.Helpers.Helpers;
import com.tritonsoft.pizzeta.R;
import com.tritonsoft.pizzeta.Retrofit.GetSucursales.Callback_GetSucursales;
import com.tritonsoft.pizzeta.Retrofit.GetSucursales.Communicator_GetSucursales;
import com.tritonsoft.pizzeta.Retrofit.GetSucursales.Response.Response_GetSucursales;
import com.tritonsoft.pizzeta.Retrofit.GetSucursales.Response.marker;

import static android.Manifest.permission_group.LOCATION;

public class SucursalesActivity extends AppCompatActivity implements Callback_GetSucursales {


    public final Gson gson = new Gson();
    public EditText ed_buscar;
    public ImageView btn_clear;
    public RecyclerView recyclerView;
    public LinearLayoutManager lm;
    public Adapter_sucursales adapter_sucursales;

    RelativeLayout ly_mensaje;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sucursales);
        Helpers.setContext(this);

        Helpers.setToolbar(this, "Sucursales");

        ly_mensaje = findViewById(R.id.ly_mensaje);

        ed_buscar = findViewById(R.id.ed_buscar);
        btn_clear = findViewById(R.id.btn_clear);
        btn_clear.setOnClickListener(v -> {
            ed_buscar.setText("");
        });


        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        String[] permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION, LOCATION};
        CardView btn_gps = findViewById(R.id.btn_gps);
        btn_gps.setOnClickListener(view -> {
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                //buildAlertMessageNoGps();

            } else {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[0])) {

                    //This is called if user has denied the permission before
                    //In this case I am just asking the permission again
                    ActivityCompat.requestPermissions(this, permissions, 1);

                } else {

                    ActivityCompat.requestPermissions(this, permissions, 1);
                }
            }

        });

        if (ContextCompat.checkSelfPermission(this, permissions[0]) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[0])) {

                //This is called if user has denied the permission before
                //In this case I am just asking the permission again
                ActivityCompat.requestPermissions(this, permissions, 1);

            } else {

                ActivityCompat.requestPermissions(this, permissions, 1);
            }
        } else {


            if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                //inicializar sucursales
                BuscarSucursales();
            }

        }
    }


    @Override
    public void onSuccess_GetSucursales(Response_GetSucursales result) {
        Log.d("onSuccess_GetSucursales", gson.toJson(result));

        recyclerView = findViewById(R.id.recycler_sucursales);
        lm = new LinearLayoutManager(this);
        adapter_sucursales = new Adapter_sucursales(this, result.data.getResponse());
        recyclerView.setLayoutManager(lm);
        recyclerView.setAdapter(adapter_sucursales);
        recyclerView.setNestedScrollingEnabled(false);


        ed_buscar.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

                // you can call or do what you want with your EditText here

                // yourEditText...

//                Log.d("afterTextChanged",s.toString());

                adapter_sucursales.filter(s.toString());
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

    }

    @Override
    public void onFail_GetSucursales(String result) {
        Log.d("onFail_GetSucursales", result);
    }

    public void BuscarSucursales()
    {
        ly_mensaje.setVisibility(View.GONE);
        GPSTracker tracker = new GPSTracker(this);
        Communicator_GetSucursales communicator_getSucursales = new Communicator_GetSucursales();
        communicator_getSucursales.BuscarSucursales(tracker.getLatitude() + "",tracker.getLongitude() + "","40",this);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        Log.d("PermissionsResult","" + requestCode);
        Log.d("PermissionsResult",gson.toJson(permissions));
        Log.d("PermissionsResult","");
        if(ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED){
            switch (requestCode) {
                //Location
                case 1:
                    Log.d("Permission","Permission granted");
                    BuscarSucursales();


                    break;

            }

        }else{

            Log.d("Permission","Permission denied");
            //Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
        }
    }



}
