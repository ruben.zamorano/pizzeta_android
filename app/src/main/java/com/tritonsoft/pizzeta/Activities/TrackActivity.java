package com.tritonsoft.pizzeta.Activities;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.tritonsoft.pizzeta.Helpers.Helpers;
import com.tritonsoft.pizzeta.R;
import com.tritonsoft.pizzeta.Retrofit.DetallePedido.Callback_DetallePedido;
import com.tritonsoft.pizzeta.Retrofit.DetallePedido.Communicator_DetallePedido;
import com.tritonsoft.pizzeta.Retrofit.DetallePedido.Response.Response_DetallePedido;

import org.w3c.dom.Text;

public class TrackActivity extends AppCompatActivity implements Callback_DetallePedido {


    public TextView lvl_fecha;
    public TextView lvl_orden;
    public final Gson gson = new Gson();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track);
        Helpers.setToolbar(this,"Historial de pedidos");

        lvl_fecha = findViewById(R.id.lvl_fecha);
        lvl_fecha.setText(Helpers.getPrefe("dFecha",""));

        lvl_orden = findViewById(R.id.lvl_orden);
        lvl_orden.setText("#" + Helpers.getPrefe("folioOrden",""));


        Communicator_DetallePedido communicator_detallePedido = new Communicator_DetallePedido();
        communicator_detallePedido.BuscarSucursales(Helpers.getPrefe("folioOrden",""),this);



    }


    public void Paso1()
    {
        ImageView img_paso1 = findViewById(R.id.img_paso1);
        img_paso1.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary), android.graphics.PorterDuff.Mode.SRC_IN);

    }
    public void Paso2()
    {
        ImageView img_paso2 = findViewById(R.id.img_paso2);
        LinearLayout line_bottom_p1 = findViewById(R.id.line_bottom_p1);
        LinearLayout line_top_p2 = findViewById(R.id.line_top_p2);


        img_paso2.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary), android.graphics.PorterDuff.Mode.SRC_IN);
        line_bottom_p1.setBackgroundColor(Color.parseColor("#e65100"));
        line_top_p2.setBackgroundColor(Color.parseColor("#e65100"));


    }
    public void Paso3()
    {
        ImageView img_paso3 = findViewById(R.id.img_paso3);
        LinearLayout line_bottom_p2 = findViewById(R.id.line_bottom_p2);
        LinearLayout line_top_p3 = findViewById(R.id.line_top_p3);

        img_paso3.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary), android.graphics.PorterDuff.Mode.SRC_IN);
        line_bottom_p2.setBackgroundColor(Color.parseColor("#e65100"));
        line_top_p3.setBackgroundColor(Color.parseColor("#e65100"));
    }
    public void Paso4()
    {
        ImageView img_paso4 = findViewById(R.id.img_paso4);
        LinearLayout line_bottom_p3 = findViewById(R.id.line_bottom_p3);
        LinearLayout line_top_p4 = findViewById(R.id.line_top_p4);

        img_paso4.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary), android.graphics.PorterDuff.Mode.SRC_IN);
        line_bottom_p3.setBackgroundColor(Color.parseColor("#e65100"));
        line_top_p4.setBackgroundColor(Color.parseColor("#e65100"));

    }

    public void Paso5()
    {
        ImageView img_paso5 = findViewById(R.id.img_paso5);
        LinearLayout line_bottom_p4 = findViewById(R.id.line_bottom_p4);
        LinearLayout line_top_p5 = findViewById(R.id.line_top_p5);

        img_paso5.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary), android.graphics.PorterDuff.Mode.SRC_IN);
        line_bottom_p4.setBackgroundColor(Color.parseColor("#e65100"));
        line_top_p5.setBackgroundColor(Color.parseColor("#e65100"));

    }

    @Override
    public void onSuccess_DetallePedido(Response_DetallePedido result) {
        Log.d("DetallePedido",gson.toJson(result));

        try {
            int estatus = Integer.parseInt(result.data.get(0).idu_estatuspedido);

            switch (estatus)
            {
                case 1:
                    Paso1();
                    break;
                case 2:
                    Paso1();
                    Paso2();
                    break;
                case 3:
                    Paso1();
                    Paso2();
                    Paso3();
                    break;
                case 4:
                    Paso1();
                    Paso2();
                    Paso3();
                    Paso4();
                    break;
                case 5:
                    Paso1();
                    Paso2();
                    Paso3();
                    Paso4();
                    Paso5();
                    break;
            }
        }
        catch (Exception e)
        {

        }

    }

    @Override
    public void onFail_DetallePedido(String result) {

    }
}
