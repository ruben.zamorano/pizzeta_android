package com.tritonsoft.pizzeta.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.solver.widgets.Helper;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tritonsoft.pizzeta.Adapters.Adapter_carrito;
import com.tritonsoft.pizzeta.DTO.DTO_Domicilio;
import com.tritonsoft.pizzeta.Helpers.CreditCardForm.CheckOutActivity;
import com.tritonsoft.pizzeta.Helpers.Helpers;
import com.tritonsoft.pizzeta.Helpers.MoneyTextWatcher;
import com.tritonsoft.pizzeta.R;
import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.producto;
import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.tamano;

import java.lang.reflect.Type;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

public class FormaPagoActivity extends AppCompatActivity {


    private static final String TAG = "FormaPagoActivity";
    public final Gson gson = new Gson();
    public EditText ed_pago;
    public TextView lvl_total;
    public CardView btn_pagar_efectivo;
    public CardView btn_pagar_tarjeta;
    public int total_ = 0;
    public int max_cambio = 500;
    public Boolean bPagoTarjeta = false;

    //bottom sheet alerta
    public RelativeLayout backgroud_alerta;
    public BottomSheetBehavior mBottomSheetBehavior;
    private View bottomSheet;
    public CardView btn_cerrarAlerta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forma_pago);
        Helpers.setContext(this);
        Helpers.setToolbar(this,"Pago");



        btn_cerrarAlerta = findViewById(R.id.btn_cerrarAlerta);
        backgroud_alerta = findViewById(R.id.backgroud_alerta);
        bottomSheet = findViewById(R.id.bottom_sheet);
        mBottomSheetBehavior = BottomSheetBehavior.from(this.bottomSheet);
        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(View bottomSheet, int newState) {

                switch (newState) {

                    case BottomSheetBehavior.STATE_DRAGGING:

                        break;

                    case BottomSheetBehavior.STATE_COLLAPSED:
                        backgroud_alerta.setVisibility(View.GONE);

                        break;

                    case BottomSheetBehavior.STATE_EXPANDED:
                        break;
                }

            }

            @Override
            public void onSlide(View bottomSheet, float slideOffset) {

            }
        });

        btn_cerrarAlerta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backgroud_alerta.setVisibility(View.GONE);
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });


        Helpers.setPrefe("imp_pagacon","0");
        Helpers.setPrefe("imp_cambio","0");


        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("bPagoTarjeta");



        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
             @Override
             public void onDataChange(DataSnapshot snapshot) {

                 Log.d("onDataChange",snapshot.getValue().toString());
                 bPagoTarjeta = Boolean.parseBoolean(snapshot.getValue().toString());
             }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });




        lvl_total = findViewById(R.id.lvl_total);
        int nTotal =  CalcularTotal(ObtenerPizzas());
        Helpers.setPrefe("nTotal",nTotal + "");
        String total  = nTotal +"";

        if(total.length() >= 4)
        {
            total = total.charAt(0) + "," + total.substring(1,total.length());
        }

        lvl_total.setText("$" + total);
        ed_pago = findViewById(R.id.ed_pago);
        ed_pago.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                int pago = 0;

                if(s.length() > 0)
                {

                    try {
                        pago = Integer.parseInt(s.toString());
                    }
                    catch (Exception e)
                    {
                        pago = 0;
                        ed_pago.setText("");
                    }
                }



            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }
        });


        Helpers.setPrefeBool("bCarrito",false);
        RecyclerView recyclerView = findViewById(R.id.recycler_carrito);
        LinearLayoutManager lm = new LinearLayoutManager(this);
        Adapter_carrito adapter_carrito = new Adapter_carrito(this,ObtenerPizzas());
        recyclerView.setLayoutManager(lm);
        recyclerView.setAdapter(adapter_carrito);
        recyclerView.setNestedScrollingEnabled(false);

        btn_pagar_tarjeta = findViewById(R.id.btn_pagar_tarjeta);
        btn_pagar_tarjeta.setOnClickListener(v ->{
            if(bPagoTarjeta)
            {
                Intent intent =  new Intent(FormaPagoActivity.this, CheckOutActivity.class);
                startActivity(intent);
            }
            else
            {
                //alerta

                MostrarAlerta("Ups","Por el momento el pago con tarjeta no esta disponible");

            }

        });
        btn_pagar_efectivo = findViewById(R.id.btn_pagar_efectivo);
        btn_pagar_efectivo.setOnClickListener(v ->{

            if(ed_pago.getText().length() == 0 || Integer.parseInt(ed_pago.getText().toString()) < total_)
            {
                MostrarAlerta("Ups","Ocupamos saber con cuanto pagas para regresarte tu cambio.");
                return;
            }
            else
            {
                Helpers.setPrefe("paga_con",ed_pago.getText().toString());
                Helpers.setPrefe("imp_pagacon",ed_pago.getText().toString());
                int paga_con = Integer.parseInt(ed_pago.getText().toString());
                int cambio = paga_con - total_;


                Helpers.setPrefe("imp_cambio",cambio + "");

                if(cambio > 500)
                {
                    //mensaje
                    MostrarAlerta("Ups","No podemos llevar mas de $500 pesos en cambio");
                    return;
                }
                else
                {
                    Helpers.setPrefe("clv_tipopago","1");
                    Intent intent =  new Intent(this, CarritoActivity.class);
                    startActivity(intent);
                }



            }

        });


        CardView btn_efectivo = findViewById(R.id.btn_efectivo);
        TextView lvl_efectivo = findViewById(R.id.lvl_efectivo);
        FrameLayout frame_efectivo = findViewById(R.id.frame_efectivo);
        LinearLayout container_efectivo = findViewById(R.id.container_efectivo);

        CardView btn_tarjeta = findViewById(R.id.btn_tarjeta);
        TextView lvl_tarjeta = findViewById(R.id.lvl_tarjeta);
        FrameLayout frame_tarjeta = findViewById(R.id.frame_tarjeta);
        LinearLayout container_tarjeta = findViewById(R.id.container_tarjeta);

        btn_efectivo.setOnClickListener(v ->{
            CerrarTeclado();
            container_tarjeta.setVisibility(View.GONE);
            container_efectivo.setVisibility(View.VISIBLE);
            frame_efectivo.setBackgroundResource(R.color.rojoPizzeta);
            frame_tarjeta.setBackgroundResource(R.drawable.card_edge_rojo);

            lvl_efectivo.setTextColor(getResources().getColor(R.color.white));
            lvl_tarjeta.setTextColor(getResources().getColor(R.color.rojoPizzeta));

        });

        btn_tarjeta.setOnClickListener(v ->{
            CerrarTeclado();
            container_efectivo.setVisibility(View.GONE);
            container_tarjeta.setVisibility(View.VISIBLE);
            frame_tarjeta.setBackgroundResource(R.color.rojoPizzeta);
            frame_efectivo.setBackgroundResource(R.drawable.card_edge_rojo);

            lvl_efectivo.setTextColor(getResources().getColor(R.color.rojoPizzeta));
            lvl_tarjeta.setTextColor(getResources().getColor(R.color.white));
        });




    }


    public List<producto> ObtenerPizzas()
    {
        List<producto> lstPizzas = new ArrayList<>();
        try {
            Type listType = new TypeToken<ArrayList<producto>>(){}.getType();
            String clstPizzas = Helpers.getPrefe("lstPizzas");
            if(clstPizzas == null || clstPizzas.length() == 0)
            {
                lstPizzas = new ArrayList<>();
            }
            else
            {
                lstPizzas = gson.fromJson(clstPizzas, listType);
            }

        }
        catch (Exception e)
        {
            lstPizzas = new ArrayList<>();
        }

        return lstPizzas;
    }


    public int CalcularTotal(List<producto> items)
    {
        int subtotal = 0;
        int envio = Integer.parseInt(Helpers.getPrefe("envio","0"));

        for (producto p: items)
        {
            for (tamano d: p.tamanios)
            {
                subtotal += d.imp_total;
            }
        }

        total_ =  subtotal + envio;


        String total  = subtotal + envio +"";

        if(total.length() >= 4)
        {
            total = total.charAt(0) + "," + total.substring(1,total.length());
        }

        lvl_total.setText("$" + total);

        return  subtotal + envio;





    }


    public void CerrarTeclado()
    {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void MostrarAlerta(String cTitulo, String cMensaje)
    {
        TextView lvl_tituloMensaje = findViewById(R.id.lvl_tituloMensaje);
        TextView lvl_mensaje = findViewById(R.id.lvl_mensaje);

        lvl_tituloMensaje.setText(cTitulo);
        lvl_mensaje.setText(cMensaje);

        backgroud_alerta.setVisibility(View.VISIBLE);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }


//    private String current = "";
//    @Override
//    public void onTextChanged(CharSequence s, int start, int before, int count) {
//        if(!s.toString().equals(current)){
//            ed_pago.removeTextChangedListener(this);
//
//            String cleanString = s.toString().replaceAll("[$,.]", "");
//
//            double parsed = Double.parseDouble(cleanString);
//            String formatted = NumberFormat.getCurrencyInstance().format((parsed/100));
//
//            current = formatted;
//            ed_pago.setText(formatted);
//            ed_pago.setSelection(formatted.length());
//
//            ed_pago.addTextChangedListener(this);
//        }
//    }

}
