package com.tritonsoft.pizzeta.Activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.constraint.solver.widgets.Helper;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.L;
import com.bumptech.glide.Glide;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tritonsoft.pizzeta.Adapters.Adapter_carrito;
import com.tritonsoft.pizzeta.Adapters.Adapter_ingrediente;
import com.tritonsoft.pizzeta.DTO.DTO_Carrito;
import com.tritonsoft.pizzeta.DTO.DTO_Domicilio;
import com.tritonsoft.pizzeta.DTO.DTO_Perfil;
import com.tritonsoft.pizzeta.Helpers.CreditCardForm.CheckOutActivity;
import com.tritonsoft.pizzeta.Helpers.Helpers;
import com.tritonsoft.pizzeta.Helpers.datospedido;
import com.tritonsoft.pizzeta.R;
import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.producto;
import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.tamano;
import com.tritonsoft.pizzeta.Retrofit.createChargeGuest.Callback_createChargeGuest;
import com.tritonsoft.pizzeta.Retrofit.createChargeGuest.Communicator_createChargeGuest;
import com.tritonsoft.pizzeta.Retrofit.createChargeGuest.JSON_createChargeGuest;
import com.tritonsoft.pizzeta.Retrofit.createChargeGuest.Response.Response_createChargeGuest;
import com.tritonsoft.pizzeta.Retrofit.guardarPedidoEnLinea.Callback_guardarPedidoEnLinea;
import com.tritonsoft.pizzeta.Retrofit.guardarPedidoEnLinea.Communicator_guardarPedidoEnLinea;
import com.tritonsoft.pizzeta.Retrofit.guardarPedidoEnLinea.Response.Response_guardarPedidoEnLinea;
import com.tritonsoft.pizzeta.Retrofit.obtenerConfigPagoTarjeta.Callback_obtenerConfigPagoTarjeta;
import com.tritonsoft.pizzeta.Retrofit.obtenerConfigPagoTarjeta.Communicator_obtenerConfigPagoTarjeta;
import com.tritonsoft.pizzeta.Retrofit.obtenerConfigPagoTarjeta.Response.Response_obtenerConfigPagoTarjeta;
import com.tritonsoft.pizzeta.Retrofit.validacorreopruebausuario.Callback_validacorreopruebausuario;
import com.tritonsoft.pizzeta.Retrofit.validacorreopruebausuario.Communicator_validacorreopruebausuario;
import com.tritonsoft.pizzeta.Retrofit.validacorreopruebausuario.JSON_validacorreopruebausuario;
import com.tritonsoft.pizzeta.Retrofit.validacorreopruebausuario.Response.Response_validacorreopruebausuario;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CarritoActivity extends AppCompatActivity implements Callback_guardarPedidoEnLinea, Callback_obtenerConfigPagoTarjeta, Callback_createChargeGuest, Callback_validacorreopruebausuario {


    public TextView lvl_subtotal;
    public TextView lvl_total;
    public final Gson gson = new Gson();
    public DTO_Carrito carrito;

    public DatabaseReference myRef;
    public boolean bFolioCargado = false;
    public JSON_createChargeGuest bodyCard;


    //bottom sheet alerta
    public RelativeLayout backgroud_alerta;
    public BottomSheetBehavior mBottomSheetBehavior;
    private View bottomSheet;
    public CardView btn_cerrarAlerta;



    //bottom sheet registro
    public BottomSheetBehavior mBottomSheetBehavior_registro;
    private View bottomSheet_registro;
    public CardView btn_cerrarAlerta_registro;
    public CardView btn_registro;

    public boolean opc_puedecomprar = false;
    public String des_mensaje = "";
    public int pje_descuento = 60;
    public int num_limiteproductos = 5;
    public  List<producto> lstPizzas = new ArrayList<>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carrito);
        Helpers.setContext(this);






//        Toast.makeText(this, Helpers.getPrefe("token_openpay","error"), Toast.LENGTH_SHORT).show();

       Log.d("JSON_createChargeGuest", Helpers.getPrefe("JSON_createChargeGuest",""));

        carrito = new DTO_Carrito();


        FirebaseDatabase database = FirebaseDatabase.getInstance();
        myRef = database.getReference("lstPedidos");


        ImageView btn_cerrar = findViewById(R.id.btn_cerrar);
        btn_cerrar.setOnClickListener(v -> {
            finish();
        });

        ImageView btn_delete_all = findViewById(R.id.btn_delete_all);
        btn_delete_all.setOnClickListener(v -> {
            delete_all();
        });

        LinearLayout btn_continuar = findViewById(R.id.btn_continuar);
        btn_continuar.setOnClickListener(v ->{

            if(!opc_puedecomprar )
            {
                MostrarAlerta("Ups",des_mensaje);
                return;
            }

            if(calcularArticulos(ObtenerPizzas()) > num_limiteproductos)
            {
                MostrarAlerta("Ups","Solo puedes comprar 5 productos");
                return;
            }



            if(!bFolioCargado)
            {
                //mostrar mensaje
                MostrarAlerta("Mensaje","Espera un momento");
                return;
            }
            else
            {
                Helpers.showModal(this);

                String cFormaPago = Helpers.getPrefe("clv_tipopago","1");

                Log.d("FormaPago",cFormaPago);

                if(Integer.parseInt(cFormaPago) == 2)
                {
                    //llamar a openpay
                    Communicator_createChargeGuest communicator_createChargeGuest = new Communicator_createChargeGuest();
                    communicator_createChargeGuest.GuardarPedido(bodyCard,this);
                }
                else
                {
                    CheckoutPizzeta();
                }



            }




















        });




        btn_cerrarAlerta = findViewById(R.id.btn_cerrarAlerta);
        backgroud_alerta = findViewById(R.id.backgroud_alerta);
        bottomSheet = findViewById(R.id.bottom_sheet);
        mBottomSheetBehavior = BottomSheetBehavior.from(this.bottomSheet);
        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(View bottomSheet, int newState) {

                switch (newState) {

                    case BottomSheetBehavior.STATE_DRAGGING:

                        break;

                    case BottomSheetBehavior.STATE_COLLAPSED:
                        backgroud_alerta.setVisibility(View.GONE);

                        break;

                    case BottomSheetBehavior.STATE_EXPANDED:
                        break;
                }

            }

            @Override
            public void onSlide(View bottomSheet, float slideOffset) {

            }
        });

        btn_cerrarAlerta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backgroud_alerta.setVisibility(View.GONE);
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });



        btn_cerrarAlerta_registro = findViewById(R.id.btn_cerrarAlerta_registro);
        btn_registro = findViewById(R.id.btn_registro);
        bottomSheet_registro = findViewById(R.id.bottom_sheet_registro);
        mBottomSheetBehavior_registro = BottomSheetBehavior.from(this.bottomSheet_registro);
        mBottomSheetBehavior_registro.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(View bottomSheet, int newState) {

                switch (newState) {

                    case BottomSheetBehavior.STATE_DRAGGING:

                        break;

                    case BottomSheetBehavior.STATE_COLLAPSED:
                        backgroud_alerta.setVisibility(View.GONE);

                        break;

                    case BottomSheetBehavior.STATE_EXPANDED:
                        break;
                }

            }

            @Override
            public void onSlide(View bottomSheet, float slideOffset) {

            }
        });

        btn_cerrarAlerta_registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backgroud_alerta.setVisibility(View.GONE);
                mBottomSheetBehavior_registro.setState(BottomSheetBehavior.STATE_COLLAPSED);

                Intent intent =  new Intent(CarritoActivity.this, FormaPagoActivity.class);
                startActivity(intent);
            }
        });

        btn_registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                backgroud_alerta.setVisibility(View.GONE);
                mBottomSheetBehavior_registro.setState(BottomSheetBehavior.STATE_COLLAPSED);

                Intent intent =  new Intent(CarritoActivity.this, FormaPagoActivity.class);
                startActivity(intent);

            }
        });

    }


    public void CalcularTotal(List<producto> items)
    {
        if(items.size() == 0)
        {
            ValidarCarrito(true);
        }
        else
        {
            int subtotal = 0;
            int envio = Integer.parseInt(Helpers.getPrefe("envio","0"));
            TextView lvl_envio = findViewById(R.id.lvl_envio);
            lvl_envio.setText("$" + envio);

            for (producto p: items)
            {
                for (tamano d: p.tamanios)
                {
                    subtotal += d.imp_total;
                }
            }
            lvl_subtotal = findViewById(R.id.lvl_subtotal);
            lvl_subtotal.setText("$" + subtotal);
            lvl_total = findViewById(R.id.lvl_total);
            lvl_total.setText("$" + (subtotal + envio));

            Helpers.setPrefe("imp_totalpedido",(subtotal + envio) + "");
            Helpers.setPrefe("nTotal",(subtotal + envio) + "");
        }


    }

    public void delete_all() {
        new AlertDialog.Builder(this,R.style.AlertDialogTheme)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Vaciar carrito")
                .setMessage("Estas seguro de vaciar el carrito?")
                .setPositiveButton("Si", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //finish();
                        Helpers.setPrefe("lstPizzas","[]");
                        ValidarCarrito(true);


                    }

                })
                .setNegativeButton("No", null)
                .show();


    }

    public void BuscarPizzas()
    {

        List<producto> lstPizzas = new ArrayList<>();
        try {
            Type listType = new TypeToken<ArrayList<producto>>(){}.getType();
            String clstPizzas = Helpers.getPrefe("lstPizzas");
            if(clstPizzas == null || clstPizzas.length() == 0)
            {
                lstPizzas = new ArrayList<>();
            }
            else
            {
                lstPizzas = gson.fromJson(clstPizzas, listType);
            }

        }
        catch (Exception e)
        {
            lstPizzas = new ArrayList<>();
        }
        finally {
            CalcularTotal(lstPizzas);
            Helpers.setPrefeBool("bCarrito",true);
            RecyclerView recyclerView = findViewById(R.id.recycler_carrito);
            LinearLayoutManager lm = new LinearLayoutManager(this);
            Adapter_carrito adapter_carrito = new Adapter_carrito(this,lstPizzas);
            recyclerView.setLayoutManager(lm);
            recyclerView.setAdapter(adapter_carrito);
            recyclerView.setNestedScrollingEnabled(false);
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        // put your code here...

        boolean bCarritoVacio =  Helpers.getPrefeBool("bCarritoVacio",false);
        ValidarCarrito(bCarritoVacio);
        BuscarPizzas();

        DTO_Perfil perfil = gson.fromJson(Helpers.getPrefe("perfil"),DTO_Perfil.class);

        if(!bCarritoVacio)
        {
            if(Helpers.getPrefeBool("bPruebaCerrada",false))
            {
                Helpers.showModal(this);
                JSON_validacorreopruebausuario body = new JSON_validacorreopruebausuario(perfil.cMail);
                Communicator_validacorreopruebausuario communicator_validacorreopruebausuario = new Communicator_validacorreopruebausuario();
                communicator_validacorreopruebausuario.GuardarPedido(body,this);
            }

        }



    }

    public void ValidarCarrito(boolean bCarritoVacio)
    {

        LinearLayout ly_carrito = findViewById(R.id.ly_carrito);
        CardView ly_total = findViewById(R.id.ly_total);
        RelativeLayout ly_info = findViewById(R.id.ly_info);
        if (bCarritoVacio)
        {
            ly_info.setVisibility(View.VISIBLE);
            ly_carrito.setVisibility(View.GONE);
            ly_total.setVisibility(View.GONE);

        }
        else
        {
            ly_info.setVisibility(View.GONE);
            ly_carrito.setVisibility(View.VISIBLE);
            ly_total.setVisibility(View.VISIBLE);
        }
    }

    public List<producto> ObtenerPizzas()
    {
        List<producto> lstPizzas = new ArrayList<>();
        try {
            Type listType = new TypeToken<ArrayList<producto>>(){}.getType();
            String clstPizzas = Helpers.getPrefe("lstPizzas");
            if(clstPizzas == null || clstPizzas.length() == 0)
            {
                lstPizzas = new ArrayList<>();
            }
            else
            {
                lstPizzas = gson.fromJson(clstPizzas, listType);
            }

        }
        catch (Exception e)
        {
            lstPizzas = new ArrayList<>();
        }

        return lstPizzas;
    }

    public List<DTO_Carrito> ObtenerPedidos()
    {
        List<DTO_Carrito> lstCarrito = new ArrayList<>();
        try {
            Type listType = new TypeToken<ArrayList<DTO_Carrito>>(){}.getType();
            String clstPizzas = Helpers.getPrefe("lstCarrito");
            if(clstPizzas == null || clstPizzas.length() == 0)
            {
                lstCarrito = new ArrayList<>();
            }
            else
            {
                lstCarrito = gson.fromJson(clstPizzas, listType);
            }

        }
        catch (Exception e)
        {
            lstCarrito = new ArrayList<>();
        }

        return lstCarrito;
    }


    @Override
    public void onSuccess_guardarPedidoEnLinea(Response_guardarPedidoEnLinea result) {
        Helpers.closeModal(this);
        Log.d("guardarPedidoEnLinea",gson.toJson(result));
        carrito.setIdu_pedidoEnLinea(result.data.idu_pedidoEnLinea);


        //fecha del pedido
        Date dt = new Date();
        SimpleDateFormat simpleDate = new SimpleDateFormat("dd/MM/yyyy");
        String strDt = simpleDate.format(dt);
        carrito.setdFechaPedido(strDt);
        GuardaPedido_cache(carrito);
    }

    @Override
    public void onFail_guardarPedidoEnLinea(String result) {
        Helpers.closeModal(this);
        Log.d("guardarPedidoEnLinea_",result);
        //error de pedido "super mal"

    }


    public void GuardaPedido_cache(DTO_Carrito item)
    {
        //myRef.push().setValue(carrito);
        List<DTO_Carrito> lstCarrito = ObtenerPedidos();
        lstCarrito.add(carrito);
        Helpers.setPrefe("lstCarrito",gson.toJson(lstCarrito));
        Helpers.setPrefe("lstPizzas","[]");
        Helpers.setPrefe("idu_tipopedido","0");

        Intent intent = new Intent(this, PreTrackActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void onSuccess_obtenerConfigPagoTarjeta(Response_obtenerConfigPagoTarjeta result, int nTipoProducto) {
        Log.d("ConfigPagoTarjeta",gson.toJson(result));

        bFolioCargado = true;
        bodyCard = gson.fromJson(Helpers.getPrefe("JSON_createChargeGuest","{}"),JSON_createChargeGuest.class);
        bodyCard.order_id = result.data.get(0).Ds_Merchant_Order;
        bodyCard.amount = Integer.parseInt(Helpers.getPrefe("imp_totalpedido","0"));



    }

    @Override
    public void onFail_obtenerConfigPagoTarjeta(String result) {

        //mostrar error
    }

    public void CheckoutPizzeta()
    {
        //generar JSON carrito
        carrito.setIdu_origenpedido(2);//2 = app
        carrito.setIdu_tipopedido(Integer.parseInt(Helpers.getPrefe("idu_tipopedido")));
        carrito.setIdu_sucursal(Integer.parseInt(Helpers.getPrefe("idu_sucursal")));
//        carrito.setIdu_sucursal(52);
        carrito.setClv_tipopago(Integer.parseInt(Helpers.getPrefe("clv_tipopago")));
        carrito.setImp_cargoservicio(Integer.parseInt(Helpers.getPrefe("envio","0")));
        carrito.setImp_totalpedido(Integer.parseInt(Helpers.getPrefe("imp_totalpedido")));

        DTO_Perfil perfil = gson.fromJson(Helpers.getPrefe("perfil"),DTO_Perfil.class);
        datospedido datospedido = new datospedido();
        DTO_Domicilio item_direccion = gson.fromJson(Helpers.getPrefe("direccion"),DTO_Domicilio.class);


        if(Integer.parseInt(Helpers.getPrefe("idu_tipopedido")) == 2)
        {
            datospedido.setDes_calle(item_direccion.cDireccion);
            datospedido.setNom_ciudad(item_direccion.geo.getLocality() == null ? "" : item_direccion.geo.getLocality());
            datospedido.setNom_colonia(item_direccion.geo.getSubLocality() == null ? "" : item_direccion.geo.getSubLocality());
            datospedido.setNom_estado(item_direccion.geo.getAdminArea() == null ? "" : item_direccion.geo.getAdminArea());

            datospedido.setNum_codigopostal(item_direccion.cp);
            datospedido.setNum_ext(item_direccion.cNumeroExt);
            datospedido.setNum_int(item_direccion.cNumeroInt);
        }


        carrito.setOpc_programado(Integer.parseInt(Helpers.getPrefe("opc_programado")));
        carrito.setFec_programado(Helpers.getPrefe("fec_programado",""));
        carrito.setImp_pagacon(Integer.parseInt(Helpers.getPrefe("imp_pagacon")));
        carrito.setImp_cambio(Integer.parseInt(Helpers.getPrefe("imp_cambio")));



        datospedido.setDes_correo(perfil.cMail);
        datospedido.setDes_linkmap("");

        datospedido.setNom_cliente(perfil.cNombres);


        datospedido.setNum_telefono(perfil.cTelefono);

        carrito.setDatospedido(datospedido);
        carrito.setProductos(lstPizzas);



        Log.d("supercarrito",gson.toJson(carrito));


        Communicator_guardarPedidoEnLinea communicator_guardarPedidoEnLinea = new Communicator_guardarPedidoEnLinea();
        communicator_guardarPedidoEnLinea.GuardarPedido(carrito,this);
    }

    @Override
    public void onSuccess_createChargeGuest(Response_createChargeGuest result) {
        if(result.bError)
        {
            //cambiar metodo de pago o tarjeta
            MostrarAlertaRegistro();
        }
        else
        {
            CheckoutPizzeta();
        }

    }

    @Override
    public void onFail_createChargeGuest(String result) {
        Helpers.closeModal(this);
        //mostrar error de open[ay
        //cambiar metodo de pago o tarjeta
        MostrarAlertaRegistro();
    }


    public void MostrarAlerta(String cTitulo, String cMensaje)
    {
        TextView lvl_tituloMensaje = findViewById(R.id.lvl_tituloMensaje);
        TextView lvl_mensaje = findViewById(R.id.lvl_mensaje);

        lvl_tituloMensaje.setText(cTitulo);
        lvl_mensaje.setText(cMensaje);

        backgroud_alerta.setVisibility(View.VISIBLE);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    public void MostrarAlertaRegistro()
    {

        Helpers.closeModal(this);
        backgroud_alerta.setVisibility(View.VISIBLE);
        mBottomSheetBehavior_registro.setState(BottomSheetBehavior.STATE_EXPANDED);
    }


    @Override
    public void onSuccess_validacorreopruebausuario(Response_validacorreopruebausuario result) {
        Helpers.closeModal(this);
        //result.data.response.opc_puedecomprar = true;
        try {

//            {"meta":{"status":"SUCCESS","count":1},"data":{"response":{"opc_descuento":false,"opc_puedecomprar":true,"des_mensaje":"Ya aplic\u00f3 el descuento anteriormente por este medio (APP)"}}}
            if(result.data.response.opc_puedecomprar && result.data.response.opc_descuento)
            {
                MostrarAlerta("Felicidades","Tienes un 60% de descuento en la compra de 5 articulos o menos");
                opc_puedecomprar = true;
//                pje_descuento = result.data.response.pje_descuento;
//                num_limiteproductos = result.data.response.num_limiteproductos;

                pje_descuento = 40;
                num_limiteproductos = 5;

                //
                if(pje_descuento > 0)
                {
                    //aplicar descuento
                   lstPizzas =  ObtenerPizzas();




                    Log.d("obtenerConfigPago",gson.toJson(lstPizzas));
                    Log.d("obtenerConfigPago",lstPizzas.size() + "");
                    for(int i=0; i<lstPizzas.size(); i++)
                    {
                        for(int j=0; j<lstPizzas.get(i).tamanios.size(); j++)
                        {
                            double precio_original = lstPizzas.get(i).tamanios.get(j).imp_total;
                            precio_original = precio_original / 100;
                            precio_original = precio_original * pje_descuento;

                            Log.d("precio_original",precio_original+"");

                            lstPizzas.get(i).tamanios.get(j).setImp_total((int)precio_original);
                            lstPizzas.get(i).tamanios.get(j).setImp_precio(String.valueOf((int)precio_original));
                        }
                    }





                    CalcularTotal(lstPizzas);



                }



                Log.d("obtenerConfigPago","llego hasta aqui");
                Communicator_obtenerConfigPagoTarjeta communicator_obtenerConfigPagoTarjeta = new Communicator_obtenerConfigPagoTarjeta();
                communicator_obtenerConfigPagoTarjeta.Communicator_obtenerConfigPagoTarjeta(Integer.parseInt(Helpers.getPrefe("nTotal","0")),CarritoActivity.this);





            }
            else
            {
                opc_puedecomprar = false;
                MostrarAlerta("Ups",result.data.response.des_mensaje);
                des_mensaje = result.data.response.des_mensaje;
            }
        }
        catch (Exception e)
        {
            Log.d("obtenerConfigPago",e.getMessage());
        }

    }

    @Override
    public void onFail_validacorreopruebausuario(String result) {
        Helpers.closeModal(this);
    }


    public int calcularArticulos(List<producto> lstProductos)
    {
        Log.d("calcularArticulos",gson.toJson(lstProductos));
        int nArticulos = 0;

        for(producto p : lstProductos)
        {
            for(tamano t : p.tamanios)
            {
                nArticulos += t.num_cantidad;
            }
        }

        Log.d("calcularArticulos",lstProductos.size() + "");
        return nArticulos;
    }
}
