package com.tritonsoft.pizzeta.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.tritonsoft.pizzeta.Helpers.Helpers;
import com.tritonsoft.pizzeta.R;

public class PerfilActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);

        Helpers.setPrefeBool("bMenuPerfil",true);
        ImageView btn_cerrar = findViewById(R.id.btn_cerrar);
        btn_cerrar.setOnClickListener(v -> {
            finish();
        });
        ImageView btn_salir = findViewById(R.id.btn_salir);
        btn_salir.setOnClickListener(v -> {
//            Intent homeIntent = new Intent(Intent.ACTION_MAIN);
//            homeIntent.addCategory( Intent.CATEGORY_HOME );
//            homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            startActivity(homeIntent);

            moveTaskToBack(true);
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(1);
        });

        LinearLayout btn_direcciones = findViewById(R.id.btn_direcciones);
        btn_direcciones.setOnClickListener(v ->{
            Intent intent = new Intent(this,SeleccionarDomicilioActivity.class);
            startActivity(intent);
        });

        LinearLayout btn_historial = findViewById(R.id.btn_historial);
        btn_historial.setOnClickListener(v ->{
            Intent intent = new Intent(this,HistorialActivity.class);
            startActivity(intent);
        });

        LinearLayout btn_datosPersonales = findViewById(R.id.btn_datosPersonales);
        btn_datosPersonales.setOnClickListener(v ->{
            Intent intent = new Intent(this,RegistroActivity.class);
            Bundle mBundle = new Bundle();
            mBundle.putBoolean("bPantallaPerfil", true);
            intent.putExtras(mBundle);
            startActivity(intent);
        });



    }
}
