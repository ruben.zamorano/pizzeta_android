package com.tritonsoft.pizzeta.Activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tritonsoft.pizzeta.Adapters.AdaptadorPizza;
import com.tritonsoft.pizzeta.Adapters.Adapter_carrito;
import com.tritonsoft.pizzeta.DTO.DTO_Pizza;
import com.tritonsoft.pizzeta.Fragments.BlankFragment;
import com.tritonsoft.pizzeta.Fragments.HomeFragment;
import com.tritonsoft.pizzeta.Fragments.PizzasFragment;
import com.tritonsoft.pizzeta.Fragments.SucursalesFragment;
import com.tritonsoft.pizzeta.Helpers.BasicAddHeaderInterceptor;
import com.tritonsoft.pizzeta.Helpers.Helpers;
import com.tritonsoft.pizzeta.Helpers.Response_AddCardOpenPay;
import com.tritonsoft.pizzeta.R;
import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.producto;
import com.tritonsoft.pizzeta.Retrofit.addCard.Callback_addCard;
import com.tritonsoft.pizzeta.Retrofit.addCard.Communicator_addCard;
import com.tritonsoft.pizzeta.Retrofit.addCard.Response.JSON_addCard;
import com.tritonsoft.pizzeta.Retrofit.addCard.Response.Response_addCard;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import mx.openpay.android.Openpay;
import mx.openpay.android.OperationCallBack;
import mx.openpay.android.OperationResult;
import mx.openpay.android.exceptions.OpenpayServiceException;
import mx.openpay.android.exceptions.ServiceUnavailableException;
import mx.openpay.android.model.Card;
import mx.openpay.android.validation.CardValidator;

public class PrincipalActivity extends AppCompatActivity implements Callback_addCard {



    public final Gson gson = new Gson();
    public int nTab = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        Helpers.setContext(this);

//        Helpers.setPrefe("lstCarrito","[]");
//        Helpers.setPrefe("lstPizzas","[]");
        InicializarBottomTabs(1);


//        credenciales pizzeta
//        ID: m8caejzcetc7ib2i44df
//        Llave privada sk_60f283efd7c94d7bbdf2bb9e8297280a
//        publica pk_1011f940f8d340c4bd6fc20edd3ecb7d
//        Openpay openpay = new Openpay("m8caejzcetc7ib2i44df", "pk_1011f940f8d340c4bd6fc20edd3ecb7d", false);
//
//
//        Card card = new Card();
//        card.holderName("ruben zamorano");
//        card.cardNumber("4152313427748558");
//        card.expirationMonth(8);
//        card.expirationYear(23);
//        card.cvv2("611");
//
//        openpay.createToken(card, new OperationCallBack() {
//
//            @Override
//            public void onSuccess(OperationResult arg0) {
//                //Handlo in success
//                Log.d("createToken","onSuccess" + ", " + gson.toJson(arg0.getResult()));
//
////                Response_AddCardOpenPay response = gson.fromJson(gson.toJson(arg0.getResult()),Response_AddCardOpenPay.class);
////                Helpers.setPrefe("token_openpay",response.id);
////                Helpers.setPrefe("response_openpay",gson.toJson(response));
//            }
//
//            @Override
//            public void onError(OpenpayServiceException arg0) {
//                //Handle Error
//            }
//
//            @Override
//            public void onCommunicationError(ServiceUnavailableException arg0) {
//                //Handle communication error
//            }
//        });









////        Registrar tarjeta cliente
//        String nIdCliente = "awvq9zljjwp82ymqkcn3";
//        JSON_addCard body = new JSON_addCard("4111111111111111","Juan Perez Ramirez","20","12","110","");
//        Communicator_addCard communicator_addCard = new Communicator_addCard();
//        communicator_addCard.addCardServer(nIdCliente,body,this);


//        String varToken = "sk_4d98840e559a4f2aa002159192e561d2";
//        BasicAddHeaderInterceptor authorization = new BasicAddHeaderInterceptor(varToken);
//        Log.d("varToken", authorization.Authorization);






    }



    public void InicializarBottomTabs(int nPantalla)
    {

        //validar clicks en los tabs para no repetir

        RelativeLayout btn_home = findViewById(R.id.btn_home);
        RelativeLayout btn_sucursales = findViewById(R.id.btn_sucursales);
        RelativeLayout btn_add = findViewById(R.id.btn_add);
        RelativeLayout btn_promociones = findViewById(R.id.btn_promociones);
        RelativeLayout btn_perfil = findViewById(R.id.btn_perfil);

        setScreen(nPantalla);


        btn_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setScreen(1);

            }
        });

        btn_sucursales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setScreen(2);

            }
        });
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //setScreen(3);
                Helpers.setPrefe("nTipoProducto","1");
                Intent intent =  new Intent(PrincipalActivity.this, PizzaActivity.class);
//                Intent intent =  new Intent(PrincipalActivity.this, ArmaTuPizzaActivity.class);
                //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                //finish();
            }
        });
        btn_promociones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BuscarPizzas();
            }
        });
        btn_perfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //setScreen(3);
                Intent intent =  new Intent(PrincipalActivity.this, PerfilActivity.class);
                //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                //finish();
            }
        });


    }


    public void setScreen(int nPantalla)
    {

        Fragment selectedFragment = null;
        LinearLayout indicator_home = findViewById(R.id.indicator_home);
        indicator_home.setVisibility(View.GONE);
        LinearLayout indicator_sucursales = findViewById(R.id.indicator_sucursales);
        indicator_sucursales.setVisibility(View.GONE);
        LinearLayout indicator_pizzas = findViewById(R.id.indicator_pizzas);
        indicator_pizzas.setVisibility(View.GONE);
        LinearLayout indicator_notificaciones = findViewById(R.id.indicator_promociones);
        indicator_notificaciones.setVisibility(View.GONE);
//        LinearLayout indicator_per = findViewById(R.id.indicator_home);
        String tag = "";
        switch (nPantalla)
        {
            case 1:
                selectedFragment = HomeFragment.newInstance();
                indicator_home.setVisibility(View.VISIBLE);
                break;
            case 2:
                selectedFragment = SucursalesFragment.newInstance();
                indicator_sucursales.setVisibility(View.VISIBLE);
                tag = "sucursales_tag";
                break;
            case 3:
                selectedFragment = PizzasFragment.newInstance();
                indicator_pizzas.setVisibility(View.VISIBLE);
                break;
            case 4:
                selectedFragment = BlankFragment.newInstance();
                indicator_notificaciones.setVisibility(View.VISIBLE);
                break;
            case 5: selectedFragment = BlankFragment.newInstance();break;
        }
        if(nPantalla != nTab)
        {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout, selectedFragment,tag);
            transaction.commit();
            nTab = nPantalla;
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        Log.d("PermissionsResult","" + requestCode);
        Log.d("PermissionsResult",gson.toJson(permissions));
        Log.d("PermissionsResult","");
        if(ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED){
            switch (requestCode) {
                //Location
                case 1:
                    Log.d("Permission","Permission granted");
                    FragmentManager fm = getSupportFragmentManager();

                    //if you added fragment via layout xml
//                    SucursalesFragment fragment = (SucursalesFragment)fm.findFragmentById(R.id.your_fragment_id);
                    SucursalesFragment fragment = (SucursalesFragment)fm.findFragmentByTag("sucursales_tag");
                    fragment.InicializarMapa();
                    //askForGPS();
                    break;

            }

        }else{

            Log.d("Permission","Permission denied");
            //Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
        }
    }




    public void BuscarPizzas()
    {

        List<producto> lstPizzas = new ArrayList<>();
        try {
            Type listType = new TypeToken<ArrayList<producto>>(){}.getType();
            String clstPizzas = Helpers.getPrefe("lstPizzas");
            if(clstPizzas == null || clstPizzas.length() == 0)
            {
                lstPizzas = new ArrayList<>();
            }
            else
            {
                lstPizzas = gson.fromJson(clstPizzas, listType);
            }

        }
        catch (Exception e)
        {
            lstPizzas = new ArrayList<>();
        }
        finally {
            if(lstPizzas.size() == 0)
            {
                //mostrar carrito vacio
                Helpers.setPrefeBool("bCarritoVacio",true);
                Intent intent =  new Intent(PrincipalActivity.this, CarritoActivity.class);
                startActivity(intent);
            }
            else
            {
                Helpers.setPrefeBool("bCarritoVacio",false);
                //validar direccion
                int nTipoPedido = Integer.parseInt(Helpers.getPrefe("idu_tipopedido","0"));
                Intent intent;
                switch (nTipoPedido)
                {
                    case 0:
                        intent =  new Intent(PrincipalActivity.this, SeleccionarDomicilioActivity.class);
                        startActivity(intent);
                        break;
                    case 1:
//                        Helpers.setPrefe("idu_tipopedido","1");
                        intent =  new Intent(PrincipalActivity.this, SeleccionarDomicilioActivity.class);
                        startActivity(intent);
                        break;
                    case 2:
//                        Helpers.setPrefe("idu_tipopedido","2");
                        intent =  new Intent(PrincipalActivity.this, SeleccionarDomicilioActivity.class);
                        startActivity(intent);
                        break;

                }
                if(nTipoPedido == 0 || nTipoPedido == 1)
                {

                }
                else
                {

                }
            }
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        // put your code here...
        Helpers.setPrefeBool("bMenuPerfil",false);




    }


    @Override
    public void onSuccess_addCard(Response_addCard result) {
        Log.d("addCard","onSuccess: " + gson.toJson(result));
    }

    @Override
    public void onFail_addCard(String result) {
        Log.d("addCard","onFail: " + result);
    }
}
