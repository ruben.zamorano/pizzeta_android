package com.tritonsoft.pizzeta.Activities;

import android.content.Intent;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tritonsoft.pizzeta.Adapters.Adapter_extras;
import com.tritonsoft.pizzeta.Adapters.Adapter_ingrediente;
import com.tritonsoft.pizzeta.Adapters.Adapter_tamanos;
import com.tritonsoft.pizzeta.Helpers.Helpers;
import com.tritonsoft.pizzeta.R;
import com.tritonsoft.pizzeta.Retrofit.GetDetallePizza.Callback_GetDetallePizza;
import com.tritonsoft.pizzeta.Retrofit.GetDetallePizza.Communicator_GetDetallePizza;
import com.tritonsoft.pizzeta.Retrofit.GetDetallePizza.Response.Response_GetDetallePizza;
import com.tritonsoft.pizzeta.Retrofit.GetDetallePizza.Response.ingrediente;
import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.producto;
import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.tamano;
import com.tritonsoft.pizzeta.Retrofit.GetSucursales.Callback_GetSucursales;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class PizzaDetalleActivity extends AppCompatActivity implements Callback_GetDetallePizza {

    public producto pizza;
    public List<producto> lstPizzas;
    public final Gson gson = new Gson();

    public CardView card_companera;
    public TextView lvl_companera;
    public FrameLayout frame_companera;

    public CardView card_familiar;
    public TextView lvl_familiar;
    public FrameLayout frame_familiar;

    public CardView card_pizzota;
    public TextView lvl_pizzota;
    public FrameLayout frame_pizzota;

    public TextView lvl_descripcion;
    public TextView lvl_descripcion_tamanio;


    //bottom sheet alerta
    public RelativeLayout backgroud_alerta;
    public BottomSheetBehavior mBottomSheetBehavior;
    private View bottomSheet;
    public CardView btn_cerrarAlerta;


    //bottom sheet registro
    public BottomSheetBehavior mBottomSheetBehavior_registro;
    private View bottomSheet_registro;
    public CardView btn_cerrarAlerta_registro;
    public CardView btn_registro;

    public ImageView btn_remove;
    public ImageView btn_add;
    public int cont = 1;
    public int limite = 9;
    public TextView lvl_cont;




    public List<tamano> tamanos = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pizza_detalle);
        Helpers.setContext(this);

        Helpers.setToolbar(this,"Detalle");
        Helpers.showModal(this);




        try {
            Type listType = new TypeToken<ArrayList<producto>>(){}.getType();
            String clstPizzas = Helpers.getPrefe("lstPizzas");
            if(clstPizzas == null || clstPizzas.length() == 0)
            {
                lstPizzas = new ArrayList<>();
            }
            else
            {
                lstPizzas = gson.fromJson(clstPizzas, listType);
            }

        }
        catch (Exception e)
        {
            lstPizzas = new ArrayList<>();
        }



        lvl_cont = findViewById(R.id.lvl_cont);
        btn_add = findViewById(R.id.btn_add);
        btn_add.setOnClickListener(v ->{

            UpdateCont(true);
        });
        btn_remove = findViewById(R.id.btn_remove);
        btn_remove.setOnClickListener(v ->{
            UpdateCont(false);
        });

        pizza = gson.fromJson(Helpers.getPrefe("pizza"),producto.class);
        Log.d("super_pizza",gson.toJson(pizza));






        lvl_descripcion = findViewById(R.id.lvl_descripcion);
        lvl_descripcion.setText(pizza.des_producto);

        lvl_descripcion_tamanio = findViewById(R.id.lvl_descripcion_tamanio);

        card_companera = findViewById(R.id.card_companera);
        frame_companera = findViewById(R.id.frame_companera);
        lvl_companera = findViewById(R.id.lvl_companera);
        card_companera.setOnClickListener(v -> {
            setTamanio(0);
        });

        card_familiar = findViewById(R.id.card_familiar);
        frame_familiar = findViewById(R.id.frame_familiar);
        lvl_familiar = findViewById(R.id.lvl_familiar);
        card_familiar.setOnClickListener(v -> {
            setTamanio(1);
        });

        card_pizzota = findViewById(R.id.card_pizzota);
        frame_pizzota = findViewById(R.id.frame_pizzota);
        lvl_pizzota = findViewById(R.id.lvl_pizzota);
        card_pizzota.setOnClickListener(v -> {
            setTamanio(2);
        });





        Communicator_GetDetallePizza communicator_getDetallePizza = new Communicator_GetDetallePizza();
        communicator_getDetallePizza.BuscarDetallePizza(pizza.idu_tipoproducto,pizza.idu_agrupador,this);

        TextView lvl_pizza = findViewById(R.id.lvl_pizza);
        lvl_pizza.setText(pizza.nom_producto);



        ImageView img_pizza = findViewById(R.id.img_pizza);
        Glide.with(this)
                .load("https://new.pizzeta.com.mx/assets/images/products-300/" + pizza.des_img)
                .thumbnail(0.1f)
                .into(img_pizza);






        LinearLayout btn_comprar = findViewById(R.id.btn_comprar);
        btn_comprar.setOnClickListener(v -> {

            String cPerfil = Helpers.getPrefe("perfil");
            if(cPerfil == null || cPerfil.length() == 0)
            {
                MostrarAlertaRegistro();
            }
            else
            {
                if(tamanos.size() == 0 || tamanos == null)
                {
                    MostrarAlerta("Ups","Para continuar selecciona un tamaño");
                }
                else
                {
                    tamanos.get(0).setNum_cantidad(cont);
                    tamanos.get(0).setImp_total(cont*Integer.parseInt(tamanos.get(0).imp_precio));

                    pizza.setTamanios(tamanos);
                    lstPizzas.add(pizza);
                    Helpers.setPrefe("lstPizzas",gson.toJson(lstPizzas));
                    Log.d("lstPizzas",Helpers.getPrefe("lstPizzas","[]"));
                    Intent intent =  new Intent(PizzaDetalleActivity.this, ComplementosActivity.class);
                    startActivity(intent);
                    finish();
                }

            }

        });



        btn_cerrarAlerta = findViewById(R.id.btn_cerrarAlerta);
        backgroud_alerta = findViewById(R.id.backgroud_alerta);
        bottomSheet = findViewById(R.id.bottom_sheet);
        mBottomSheetBehavior = BottomSheetBehavior.from(this.bottomSheet);
        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(View bottomSheet, int newState) {

                switch (newState) {

                    case BottomSheetBehavior.STATE_DRAGGING:

                        break;

                    case BottomSheetBehavior.STATE_COLLAPSED:
                        backgroud_alerta.setVisibility(View.GONE);

                        break;

                    case BottomSheetBehavior.STATE_EXPANDED:
                        break;
                }

            }

            @Override
            public void onSlide(View bottomSheet, float slideOffset) {

            }
        });

        btn_cerrarAlerta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backgroud_alerta.setVisibility(View.GONE);
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });



        btn_cerrarAlerta_registro = findViewById(R.id.btn_cerrarAlerta_registro);
        btn_registro = findViewById(R.id.btn_registro);
        bottomSheet_registro = findViewById(R.id.bottom_sheet_registro);
        mBottomSheetBehavior_registro = BottomSheetBehavior.from(this.bottomSheet_registro);
        mBottomSheetBehavior_registro.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(View bottomSheet, int newState) {

                switch (newState) {

                    case BottomSheetBehavior.STATE_DRAGGING:

                        break;

                    case BottomSheetBehavior.STATE_COLLAPSED:
                        backgroud_alerta.setVisibility(View.GONE);

                        break;

                    case BottomSheetBehavior.STATE_EXPANDED:
                        break;
                }

            }

            @Override
            public void onSlide(View bottomSheet, float slideOffset) {

            }
        });

        btn_cerrarAlerta_registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backgroud_alerta.setVisibility(View.GONE);
                mBottomSheetBehavior_registro.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });

        btn_registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                backgroud_alerta.setVisibility(View.GONE);
                mBottomSheetBehavior_registro.setState(BottomSheetBehavior.STATE_COLLAPSED);

                Intent intent =  new Intent(PizzaDetalleActivity.this, RegistroActivity.class);
                startActivity(intent);

            }
        });


        switch (pizza.tamanios.size())
        {
            case 1:
                setTamanio(0);
                break;
            case 2:
                //mostar tamanos complementos
                LinearLayout container_complementos = findViewById(R.id.container_complementos);
                container_complementos.setVisibility(View.VISIBLE);



                CardView card_t1 = findViewById(R.id.card_t1);
                FrameLayout frame_t1 = findViewById(R.id.frame_t1);
                TextView lvl_t1= findViewById(R.id.lvl_t1);
                lvl_t1.setText(pizza.tamanios.get(0).nom_tamano);


                CardView card_t2 = findViewById(R.id.card_t2);
                FrameLayout frame_t2 = findViewById(R.id.frame_t2);
                TextView lvl_t2= findViewById(R.id.lvl_t2);
                lvl_t2.setText(pizza.tamanios.get(1).nom_tamano);

                card_t1.setOnClickListener(v -> {
                    frame_t1.setBackgroundResource(R.drawable.card_edge_rojo);
                    frame_t2.setBackgroundResource(R.drawable.card_edge_black);
                    setTamanio(0);
                });
                card_t2.setOnClickListener(v -> {
                    frame_t1.setBackgroundResource(R.drawable.card_edge_black);
                    frame_t2.setBackgroundResource(R.drawable.card_edge_rojo);
                    setTamanio(1);
                });


                break;
            case 3:
                //mostar tamanos pizza
                LinearLayout container_pizzas = findViewById(R.id.container_pizzas);
                container_pizzas.setVisibility(View.VISIBLE);

                //set labels
                lvl_companera.setText(pizza.tamanios.get(0).nom_tamano);
                lvl_familiar.setText(pizza.tamanios.get(1).nom_tamano);
                lvl_pizzota.setText(pizza.tamanios.get(2).nom_tamano);
                break;
        }




    }

    @Override
    public void onSuccess_GetDetallePizza(Response_GetDetallePizza result, int nTipoProducto) {
        Helpers.closeModal(this);
        Log.d("GetDetallePizza",gson.toJson(result));

        if(result.data.productos.get(0).ingredientes.size() == 0)
        {
            //ocultar lvl

        }
        else
        {
            TextView lvl_ingredientes = findViewById(R.id.lvl_ingredientes);
            lvl_ingredientes.setVisibility(View.VISIBLE);
            RecyclerView recyclerView = findViewById(R.id.recycler_ingredientes);
            LinearLayoutManager lm = new LinearLayoutManager(this);
            Adapter_ingrediente adapter_ingrediente = new Adapter_ingrediente(this,result.data.productos.get(0).ingredientes);
            recyclerView.setLayoutManager(lm);
            recyclerView.setAdapter(adapter_ingrediente);
        }

    }

    @Override
    public void onFail_GetDetallePizza(String result) {
        Helpers.closeModal(this);
        //mostrar alerta
    }


    public void setTamanio(int index)
    {
        tamanos = new ArrayList<>();
        tamano item = pizza.tamanios.get(index);
        tamanos.add(item);
        TextView lvl_precio = findViewById(R.id.lvl_precio);
        lvl_precio.setText("$" + item.imp_precio);
        lvl_descripcion_tamanio.setText(item.des_tamano );

        switch (index)
        {
            case 0:
                frame_companera.setBackgroundResource(R.drawable.card_edge_rojo);
                frame_familiar.setBackgroundResource(R.drawable.card_edge_black);
                frame_pizzota.setBackgroundResource(R.drawable.card_edge_black);
                break;
            case 1:
                frame_companera.setBackgroundResource(R.drawable.card_edge_black);
                frame_familiar.setBackgroundResource(R.drawable.card_edge_rojo);
                frame_pizzota.setBackgroundResource(R.drawable.card_edge_black);
                break;
            case 2:
                frame_companera.setBackgroundResource(R.drawable.card_edge_black);
                frame_familiar.setBackgroundResource(R.drawable.card_edge_black);
                frame_pizzota.setBackgroundResource(R.drawable.card_edge_rojo);
                break;
        }
    }


    public void MostrarAlerta(String cTitulo, String cMensaje)
    {
        TextView lvl_tituloMensaje = findViewById(R.id.lvl_tituloMensaje);
        TextView lvl_mensaje = findViewById(R.id.lvl_mensaje);

        lvl_tituloMensaje.setText(cTitulo);
        lvl_mensaje.setText(cMensaje);

        backgroud_alerta.setVisibility(View.VISIBLE);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    public void MostrarAlertaRegistro()
    {

        backgroud_alerta.setVisibility(View.VISIBLE);
        mBottomSheetBehavior_registro.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    public void UpdateCont(boolean bSuma)
    {
        if(bSuma)
        {
            if(cont == limite)
            {
                return;
            }
            else
            {
                cont += 1;
            }

        }
        else
        {
            if(cont == 1)
            {
                return;
            }
            else
            {
                cont -= 1;
            }
        }


        lvl_cont.setText("" + cont);


    }


    @Override
    public void onResume(){
        super.onResume();
        // put your code here...
        Helpers.setPrefe("idu_tipopedido","0");

    }
}
