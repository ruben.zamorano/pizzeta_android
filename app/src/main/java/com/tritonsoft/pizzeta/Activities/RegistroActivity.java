package com.tritonsoft.pizzeta.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.tritonsoft.pizzeta.DTO.DTO_Perfil;
import com.tritonsoft.pizzeta.Helpers.Helpers;
import com.tritonsoft.pizzeta.R;

public class RegistroActivity extends AppCompatActivity {

    public EditText ed_nombre;
    public EditText ed_mail;
    public EditText ed_telefono;

    public final Gson gson = new Gson();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);


        ed_nombre = findViewById(R.id.ed_nombre);
        ed_mail = findViewById(R.id.ed_mail);
        ed_telefono = findViewById(R.id.ed_telefono);



        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {

            boolean bPantallaPerfil = bundle.getBoolean("bPantallaPerfil",false);
            if(bPantallaPerfil)
            {
                DTO_Perfil perfil = gson.fromJson(Helpers.getPrefe("perfil"),DTO_Perfil.class);
                ed_nombre.setText(perfil.cNombres);
                ed_telefono.setText(perfil.cTelefono);
                ed_mail.setText(perfil.cMail);

            }





        }


        LinearLayout btn_guardar = findViewById(R.id.btn_guardar);
        btn_guardar.setOnClickListener(v -> {

            DTO_Perfil perfil = new DTO_Perfil("","","");
            boolean bError = false;
            if(ed_nombre.length() < 3)
            {
                ed_nombre.setError("Campo obligatorio");
                bError = true;
            }
            else
            {

                ed_nombre.setError(null);
                perfil.setcNombres(ed_nombre.getText().toString());
            }

            if(!ed_mail.getText().toString().contains("@"))
            {
                ed_mail.setError("Campo obligatorio");
                bError = true;
            }
            else
            {

                ed_mail.setError(null);
                perfil.setcMail(ed_mail.getText().toString());
            }

            if(ed_telefono.getText().length() != 10)
            {
                ed_telefono.setError("Campo obligatorio");
                bError = true;
            }
            else
            {

                ed_telefono.setError(null);
                perfil.setcTelefono(ed_telefono.getText().toString());
            }



            if(!bError)
            {
                Helpers.setPrefe("perfil",gson.toJson(perfil));
                finish();
            }


        });


    }

}
