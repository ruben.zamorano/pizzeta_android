package com.tritonsoft.pizzeta.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tritonsoft.pizzeta.Adapters.Adapter_carrito;
import com.tritonsoft.pizzeta.Adapters.Adapter_historial;
import com.tritonsoft.pizzeta.DTO.DTO_Carrito;
import com.tritonsoft.pizzeta.Helpers.Helpers;
import com.tritonsoft.pizzeta.R;
import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.producto;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class HistorialActivity extends AppCompatActivity {

    public final Gson gson = new Gson();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historial);

        Helpers.setToolbar(this,"Historial de pedidos");

        List<DTO_Carrito> lstPedidos = ObtenerPedidos();

        RecyclerView recyclerView = findViewById(R.id.recycler_pedidos);
        LinearLayoutManager lm = new LinearLayoutManager(this);
        Adapter_historial adapter_carrito = new Adapter_historial(this,lstPedidos);
        recyclerView.setLayoutManager(lm);
        recyclerView.setAdapter(adapter_carrito);
        recyclerView.setNestedScrollingEnabled(false);

    }



    public List<DTO_Carrito> ObtenerPedidos()
    {
        List<DTO_Carrito> lstCarrito = new ArrayList<>();
        try {
            Type listType = new TypeToken<ArrayList<DTO_Carrito>>(){}.getType();
            String clstPizzas = Helpers.getPrefe("lstCarrito");
            if(clstPizzas == null || clstPizzas.length() == 0)
            {
                lstCarrito = new ArrayList<>();
            }
            else
            {
                lstCarrito = gson.fromJson(clstPizzas, listType);
            }

        }
        catch (Exception e)
        {
            lstCarrito = new ArrayList<>();
        }

        return lstCarrito;
    }
}
