package com.tritonsoft.pizzeta.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.solver.widgets.Helper;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;

import com.tritonsoft.pizzeta.Adapters.PizzaGridAdapter;
import com.tritonsoft.pizzeta.Helpers.Helpers;
import com.tritonsoft.pizzeta.R;
import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Callback_GetPizzas;
import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Communicator_GetPizzas;
import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.Response_GetPizzas;

public class PizzaActivity extends AppCompatActivity implements Callback_GetPizzas {

    public GridView gridview;
    public PizzaGridAdapter gridAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Helpers.setContext(this);
        setContentView(R.layout.activity_pizza);
        Helpers.setPrefeBool("bMenuPerfil",false);
        Helpers.setToolbar(this,"Pizzas");

        Helpers.showModal(this);
        Communicator_GetPizzas communicator_getPizzas = new Communicator_GetPizzas();
        int nTipoProducto = Integer.parseInt(Helpers.getPrefe("nTipoProducto"));
        communicator_getPizzas.BuscarProductos(nTipoProducto,this);


    }



    @Override
    public void onSuccess_GetPizzas(Response_GetPizzas result, int nTipoProducto) {
        Helpers.closeModal(this);
        if(result.meta.status.contains(""))
        {
            gridview = findViewById(R.id.gridview);
            gridAdapter = new PizzaGridAdapter(this,result.data.menu);
            gridview.setNestedScrollingEnabled(false);
            gridview.setAdapter(gridAdapter);
        }
    }

    @Override
    public void onFail_GetPizzas(String result) {
        Helpers.closeModal(this);
    }

    public void onClickPizza()
    {
        Intent intent = new Intent(this,PizzaDetalleActivity.class);
        startActivity(intent);
    }


}
