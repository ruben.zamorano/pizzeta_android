package com.tritonsoft.pizzeta.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.solver.widgets.Helper;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tritonsoft.pizzeta.Adapters.Adapter_carrito;
import com.tritonsoft.pizzeta.Adapters.Adapter_extras;
import com.tritonsoft.pizzeta.Adapters.ExtrasGridAdapter;
import com.tritonsoft.pizzeta.Adapters.PizzaGridAdapter;
import com.tritonsoft.pizzeta.Helpers.Helpers;
import com.tritonsoft.pizzeta.R;
import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Callback_GetPizzas;
import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Communicator_GetPizzas;
import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.Response_GetPizzas;
import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.producto;
import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.tamano;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class ComplementosActivity extends AppCompatActivity implements Callback_GetPizzas {



    public TextView lvl_total;
    public final Gson gson = new Gson();


    //bottom sheet alerta
    public RelativeLayout backgroud_alerta;
    public BottomSheetBehavior mBottomSheetBehavior;
    private View bottomSheet;
    public CardView btn_cerrarAlerta;


    //bottom sheet tamanio
    public BottomSheetBehavior mBottomSheetBehavior_tamano;
    private View bottomSheet_tamano;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complementos);
        Helpers.setContext(this);

        lvl_total = findViewById(R.id.lvl_total);

        Helpers.setToolbar(this,"Extras");
        Helpers.showModal(this);

        Communicator_GetPizzas communicator_extras = new Communicator_GetPizzas();
        communicator_extras.BuscarProductos(3,this);//bebidas
        communicator_extras.BuscarProductos(2,this);//complementos
        communicator_extras.BuscarProductos(5,this);//ensaladas
        communicator_extras.BuscarProductos(4,this);//postres


        LinearLayout btn_comprar = findViewById(R.id.btn_comprar);
        btn_comprar.setOnClickListener(v ->{
            onCLickSiguiente();
        });


        btn_cerrarAlerta = findViewById(R.id.btn_cerrarAlerta);
        backgroud_alerta = findViewById(R.id.backgroud_alerta);
        bottomSheet = findViewById(R.id.bottom_sheet);
        mBottomSheetBehavior = BottomSheetBehavior.from(this.bottomSheet);
        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(View bottomSheet, int newState) {

                switch (newState) {

                    case BottomSheetBehavior.STATE_DRAGGING:

                        break;

                    case BottomSheetBehavior.STATE_COLLAPSED:
                        backgroud_alerta.setVisibility(View.GONE);

                        break;

                    case BottomSheetBehavior.STATE_EXPANDED:
                        break;
                }

            }

            @Override
            public void onSlide(View bottomSheet, float slideOffset) {

            }
        });

        btn_cerrarAlerta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backgroud_alerta.setVisibility(View.GONE);
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });

        //bottom sheet tamano
        bottomSheet_tamano = findViewById(R.id.bottom_sheet_tamano);
        mBottomSheetBehavior_tamano = BottomSheetBehavior.from(this.bottomSheet_tamano);
        mBottomSheetBehavior_tamano.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(View bottomSheet, int newState) {

                switch (newState) {

                    case BottomSheetBehavior.STATE_DRAGGING:

                        break;

                    case BottomSheetBehavior.STATE_COLLAPSED:
                        backgroud_alerta.setVisibility(View.GONE);

                        break;

                    case BottomSheetBehavior.STATE_EXPANDED:
                        break;
                }

            }

            @Override
            public void onSlide(View bottomSheet, float slideOffset) {

            }
        });






    }


    @Override
    public void onSuccess_GetPizzas(Response_GetPizzas result, int nTipoProducto) {
        if(result.meta.status.contains("SUCCESS"))
        {
            LinearLayoutManager lm = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
            switch (nTipoProducto)
            {

                case 2:
                    RecyclerView recycler_complementos = findViewById(R.id.recycler_complementos);

                    Adapter_extras adapter_complementos = new Adapter_extras(this,result.data.menu);
                    recycler_complementos.setLayoutManager(lm);
                    recycler_complementos.setAdapter(adapter_complementos);

                    break;
                case 3:


                    RecyclerView recycler_bebidas = findViewById(R.id.recycler_bebidas);
                    Adapter_extras adapter_bebidas = new Adapter_extras(this,result.data.menu);
                    recycler_bebidas.setLayoutManager(lm);
                    recycler_bebidas.setAdapter(adapter_bebidas);
                    break;
                case 4:
                    RecyclerView recycler_postres = findViewById(R.id.recycler_postres);
                    Adapter_extras adapter_postres = new Adapter_extras(this,result.data.menu);
                    recycler_postres.setLayoutManager(lm);
                    recycler_postres.setAdapter(adapter_postres);
                    break;
                case 5:
                    RecyclerView recycler_ensaladas = findViewById(R.id.recycler_ensaladas);
                    Adapter_extras adapter_ensaladas = new Adapter_extras(this,result.data.menu);
                    recycler_ensaladas.setLayoutManager(lm);
                    recycler_ensaladas.setAdapter(adapter_ensaladas);
                    Helpers.closeModal(this);
                    break;
            }

        }
    }

    @Override
    public void onFail_GetPizzas(String result) {

    }

    public void onClickPizza()
    {
//        Intent intent = new Intent(this,PizzaDetalleActivity.class);
//        startActivity(intent);
    }

    public void onCLickSiguiente()
    {
        Intent intent = new Intent(this,SeleccionarDomicilioActivity.class);
        startActivity(intent);
    }


    public void CalcularTotal(List<producto> items)
    {
        int subtotal = 0;
        for (producto p: items)
        {
            for (tamano d: p.tamanios)
            {
                subtotal += d.imp_total;
            }
        }
        lvl_total = findViewById(R.id.lvl_total);
        lvl_total.setText("$" + (subtotal ));
    }

    public void BuscarPizzas()
    {

        List<producto> lstPizzas = new ArrayList<>();
        try {
            Type listType = new TypeToken<ArrayList<producto>>(){}.getType();
            String clstPizzas = Helpers.getPrefe("lstPizzas");
            if(clstPizzas == null || clstPizzas.length() == 0)
            {
                lstPizzas = new ArrayList<>();
            }
            else
            {
                lstPizzas = gson.fromJson(clstPizzas, listType);
            }

        }
        catch (Exception e)
        {
            lstPizzas = new ArrayList<>();
        }
        finally {
            CalcularTotal(lstPizzas);
        }
    }


    @Override
    public void onResume(){
        super.onResume();
        // put your code here...
        BuscarPizzas();

    }

    public void MostrarAlerta(String cTitulo, String cMensaje)
    {
        TextView lvl_tituloMensaje = findViewById(R.id.lvl_tituloMensaje);
        TextView lvl_mensaje = findViewById(R.id.lvl_mensaje);

        lvl_tituloMensaje.setText(cTitulo);
        lvl_mensaje.setText(cMensaje);

        backgroud_alerta.setVisibility(View.VISIBLE);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }


    public void MostrarAlertaTamano(producto p)
    {
        TextView lvl_t1 = findViewById(R.id.lvl_t1);
        lvl_t1.setText(p.getTamanios().get(0).nom_tamano);

        TextView lvl_t2 = findViewById(R.id.lvl_t2);
        lvl_t2.setText(p.getTamanios().get(1).nom_tamano);

        ImageView btn_cerrar_tamano = findViewById(R.id.btn_cerrar_tamano);
        btn_cerrar_tamano.setOnClickListener(v ->{
            mBottomSheetBehavior_tamano.setState(BottomSheetBehavior.STATE_COLLAPSED);
        });


        CardView btn_t1 = findViewById(R.id.btn_t1);
        btn_t1.setOnClickListener(v ->{
            tamano t = p.tamanios.get(0);

            t.setNum_cantidad(1);
            t.setImp_total(Integer.parseInt(t.imp_precio));
            List<tamano> tm = new ArrayList<>();
            tm.add(t);

            producto p2 = gson.fromJson(gson.toJson(p),producto.class);
            p2.setTamanios(tm);

            List<producto> lstPizzas = ObtenerPizzas();
            lstPizzas.add(p2);
            Helpers.setPrefe("lstPizzas",gson.toJson(lstPizzas));
            mBottomSheetBehavior_tamano.setState(BottomSheetBehavior.STATE_COLLAPSED);
            MostrarAlerta(p.nom_producto,"Agregado al carrito");
        });

        CardView btn_t2 = findViewById(R.id.btn_t2);
        btn_t2.setOnClickListener(v ->{
            tamano t = p.tamanios.get(1);

            t.setNum_cantidad(1);
            t.setImp_total(Integer.parseInt(t.imp_precio));
            List<tamano> tm = new ArrayList<>();
            tm.add(t);



            producto p2 = gson.fromJson(gson.toJson(p),producto.class);
            p2.setTamanios(tm);

            List<producto> lstPizzas = ObtenerPizzas();
            lstPizzas.add(p2);
            Helpers.setPrefe("lstPizzas",gson.toJson(lstPizzas));
            mBottomSheetBehavior_tamano.setState(BottomSheetBehavior.STATE_COLLAPSED);
            MostrarAlerta(p.nom_producto,"Agregado al carrito");
        });



        backgroud_alerta.setVisibility(View.VISIBLE);
        mBottomSheetBehavior_tamano.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    public List<producto> ObtenerPizzas()
    {
        List<producto> lstPizzas = new ArrayList<>();
        try {
            Type listType = new TypeToken<ArrayList<producto>>(){}.getType();
            String clstPizzas = Helpers.getPrefe("lstPizzas");
            if(clstPizzas == null || clstPizzas.length() == 0)
            {
                lstPizzas = new ArrayList<>();
            }
            else
            {
                lstPizzas = gson.fromJson(clstPizzas, listType);
            }

        }
        catch (Exception e)
        {
            lstPizzas = new ArrayList<>();
        }

        return lstPizzas;
    }



}
