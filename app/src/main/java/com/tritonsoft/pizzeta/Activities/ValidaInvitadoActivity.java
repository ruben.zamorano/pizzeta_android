package com.tritonsoft.pizzeta.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.tritonsoft.pizzeta.R;
import com.tritonsoft.pizzeta.Retrofit.validacorreopruebausuario.Callback_validacorreopruebausuario;
import com.tritonsoft.pizzeta.Retrofit.validacorreopruebausuario.Communicator_validacorreopruebausuario;
import com.tritonsoft.pizzeta.Retrofit.validacorreopruebausuario.JSON_validacorreopruebausuario;
import com.tritonsoft.pizzeta.Retrofit.validacorreopruebausuario.Response.Response_validacorreopruebausuario;

public class ValidaInvitadoActivity extends AppCompatActivity implements Callback_validacorreopruebausuario {


    //bottom sheet alerta
    public RelativeLayout backgroud_alerta;
    public BottomSheetBehavior mBottomSheetBehavior;
    private View bottomSheet;
    public CardView btn_cerrarAlerta;

    public CardView btn_entrar;
    public EditText ed_mail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_valida_invitado);

        ed_mail = findViewById(R.id.ed_mail);
        btn_entrar = findViewById(R.id.btn_entrar);
        btn_entrar.setOnClickListener(v ->{
            if(ed_mail.getText().length() == 0)
            {
                MostrarAlerta("Ups","El correo en necesario para entrar");
                return;
            }
            if(!ed_mail.getText().toString().contains("@"))
            {
                MostrarAlerta("Ups","parece que no es un Correo valido");
                return;
            }

            ValidarInvitado(ed_mail.getText().toString());

        });


        btn_cerrarAlerta = findViewById(R.id.btn_cerrarAlerta);
        backgroud_alerta = findViewById(R.id.backgroud_alerta);
        bottomSheet = findViewById(R.id.bottom_sheet);
        mBottomSheetBehavior = BottomSheetBehavior.from(this.bottomSheet);
        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(View bottomSheet, int newState) {

                switch (newState) {

                    case BottomSheetBehavior.STATE_DRAGGING:

                        break;

                    case BottomSheetBehavior.STATE_COLLAPSED:
                        backgroud_alerta.setVisibility(View.GONE);

                        break;

                    case BottomSheetBehavior.STATE_EXPANDED:
                        break;
                }

            }

            @Override
            public void onSlide(View bottomSheet, float slideOffset) {

            }
        });

        btn_cerrarAlerta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backgroud_alerta.setVisibility(View.GONE);
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });





    }

    public void MostrarAlerta(String cTitulo, String cMensaje)
    {
        TextView lvl_tituloMensaje = findViewById(R.id.lvl_tituloMensaje);
        TextView lvl_mensaje = findViewById(R.id.lvl_mensaje);

        lvl_tituloMensaje.setText(cTitulo);
        lvl_mensaje.setText(cMensaje);

        backgroud_alerta.setVisibility(View.VISIBLE);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    public void ValidarInvitado(String cMail)
    {
        JSON_validacorreopruebausuario body = new JSON_validacorreopruebausuario(cMail);
        Communicator_validacorreopruebausuario communicator_validacorreopruebausuario = new Communicator_validacorreopruebausuario();
        communicator_validacorreopruebausuario.GuardarPedido(body,this);
    }


    @Override
    public void onSuccess_validacorreopruebausuario(Response_validacorreopruebausuario result) {
        //Helpers.closeModal(this);
        Log.d("validacorreopruebausuar",new Gson().toJson(result));
        //result.data.response.opc_puedecomprar = true;

        if(result.data.response.opc_puedecomprar)
        {
            Intent mainIntent = new Intent().setClass(ValidaInvitadoActivity.this, PrincipalActivity.class);
            mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(mainIntent);
            finish();
        }
        else
        {
            MostrarAlerta("Ups","Tu correo no esta dentro de la lista de invitados");
        }



    }

    @Override
    public void onFail_validacorreopruebausuario(String result) {
        //Helpers.closeModal(this);
    }

}
