package com.tritonsoft.pizzeta.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tritonsoft.pizzeta.Adapters.Adapter_carrito;
import com.tritonsoft.pizzeta.Adapters.Adapter_direcciones;
import com.tritonsoft.pizzeta.DTO.DTO_Domicilio;
import com.tritonsoft.pizzeta.Helpers.Helpers;
import com.tritonsoft.pizzeta.R;
import com.tritonsoft.pizzeta.Retrofit.GetSucursales.Callback_GetSucursales;
import com.tritonsoft.pizzeta.Retrofit.GetSucursales.Communicator_GetSucursales;
import com.tritonsoft.pizzeta.Retrofit.GetSucursales.Response.Response_GetSucursales;
import com.tritonsoft.pizzeta.Retrofit.GetSucursales.Response.marker;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class SeleccionarDomicilioActivity extends AppCompatActivity implements Callback_GetSucursales {


    public List<DTO_Domicilio> lstDirecciones;
    public final Gson gson = new Gson();

    //bottom sheet alerta
    public RelativeLayout backgroud_alerta;
    public BottomSheetBehavior mBottomSheetBehavior;
    private View bottomSheet;
    public CardView btn_cerrarAlerta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seleccionar_domicilio);
        Helpers.setContext(this);


        //tipo de pedido
        Helpers.setPrefe("opc_programado","0");
        Helpers.setPrefe("fec_programado","");



        boolean bMenuPerfil = Helpers.getPrefeBool("bMenuPerfil",false);
        Helpers.setToolbar(this,bMenuPerfil ? "Mis direcciones" : "Entrega de pedido");


        CardView btn_agregar_domicilio = findViewById(R.id.btn_agregar_domicilio);
        btn_agregar_domicilio.setOnClickListener(v -> {
            Intent intent = new Intent(this,NuevoDomicilioActivity.class);
            startActivity(intent);
        });

        CardView btn_recojer = findViewById(R.id.btn_recojer);
        btn_recojer.setOnClickListener(v -> {
            Intent intent = new Intent(this,SucursalesActivity.class);
            startActivity(intent);
        });
        if (bMenuPerfil)
        {
            btn_recojer.setVisibility(View.GONE);
        }

        BuscarDirecciones();

        btn_cerrarAlerta = findViewById(R.id.btn_cerrarAlerta);
        backgroud_alerta = findViewById(R.id.backgroud_alerta);
        bottomSheet = findViewById(R.id.bottom_sheet);
        mBottomSheetBehavior = BottomSheetBehavior.from(this.bottomSheet);
        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(View bottomSheet, int newState) {

                switch (newState) {

                    case BottomSheetBehavior.STATE_DRAGGING:

                        break;

                    case BottomSheetBehavior.STATE_COLLAPSED:
                        backgroud_alerta.setVisibility(View.GONE);

                        break;

                    case BottomSheetBehavior.STATE_EXPANDED:
                        break;
                }

            }

            @Override
            public void onSlide(View bottomSheet, float slideOffset) {

            }
        });

        btn_cerrarAlerta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backgroud_alerta.setVisibility(View.GONE);
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });




    }

    public void InicializarRecycler(List<DTO_Domicilio> mItems)
    {
        Log.d("lstDirecciones",gson.toJson(mItems));
        RecyclerView recyclerView = findViewById(R.id.recycler_direcciones);
        LinearLayoutManager lm = new LinearLayoutManager(this);
        Adapter_direcciones adapter = new Adapter_direcciones(this,mItems);
        recyclerView.setLayoutManager(lm);
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
    }

    public void BuscarDirecciones()
    {
        try
        {
            Type listType = new TypeToken<ArrayList<DTO_Domicilio>>(){}.getType();
            String clstDirecciones = Helpers.getPrefe("lstDirecciones");
            if(clstDirecciones == null || clstDirecciones.length() == 0)
            {
                lstDirecciones = new ArrayList<>();
            }
            else
            {
                lstDirecciones = gson.fromJson(clstDirecciones, listType);
            }


        }
        catch (Exception e)
        {
            lstDirecciones = new ArrayList<>();
        }
        finally {
            InicializarRecycler(lstDirecciones);
        }
    }


    @Override
    public void onResume(){
        super.onResume();
        // put your code here...
        Helpers.setPrefe("idu_tipopedido","0");
        BuscarDirecciones();

    }

    public void BuscarSucursales(double lat, double lng)
    {
        Helpers.showModal(this);
        Communicator_GetSucursales communicator_getSucursales = new Communicator_GetSucursales();
        communicator_getSucursales.BuscarSucursales(lat + "",lng + "","4",this);
    }

    @Override
    public void onSuccess_GetSucursales(Response_GetSucursales result) {
        Helpers.closeModal(this);

        if(result.data.getResponse().size() == 0)
        {
            //no hay sucursales cerca de tu direccion
            MostrarAlerta("Ups","No hay sucursales cerca de tu direccion");
        }
        else {
            boolean bSucursalAbierta = true;

            List<marker> lstSucursalesDom = new ArrayList<>();

            for (marker m : result.data.getResponse()) {
                if (Integer.parseInt(m.serdom) == 1 && Integer.parseInt(m.sucabierta) == 1) {
                    lstSucursalesDom.add(m);
                }
            }


            Log.d("lstSucursalesDom_usr", gson.toJson(lstSucursalesDom));
            Log.d("lstSucursalesDom", gson.toJson(result));


//            for (marker m: lstSucursalesDom)
//            {
//                if (Integer.parseInt(m.sucabierta) == 0)
//                {
//                    bSucursalAbierta = false;
//                }
//            }


//            bSucursalAbierta = true;


            if (lstSucursalesDom.size() > 0)
            {
                Helpers.setPrefe("idu_sucursal",lstSucursalesDom.get(0).id);
                Helpers.setPrefe("idu_tipopedido","2");
                Helpers.setPrefe("envio",lstSucursalesDom.get(0).impserdom);
                Helpers.setPrefe("sucursal",gson.toJson(lstSucursalesDom.get(0)));
                Helpers.setPrefeBool("bCarritoVacio",false);
                startActivity(new Intent(this, FormaPagoActivity.class));
            }
            else
            {
                //por el momento nuestras sucursales se encuentran cerradas
                MostrarAlerta("Ups","Por el momento no tenemos cobertura para tu direccion");
            }

        }
    }

    @Override
    public void onFail_GetSucursales(String result) {
        Helpers.closeModal(this);
        Helpers.setPrefe("direccion","");
    }

    public void MostrarAlerta(String cTitulo, String cMensaje)
    {
        TextView lvl_tituloMensaje = findViewById(R.id.lvl_tituloMensaje);
        TextView lvl_mensaje = findViewById(R.id.lvl_mensaje);

        lvl_tituloMensaje.setText(cTitulo);
        lvl_mensaje.setText(cMensaje);

        backgroud_alerta.setVisibility(View.VISIBLE);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

}
