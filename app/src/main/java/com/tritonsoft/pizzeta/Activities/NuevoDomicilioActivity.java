package com.tritonsoft.pizzeta.Activities;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tritonsoft.pizzeta.DTO.DTO_Domicilio;
import com.tritonsoft.pizzeta.Fragments.SucursalesFragment;
import com.tritonsoft.pizzeta.Helpers.GPSTracker;
import com.tritonsoft.pizzeta.Helpers.Helpers;
import com.tritonsoft.pizzeta.R;
import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.producto;
import com.tritonsoft.pizzeta.Retrofit.GetSucursales.Communicator_GetSucursales;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.Manifest.permission_group.LOCATION;

public class NuevoDomicilioActivity extends AppCompatActivity implements OnMapReadyCallback {

    public final Gson gson = new Gson();
    public RelativeLayout ly_direccion;
    public EditText ed_titulo;
    public EditText ed_ext;
    public EditText ed_int;
    public EditText ed_direccion;
    public EditText ed_cp;

    public EditText ed_buscar;
    public CardView btn_guardar_direcion;


    //bottom sheet alerta
    public RelativeLayout backgroud_alerta;
    public BottomSheetBehavior mBottomSheetBehavior;
    private View bottomSheet;
    public CardView btn_cerrarAlerta;
    public CardView card_buscar;
    public CardView btn_cancelar;
    public CardView btn_guardar_dir;


    MapView mapView;
    GoogleMap map;
    Bundle intancia;
    public DTO_Domicilio insert;
    public List<DTO_Domicilio> lstDirecciones;
    public boolean bModalDireccion = false;
    RelativeLayout ly_mensaje;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo_domicilio);

        card_buscar = findViewById(R.id.card_buscar);
        btn_cancelar = findViewById(R.id.btn_cancelar);
        btn_cancelar.setOnClickListener(v ->{
            OcultarFormulario();
        });
        btn_guardar_dir = findViewById(R.id.btn_guardar_dir);
        btn_guardar_dir.setOnClickListener(v ->{
            guardarDireccion();
        });

        ImageView btn_cerrar = findViewById(R.id.btn_cerrar);
        btn_cerrar.setOnClickListener(v -> {
            finish();
        });



        intancia = savedInstanceState;
        mapView = (MapView) findViewById(R.id.mapview);
        ly_mensaje = findViewById(R.id.ly_mensaje);
//        mapView.setVisibility(View.GONE);

        final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );
        String[] permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION,LOCATION};
        CardView btn_gps = findViewById(R.id.btn_gps);
        btn_gps.setOnClickListener(view -> {
            if (!manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
                //buildAlertMessageNoGps();
                //InicializarMapa();
            }
            else
            {
                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,  permissions[0])) {

                    //This is called if user has denied the permission before
                    //In this case I am just asking the permission again
                    ActivityCompat.requestPermissions(this, permissions, 1);

                } else {

                    ActivityCompat.requestPermissions(this,permissions, 1);
                }
            }

        });

        if (ContextCompat.checkSelfPermission(this, permissions[0]) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,  permissions[0])) {

                //This is called if user has denied the permission before
                //In this case I am just asking the permission again
                ActivityCompat.requestPermissions(this, permissions, 1);

            } else {

                ActivityCompat.requestPermissions(this,permissions, 1);
            }
        }
        else
        {


            if (manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
//                InicializarMapa();
                InicializarMapa();
            }

        }



        //
        ly_direccion = findViewById(R.id.ly_direccion);
        ed_titulo = findViewById(R.id.ed_titulo);
        ed_ext = findViewById(R.id.ed_ext);
        ed_int = findViewById(R.id.ed_int);
        ed_direccion = findViewById(R.id.ed_direccion);
        ed_cp = findViewById(R.id.ed_cp);


        ed_buscar = findViewById(R.id.ed_buscar);
        ImageView btn_clear = findViewById(R.id.btn_clear);
        btn_clear.setOnClickListener(v -> {
            ed_buscar.setText("");
            btn_guardar_direcion.setVisibility(View.GONE);
        });
        ImageView btn_buscar = findViewById(R.id.btn_buscar);
        btn_buscar.setOnClickListener(v ->{
            CerrarTeclado();
            ValidarDireccion();
        });




        btn_cerrarAlerta = findViewById(R.id.btn_cerrarAlerta);
        backgroud_alerta = findViewById(R.id.backgroud_alerta);
        bottomSheet = findViewById(R.id.bottom_sheet);
        mBottomSheetBehavior = BottomSheetBehavior.from(this.bottomSheet);
        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(View bottomSheet, int newState) {

                switch (newState) {

                    case BottomSheetBehavior.STATE_DRAGGING:

                        break;

                    case BottomSheetBehavior.STATE_COLLAPSED:
                        backgroud_alerta.setVisibility(View.GONE);

                        break;

                    case BottomSheetBehavior.STATE_EXPANDED:
                        break;
                }

            }

            @Override
            public void onSlide(View bottomSheet, float slideOffset) {

            }
        });

        btn_cerrarAlerta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backgroud_alerta.setVisibility(View.GONE);
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });

        btn_guardar_direcion = findViewById(R.id.btn_guardar_direcion);
        btn_guardar_direcion.setOnClickListener(v ->{
            MostrarFormulario();
        });

    }



    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.getUiSettings().setMyLocationButtonEnabled(false);

        // Updates the location and zoom of the MapView
        GPSTracker tracker = new GPSTracker(this);

        LatLng sydney = new LatLng(tracker.getLatitude(), tracker.getLongitude());
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, 18.0f));
        Buscar_Coord(tracker.getLatitude(), tracker.getLongitude());


        MarkerOptions markerOption = new MarkerOptions().position(sydney);
        markerOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        markerOption.title("Casa");
        markerOption.draggable(true);

        map.addMarker(markerOption);


        map.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker arg0) {
                // TODO Auto-generated method stub
                CerrarTeclado();
//                        Log.d("System out", "onMarkerDragStart..."+arg0.getPosition().latitude+"..."+arg0.getPosition().longitude);
            }

            @SuppressWarnings("unchecked")
            @Override
            public void onMarkerDragEnd(Marker arg0) {
                // TODO Auto-generated method stub
//          Log.d("System out", "onMarkerDragEnd..."+arg0.getPosition().latitude+"..."+arg0.getPosition().longitude);

                map.animateCamera(CameraUpdateFactory.newLatLng(arg0.getPosition()));

                Buscar_Coord(arg0.getPosition().latitude, arg0.getPosition().longitude);

//                Geocoder geoCoder = new Geocoder(NuevoDomicilioActivity.this);
//                List<Address> matches = null;
//                try {
//                    matches = geoCoder.getFromLocation(arg0.getPosition().latitude, arg0.getPosition().longitude, 1);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                Address bestMatch = (matches.isEmpty() ? null : matches.get(0));
//                Log.d("onMarkerDragEnd", bestMatch.getAddressLine(0));
//
//                String[] lstDireccion = bestMatch.getAddressLine(0).split("\\,");
//                ed_buscar.setText(lstDireccion[0]);
            }

            @Override
            public void onMarkerDrag(Marker arg0) {
                // TODO Auto-generated method stub
//                        Log.i("System out", "onMarkerDrag...");
            }
        });



        ed_buscar.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    //submit_btn.performClick();

//                            MostrarLoader();

                    Log.d("Buscar domicilio", ed_buscar.getText().toString());

                    CerrarTeclado();
                    Handler handler = new Handler();
                    handler.postDelayed( NuevoDomicilioActivity.this::ValidarDireccion,300);






                    return false;
                }
                return false;
            }
        });

    }


    @Override
    public void onResume() {
        try {
            mapView.onResume();
        }
        catch (Exception e)
        {

        }

        super.onResume();
    }


    @Override
    public void onPause() {
        super.onPause();
        try {
            mapView.onPause();
        }
        catch (Exception e)
        {

        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            mapView.onDestroy();
        }
        catch (Exception e)
        {

        }

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        try {
            mapView.onLowMemory();
        }
        catch (Exception e)
        {

        }

    }

    public void InicializarMapa()
    {
        ly_mensaje.setVisibility(View.GONE);
        mapView.onCreate(intancia);
        mapView.getMapAsync(this);

    }


    public void ValidarDireccion()
    {
        if(ed_buscar.getText().length() < 10)
        {
            MostrarAlerta("Mensaje", "Parece que no es una direccion correcta");
            btn_guardar_direcion.setVisibility(View.GONE);
        }
        else
        {
            BuscarDireccion(ed_buscar.getText().toString(),map);
        }
    }

    public void CerrarTeclado() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void BuscarDireccion(String cDireccion, GoogleMap mMap)
    {
        try {
            Geocoder geoCoder = new Geocoder( NuevoDomicilioActivity.this);
            List<Address> matches = null;
            try {
                matches = geoCoder.getFromLocationName(cDireccion + " " + Helpers.getPrefe("ciudad",""), 1);
            } catch (IOException e) {
                e.printStackTrace();
                MostrarAlerta("Mensaje", "Parece que no es una direccion correcta");
            }
            Address bestMatch = (matches.isEmpty() ? null : matches.get(0));
            if(bestMatch == null)
            {
                MostrarAlerta("Mensaje", "Parece que no es una direccion correcta");
                btn_guardar_direcion.setVisibility(View.GONE);
            }
            else
            {

                mMap.clear();
                LatLng domi = new LatLng(bestMatch.getLatitude(), bestMatch.getLongitude());
                MarkerOptions markerOption = new MarkerOptions().position(domi);

//                            googlemap.addMarker(new MarkerOptions()
//                                    .position(new LatLng( 65.07213,-2.109375))
//                                    .title("This is my title")
//                                    .snippet("and snippet")
//                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));

                markerOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));

                markerOption.title("Casa");
                markerOption.draggable(true);

                mMap.addMarker(markerOption);
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(domi, 18.0f));

                //add_domicilio = new DTO_DomicilioF("", ed_domicilio.getText().toString()
                // , domi.latitude, domi.longitude);
                //

                btn_guardar_direcion.setVisibility(View.VISIBLE);
                setFormulario(bestMatch);

                Log.d("bestMatch", gson.toJson(bestMatch));
            }


        } catch (Exception e) {
            //MostrarAlerta("Mensaje", "No encontramos esta direccion");
        } finally {
            //OcultarLoader();
        }
    }


    public void MostrarAlerta(String cTitulo, String cMensaje)
    {
        TextView lvl_tituloMensaje = findViewById(R.id.lvl_tituloMensaje);
        TextView lvl_mensaje = findViewById(R.id.lvl_mensaje);

        lvl_tituloMensaje.setText(cTitulo);
        lvl_mensaje.setText(cMensaje);

        backgroud_alerta.setVisibility(View.VISIBLE);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }


    public void MostrarFormulario()
    {
        bModalDireccion = true;
        card_buscar.setVisibility(View.GONE);
        btn_guardar_direcion.setVisibility(View.GONE);
        mapView.setVisibility(View.GONE);
        ly_direccion.setVisibility(View.VISIBLE);
    }

    public void OcultarFormulario()
    {
        bModalDireccion = false;
        clearFormulario();
        card_buscar.setVisibility(View.VISIBLE);
        btn_guardar_direcion.setVisibility(View.VISIBLE);
        mapView.setVisibility(View.VISIBLE);
        ly_direccion.setVisibility(View.GONE);
    }


    public void Buscar_Coord(double latitude, double longitude)
    {
        Geocoder geoCoder = new Geocoder(NuevoDomicilioActivity.this);
        List<Address> matches = null;
        try {
            matches = geoCoder.getFromLocation(latitude, longitude, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Address bestMatch = (matches.isEmpty() ? null : matches.get(0));

        if(bestMatch != null)
        {
            Log.d("bestMatch", gson.toJson(bestMatch));
            String[] lstDireccion = bestMatch.getAddressLine(0).split("\\,");
            ed_buscar.setText(lstDireccion[0]);
            btn_guardar_direcion.setVisibility(View.VISIBLE);

            setFormulario(bestMatch);
        }
        else
        {
            btn_guardar_direcion.setVisibility(View.GONE);
        }

    }

    public void setFormulario(Address bestMatch)
    {
        insert = new DTO_Domicilio();
        insert.geo = bestMatch;
        insert.lat = bestMatch.getLatitude();
        insert.lng = bestMatch.getLongitude();

        String[] lstDireccion = bestMatch.getAddressLine(0).split("\\,");
        ed_titulo.setText("");
        ed_ext.setText(bestMatch.getSubThoroughfare() == null ? "" : bestMatch.getSubThoroughfare() );
        ed_int.setText("");
        ed_direccion.setText(lstDireccion[0]);
        ed_direccion.setFocusable(false);
        ed_cp.setText(bestMatch.getPostalCode() == null ? "" : bestMatch.getPostalCode());



    }
    public void clearFormulario()
    {
        ed_titulo.setText("");
        ed_ext.setText("");
        ed_int.setText("");
        ed_direccion.setText("");
        ed_direccion.setFocusableInTouchMode(true);
        ed_cp.setText("");
    }

    public void guardarDireccion()
    {
        boolean bError = false;

        if(ed_titulo.getText().length() < 4)
        {
            bError = true;
            ed_titulo.setError("Campo obligatorio");

        }
        if(ed_ext.getText().length() < 2)
        {
            bError = true;
            ed_ext.setError("Campo obligatorio");
        }
        if(ed_cp.getText().length() < 5)
        {
            bError = true;
            ed_cp.setError("Campo obligatorio");
        }



        if(!bError)
        {
            //guardar
            insert.cNumeroExt = ed_ext.getText().toString();
            insert.cNumeroInt = ed_int.getText().toString();
            insert.cDireccion = ed_direccion.getText().toString();
            insert.cp = ed_cp.getText().toString();
            insert.cTitulo = ed_titulo.getText().toString();

            try
            {
                Type listType = new TypeToken<ArrayList<DTO_Domicilio>>(){}.getType();
                String clstDirecciones = Helpers.getPrefe("lstDirecciones");
                if(clstDirecciones == null || clstDirecciones.length() == 0)
                {
                    lstDirecciones = new ArrayList<>();
                }
                else
                {
                    lstDirecciones = gson.fromJson(clstDirecciones, listType);
                }
                lstDirecciones.add(insert);
                Helpers.setPrefe("lstDirecciones",gson.toJson(lstDirecciones));
                finish();


            }
            catch (Exception e)
            {
                lstDirecciones = new ArrayList<>();
            }
        }
    }


    @Override
    public void onBackPressed()
    {
        if(bModalDireccion)
        {
            OcultarFormulario();
        }
        else
        {
            super.onBackPressed();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        Log.d("PermissionsResult","" + requestCode);
        Log.d("PermissionsResult",gson.toJson(permissions));
        Log.d("PermissionsResult","");
        if(ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED){
            switch (requestCode) {
                //Location
                case 1:
                    Log.d("Permission","Permission granted");
                    InicializarMapa();


                    break;

            }

        }else{

            Log.d("Permission","Permission denied");
            //Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
        }
    }




}
