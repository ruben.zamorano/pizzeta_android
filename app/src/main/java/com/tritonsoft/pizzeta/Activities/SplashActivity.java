package com.tritonsoft.pizzeta.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.tritonsoft.pizzeta.Helpers.Helpers;
import com.tritonsoft.pizzeta.R;
import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.producto;
import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.tamano;
import com.tritonsoft.pizzeta.Retrofit.obtenerConfigPagoTarjeta.Communicator_obtenerConfigPagoTarjeta;
import com.tritonsoft.pizzeta.Retrofit.validacorreopruebausuario.Callback_validacorreopruebausuario;
import com.tritonsoft.pizzeta.Retrofit.validacorreopruebausuario.Response.Response_validacorreopruebausuario;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends AppCompatActivity  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Helpers.setContext(this);
        FirebaseApp.initializeApp(this);
        TimerTask task = new TimerTask() {
            @Override
            public void run() {

//                String sesion = Helpers.getPrefe("DTO_Login","*");
                String sesion = "*";

                if(!sesion.equals("*"))
                {
//
//
//                    DTO_Login login = gson.fromJson(sesion,DTO_Login.class);
//
//
//                    DatabaseReference myRef = FirebaseDatabase.getInstance().getReference();
//                    DatabaseReference ref = myRef.child("lstUsuarios/" + login.cTelefono + "/direccion");
//                    ref.keepSynced(true);
//                    ValueEventListener postListener = new ValueEventListener() {
//                        @Override
//                        public void onDataChange(DataSnapshot dataSnapshot) {
//                            // Get Post object and use the values to update the UI
//                            Log.d(TAG, "onDataChange:" + dataSnapshot.getValue());
//                            Helpers.setPrefe("DTO_Domicilio", gson.toJson(dataSnapshot.getValue()));
//                            PantallaMain(sesion);
//
//                        }
//
//                        @Override
//                        public void onCancelled(DatabaseError databaseError) {
//                            // Getting Post failed, log a message
//                            Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
//                            // ...
//                        }
//                    };
//                    ref.addListenerForSingleValueEvent(postListener);








                } else {
                    FirebaseDatabase database = FirebaseDatabase.getInstance();
                    DatabaseReference myRef = database.getReference("bPruebaCerrada");



                    myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshot) {

                            Log.d("onDataChange",snapshot.getValue().toString());
                            Boolean bPruebaCerrada = Boolean.parseBoolean(snapshot.getValue().toString());
                            Helpers.setPrefeBool("bPruebaCerrada",bPruebaCerrada);
                            if (bPruebaCerrada)
                            {
                                Intent mainIntent = new Intent().setClass(SplashActivity.this, ValidaInvitadoActivity.class);
                                mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(mainIntent);
                                finish();
                            }
                            else
                            {
                                Intent mainIntent = new Intent().setClass(SplashActivity.this, PrincipalActivity.class);
                                mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(mainIntent);
                                finish();
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                }

            }
        };

        Timer timer = new Timer();
        timer.schedule(task, 3000);
    }





}
