package com.tritonsoft.pizzeta.Fragments;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.tritonsoft.pizzeta.Activities.NuevoDomicilioActivity;
import com.tritonsoft.pizzeta.Helpers.GPSTracker;
import com.tritonsoft.pizzeta.Helpers.Helpers;
import com.tritonsoft.pizzeta.MainActivity;
import com.tritonsoft.pizzeta.R;
import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Callback_GetPizzas;
import com.tritonsoft.pizzeta.Retrofit.GetSucursales.Callback_GetSucursales;
import com.tritonsoft.pizzeta.Retrofit.GetSucursales.Communicator_GetSucursales;
import com.tritonsoft.pizzeta.Retrofit.GetSucursales.Response.Response_GetSucursales;
import com.tritonsoft.pizzeta.Retrofit.GetSucursales.Response.marker;

import java.io.IOException;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static android.Manifest.permission_group.LOCATION;

/**
 * A simple {@link Fragment} subclass.
 */
public class SucursalesFragment extends Fragment implements OnMapReadyCallback, Callback_GetSucursales {


    public SucursalesFragment() {
        // Required empty public constructor
    }
    public static SucursalesFragment newInstance() {
        SucursalesFragment fragment = new SucursalesFragment();
        return fragment;
    }

    MapView mapView;
    GoogleMap map;
    final Gson gson = new Gson();
    public Bundle intancia;
    RelativeLayout ly_mensaje;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_sucursales, container, false);
        // Gets the MapView from the XML layout and creates it
        mapView = (MapView) v.findViewById(R.id.mapview);
        intancia = savedInstanceState;
        ly_mensaje = v.findViewById(R.id.ly_mensaje);


        final LocationManager manager = (LocationManager) getActivity().getSystemService( Context.LOCATION_SERVICE );
        String[] permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION,LOCATION};
        CardView btn_gps = v.findViewById(R.id.btn_gps);
        btn_gps.setOnClickListener(view -> {
            if (!manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
                //buildAlertMessageNoGps();
                //InicializarMapa();
            }
            else
            {
                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),  permissions[0])) {

                    //This is called if user has denied the permission before
                    //In this case I am just asking the permission again
                    ActivityCompat.requestPermissions(getActivity(), permissions, 1);

                } else {

                    ActivityCompat.requestPermissions(getActivity(),permissions, 1);
                }
            }

        });

        if (ContextCompat.checkSelfPermission(getContext(), permissions[0]) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),  permissions[0])) {

                //This is called if user has denied the permission before
                //In this case I am just asking the permission again
                ActivityCompat.requestPermissions(getActivity(), permissions, 1);

            } else {

                ActivityCompat.requestPermissions(getActivity(),permissions, 1);
            }
        }
        else
        {


            if (manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
                InicializarMapa();
            }

        }








        return v;
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.getUiSettings().setMyLocationButtonEnabled(false);

        map.setMyLocationEnabled(true);


        // Updates the location and zoom of the MapView
        GPSTracker tracker = new GPSTracker(getContext());
//        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(tracker.getLatitude(), tracker.getLongitude()), 16);
//        map.animateCamera(cameraUpdate);
//        map.moveCamera(cameraUpdate);

        LatLng domi = new LatLng(tracker.getLatitude(), tracker.getLongitude());
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(domi, 18.0f));


    }


    @Override
    public void onResume() {
        try {
            mapView.onResume();
        }
        catch (Exception e)
        {

        }

        super.onResume();
    }


    @Override
    public void onPause() {
        super.onPause();
        try {
            mapView.onPause();
        }
        catch (Exception e)
        {

        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            mapView.onDestroy();
        }
        catch (Exception e)
        {

        }

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        try {
            mapView.onLowMemory();
        }
        catch (Exception e)
        {

        }

    }

    @Override
    public void onSuccess_GetSucursales( Response_GetSucursales result) {
        Log.d("onSuccess_GetSucursales",gson.toJson(result));

        for(marker m : result.data.getResponse())
        {
            LatLng suc = new LatLng(Double.parseDouble(m.lat), Double.parseDouble(m.lng));
            MarkerOptions markerOption = new MarkerOptions().position(suc);

            markerOption.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_pizzeta));

            markerOption.title(m.name);
            map.addMarker(markerOption);

//            map.addMarker(new MarkerOptions()
//                    .position(new LatLng(Double.parseDouble(m.lat), Double.parseDouble(m.lng)))
//                    .title(m.name));
        }
    }

    @Override
    public void onFail_GetSucursales(String result) {
        Log.d("onFail_GetSucursales",result);
    }

    private void askForPermission(String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(getContext(), permission) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permission)) {

                //This is called if user has denied the permission before
                //In this case I am just asking the permission again
                ActivityCompat.requestPermissions(getActivity(), new String[]{permission}, requestCode);

            } else {

                ActivityCompat.requestPermissions(getActivity(), new String[]{permission}, requestCode);
            }
        } else {
            Toast.makeText(getContext(), "" + permission + " is already granted.", Toast.LENGTH_SHORT).show();
        }
    }




    public void InicializarMapa()
    {

//        mapView.onCreate(savedInstanceState);
        ly_mensaje.setVisibility(View.GONE);
        mapView.onCreate(intancia);


        mapView.getMapAsync(this);

        GPSTracker tracker = new GPSTracker(getContext());

        List<Address> matches = null;
        Geocoder geoCoder = new Geocoder(getActivity());
        try {
            matches = geoCoder.getFromLocation(tracker.getLatitude(),tracker.getLongitude(), 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Address bestMatch = (matches.isEmpty() ? null : matches.get(0));
        if(bestMatch != null)
        {
            try
            {
                Helpers.setPrefe("ciudad",bestMatch.getSubAdminArea());
                Log.d("ciudad_",bestMatch.getSubAdminArea());
            }
            catch (Exception e)
            {

            }

        }
        Communicator_GetSucursales communicator_getSucursales = new Communicator_GetSucursales();
        communicator_getSucursales.BuscarSucursales(tracker.getLatitude() + "",tracker.getLongitude() + "","40",this);
    }
}
