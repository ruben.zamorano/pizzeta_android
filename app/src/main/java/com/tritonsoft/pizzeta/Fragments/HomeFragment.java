package com.tritonsoft.pizzeta.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tritonsoft.pizzeta.Activities.PizzaActivity;
import com.tritonsoft.pizzeta.Activities.PrincipalActivity;
import com.tritonsoft.pizzeta.Adapters.Adapter_banner;
import com.tritonsoft.pizzeta.Adapters.Adapter_carrito;
import com.tritonsoft.pizzeta.Helpers.CenterZoomLayoutManager;
import com.tritonsoft.pizzeta.Helpers.Helpers;
import com.tritonsoft.pizzeta.R;
import com.tritonsoft.pizzeta.Retrofit.GetBanner.Callback_GetBanner;
import com.tritonsoft.pizzeta.Retrofit.GetBanner.Communicator_GetBanner;
import com.tritonsoft.pizzeta.Retrofit.GetBanner.Response.Response_GetBanner;
import com.tritonsoft.pizzeta.Retrofit.GetBanner.Response.toda;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements Callback_GetBanner {


    public HomeFragment() {
        // Required empty public constructor
    }
    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }


    public  RecyclerView recyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_home, container, false);

        recyclerView = v.findViewById(R.id.recycler_banner);
        Communicator_GetBanner communicator = new Communicator_GetBanner();
        communicator.BuscarBanner(this);
        ImageView b1 = v.findViewById(R.id.img_b1);
        Glide.with(getContext())
                .load("https://new.pizzeta.com.mx/assets/images/home/banners/small/banner_1.jpg")
                .thumbnail(0.1f)
                .into(b1);


        ImageView b2 = v.findViewById(R.id.img_b2);
        Glide.with(getContext())
                .load("https://new.pizzeta.com.mx/assets/images/home/banners/large/banner_2.jpg")
                .thumbnail(0.1f)
                .into(b2);

        ImageView b3 = v.findViewById(R.id.img_b3);
        Glide.with(getContext())
                .load("https://new.pizzeta.com.mx/assets/images/home/banners/large/banner_3.jpg")
                .thumbnail(0.1f)
                .into(b3);

        ImageView b4 = v.findViewById(R.id.img_b4);
        Glide.with(getContext())
                .load("https://new.pizzeta.com.mx/assets/images/home/banners/medium/banner_4.jpg")
                .thumbnail(0.1f)
                .into(b4);

        ImageView btn_pizzas = v.findViewById(R.id.btn_pizzas);
        btn_pizzas.setOnClickListener(v2 ->{
            Helpers.setPrefe("nTipoProducto","1");
            Intent intent =  new Intent(getContext(), PizzaActivity.class);
            getContext().startActivity(intent);
        });
        ImageView btn_complementos = v.findViewById(R.id.btn_complementos);
        btn_complementos.setOnClickListener(v2 ->{
            Helpers.setPrefe("nTipoProducto","2");
            Intent intent =  new Intent(getContext(), PizzaActivity.class);
            getContext().startActivity(intent);
        });
        ImageView btn_ensaladas = v.findViewById(R.id.btn_ensaladas);
        btn_ensaladas.setOnClickListener(v2 ->{
            Helpers.setPrefe("nTipoProducto","5");
            Intent intent =  new Intent(getContext(), PizzaActivity.class);
            getContext().startActivity(intent);
        });
        ImageView btn_bebidas = v.findViewById(R.id.btn_bebidas);
        btn_bebidas.setOnClickListener(v2 ->{
            Helpers.setPrefe("nTipoProducto","3");
            Intent intent =  new Intent(getContext(), PizzaActivity.class);
            getContext().startActivity(intent);
        });
        ImageView btn_postres = v.findViewById(R.id.btn_postres);
        btn_postres.setOnClickListener(v2 ->{
            Helpers.setPrefe("nTipoProducto","4");
            Intent intent =  new Intent(getContext(), PizzaActivity.class);
            getContext().startActivity(intent);
        });


//        ImageView img_banner = v.findViewById(R.id.img_banner);
//        Glide.with(getContext())
//                .load("https://new.pizzeta.com.mx/assets/images/home/slider/large/slider_13.jpg")
//                .thumbnail(0.1f)
//                .into(img_banner);



        return v;
    }

    @Override
    public void onSuccess_GetBanner(Response_GetBanner result, int nTipoProducto) {
        InicializarBanner(result.data.vigentes);


    }

    @Override
    public void onFail_GetBanner(String result) {

    }

    public void InicializarBanner(List<toda> mItems)
    {

        CenterZoomLayoutManager lm = new CenterZoomLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL, false);
        Adapter_banner adapter_banner = new Adapter_banner(getContext(),mItems);
        recyclerView.setLayoutManager(lm);
        recyclerView.setAdapter(adapter_banner);
    }
}
