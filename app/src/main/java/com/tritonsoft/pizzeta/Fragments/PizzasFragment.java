package com.tritonsoft.pizzeta.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.tritonsoft.pizzeta.Adapters.PizzaGridAdapter;
import com.tritonsoft.pizzeta.R;
import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Callback_GetPizzas;
import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Communicator_GetPizzas;
import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.Response_GetPizzas;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class PizzasFragment extends Fragment implements Callback_GetPizzas {


    public PizzasFragment() {
        // Required empty public constructor
    }

    public static PizzasFragment newInstance() {
        PizzasFragment fragment = new PizzasFragment();
        return fragment;
    }

    public GridView gridview;
    public PizzaGridAdapter gridAdapter;
    public  View v;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_pizzas, container, false);

        Communicator_GetPizzas communicator_getPizzas = new Communicator_GetPizzas();
        communicator_getPizzas.BuscarProductos(1,this);

        return v;
    }

    @Override
    public void onSuccess_GetPizzas(Response_GetPizzas result, int nTipoProducto) {
        if(result.meta.status.contains(""))
        {
            gridview = v.findViewById(R.id.gridview);
            gridAdapter = new PizzaGridAdapter(getContext(),result.data.menu);
            gridview.setNestedScrollingEnabled(false);
            gridview.setAdapter(gridAdapter);
        }
    }

    @Override
    public void onFail_GetPizzas(String result) {

    }
}
