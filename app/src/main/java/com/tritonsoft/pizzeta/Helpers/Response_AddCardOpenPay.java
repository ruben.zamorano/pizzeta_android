package com.tritonsoft.pizzeta.Helpers;

public class Response_AddCardOpenPay {
    public Response_AddCardOpenPay(String id, com.tritonsoft.pizzeta.Helpers.card card) {
        this.id = id;
        this.card = card;
    }

    public String id;
    public card card;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public com.tritonsoft.pizzeta.Helpers.card getCard() {
        return card;
    }

    public void setCard(com.tritonsoft.pizzeta.Helpers.card card) {
        this.card = card;
    }
}



//{
//        "card": {
//        "address": {},
//        "brand": "visa",
//        "cardNumber": "416916XXXXXX3696",
//        "creationDate": {
//        "dateOnly": false,
//        "tzShift": -360,
//        "value": 0
//        },
//        "expirationMonth": "06",
//        "expirationYear": "22",
//        "holderName": "ruben zamorano"
//        },
//        "id": "kplowcqnieypkcmvup8i"
//        }
