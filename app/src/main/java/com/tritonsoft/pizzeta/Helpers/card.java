package com.tritonsoft.pizzeta.Helpers;

public class card {
    public String brand;
    public String cardNumber;
    public creationDate creationDate;
    public String expirationMonth;
    public String expirationYear;
    public String holderName;

    public card(String brand, String cardNumber, com.tritonsoft.pizzeta.Helpers.creationDate creationDate, String expirationMonth, String expirationYear, String holderName) {
        this.brand = brand;
        this.cardNumber = cardNumber;
        this.creationDate = creationDate;
        this.expirationMonth = expirationMonth;
        this.expirationYear = expirationYear;
        this.holderName = holderName;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public com.tritonsoft.pizzeta.Helpers.creationDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(com.tritonsoft.pizzeta.Helpers.creationDate creationDate) {
        this.creationDate = creationDate;
    }

    public String getExpirationMonth() {
        return expirationMonth;
    }

    public void setExpirationMonth(String expirationMonth) {
        this.expirationMonth = expirationMonth;
    }

    public String getExpirationYear() {
        return expirationYear;
    }

    public void setExpirationYear(String expirationYear) {
        this.expirationYear = expirationYear;
    }

    public String getHolderName() {
        return holderName;
    }

    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }
}



//"card": {
//        "address": {},
//        "brand": "visa",
//        "cardNumber": "416916XXXXXX3696",
//        "creationDate": {
//        "dateOnly": false,
//        "tzShift": -360,
//        "value": 0
//        },
//        "expirationMonth": "06",
//        "expirationYear": "22",
//        "holderName": "ruben zamorano"
//        }