package com.tritonsoft.pizzeta.Helpers;

public class creationDate {
    public boolean dateOnly;
    public int tzShift;
    public int value;

    public creationDate(boolean dateOnly, int tzShift, int value) {
        this.dateOnly = dateOnly;
        this.tzShift = tzShift;
        this.value = value;
    }

    public boolean isDateOnly() {
        return dateOnly;
    }

    public void setDateOnly(boolean dateOnly) {
        this.dateOnly = dateOnly;
    }

    public int getTzShift() {
        return tzShift;
    }

    public void setTzShift(int tzShift) {
        this.tzShift = tzShift;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}


// "creationDate": {
////        "dateOnly": false,
////        "tzShift": -360,
////        "value": 0
////        },