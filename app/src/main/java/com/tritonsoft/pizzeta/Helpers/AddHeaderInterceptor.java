package com.tritonsoft.pizzeta.Helpers;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by rubenzamorano on 13/03/18.
 */

public class AddHeaderInterceptor implements Interceptor {

    public String Authorization;

    public AddHeaderInterceptor(String authorization) {
        Authorization = authorization;
    }

    public String getAuthorization() {
        return Authorization;
    }

    public void setAuthorization(String authorization) {
        Authorization = authorization;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {

        Request.Builder builder = chain.request().newBuilder();
        builder.addHeader("Authorization", Authorization);

        return chain.proceed(builder.build());
    }



}