package com.tritonsoft.pizzeta.Helpers;

public class datospedido {
    public String des_calle;
    public String des_correo;
    public String des_linkmap;
    public String nom_ciudad;
    public String nom_cliente;
    public String nom_colonia;
    public String nom_estado;
    public String num_codigopostal;
    public String num_ext;
    public String num_int;
    public String num_telefono;

    public datospedido() {
        this.des_calle = "";
        this.des_correo = "";
        this.des_linkmap = "";
        this.nom_ciudad = "";
        this.nom_cliente = "";
        this.nom_colonia = "";
        this.nom_estado = "";
        this.num_codigopostal = "";
        this.num_ext = "";
        this.num_int = "";
        this.num_telefono = "";
    }

    public String getDes_calle() {
        return des_calle;
    }

    public void setDes_calle(String des_calle) {
        this.des_calle = des_calle;
    }

    public String getDes_correo() {
        return des_correo;
    }

    public void setDes_correo(String des_correo) {
        this.des_correo = des_correo;
    }

    public String getDes_linkmap() {
        return des_linkmap;
    }

    public void setDes_linkmap(String des_linkmap) {
        this.des_linkmap = des_linkmap;
    }

    public String getNom_ciudad() {
        return nom_ciudad;
    }

    public void setNom_ciudad(String nom_ciudad) {
        this.nom_ciudad = nom_ciudad;
    }

    public String getNom_cliente() {
        return nom_cliente;
    }

    public void setNom_cliente(String nom_cliente) {
        this.nom_cliente = nom_cliente;
    }

    public String getNom_colonia() {
        return nom_colonia;
    }

    public void setNom_colonia(String nom_colonia) {
        this.nom_colonia = nom_colonia;
    }

    public String getNom_estado() {
        return nom_estado;
    }

    public void setNom_estado(String nom_estado) {
        this.nom_estado = nom_estado;
    }

    public String getNum_codigopostal() {
        return num_codigopostal;
    }

    public void setNum_codigopostal(String num_codigopostal) {
        this.num_codigopostal = num_codigopostal;
    }

    public String getNum_ext() {
        return num_ext;
    }

    public void setNum_ext(String num_ext) {
        this.num_ext = num_ext;
    }

    public String getNum_int() {
        return num_int;
    }

    public void setNum_int(String num_int) {
        this.num_int = num_int;
    }

    public String getNum_telefono() {
        return num_telefono;
    }

    public void setNum_telefono(String num_telefono) {
        this.num_telefono = num_telefono;
    }
}


//         "des_calle": "",
//         "des_correo": "josecarlos_182_6@hotmail.com", // REQUERIDO
//         "des_linkmap": "",
//         "nom_ciudad": "",
//         "nom_cliente": "JOSE CARLOS GALLEGOS CASTRO", // REQUERIDO
//         "nom_colonia": "",
//         "nom_estado": "",
//         "num_codigopostal": "",
//         "num_ext": "",
//         "num_int": "",
//         "num_telefono": 6672048064 //REQUERIDO
