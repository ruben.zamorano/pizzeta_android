package com.tritonsoft.pizzeta.Helpers.CreditCardForm;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.solver.widgets.Helper;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.Toast;

//import com.droidmentor.checkoutflow.CCFragment.CCNameFragment;
//import com.droidmentor.checkoutflow.CCFragment.CCNumberFragment;
//import com.droidmentor.checkoutflow.CCFragment.CCSecureCodeFragment;
//import com.droidmentor.checkoutflow.CCFragment.CCValidityFragment;
//import com.droidmentor.checkoutflow.Utils.CreditCardUtils;
//import com.droidmentor.checkoutflow.Utils.ViewPagerAdapter;

import com.google.gson.Gson;
import com.tritonsoft.pizzeta.Activities.CarritoActivity;
import com.tritonsoft.pizzeta.DTO.DTO_Perfil;
import com.tritonsoft.pizzeta.Helpers.CreditCardForm.CCFragment.CCNameFragment;
import com.tritonsoft.pizzeta.Helpers.CreditCardForm.CCFragment.CCNumberFragment;
import com.tritonsoft.pizzeta.Helpers.CreditCardForm.CCFragment.CCSecureCodeFragment;
import com.tritonsoft.pizzeta.Helpers.CreditCardForm.CCFragment.CCValidityFragment;
import com.tritonsoft.pizzeta.Helpers.CreditCardForm.Utils.CreditCardUtils;
import com.tritonsoft.pizzeta.Helpers.CreditCardForm.Utils.ViewPagerAdapter;
import com.tritonsoft.pizzeta.Helpers.Helpers;
import com.tritonsoft.pizzeta.Helpers.Response_AddCardOpenPay;
import com.tritonsoft.pizzeta.R;
import com.tritonsoft.pizzeta.Retrofit.createChargeGuest.JSON_createChargeGuest;
import com.tritonsoft.pizzeta.Retrofit.createChargeGuest.customer;

import butterknife.BindView;
import butterknife.ButterKnife;
import mx.openpay.android.Openpay;
import mx.openpay.android.OperationCallBack;
import mx.openpay.android.OperationResult;
import mx.openpay.android.exceptions.OpenpayServiceException;
import mx.openpay.android.exceptions.ServiceUnavailableException;
import mx.openpay.android.model.Card;
import mx.openpay.android.validation.CardValidator;

public class CheckOutActivity extends FragmentActivity implements FragmentManager.OnBackStackChangedListener {

    @BindView(R.id.btnNext)
    Button btnNext;

    public CardFrontFragment cardFrontFragment;
    public CardBackFragment cardBackFragment;

    //This is our viewPager
    private ViewPager viewPager;

    CCNumberFragment numberFragment;
    CCNameFragment nameFragment;
    CCValidityFragment validityFragment;
    CCSecureCodeFragment secureCodeFragment;


    public final Gson gson = new Gson();

    int total_item;
    boolean backTrack = false;

    private boolean mShowingBack = false;

    String cardNumber, cardCVV, cardValidity, cardName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);

        ButterKnife.bind(this);


        cardFrontFragment = new CardFrontFragment();
        cardBackFragment = new CardBackFragment();

        if (savedInstanceState == null) {
            // Add the fragment to the 'fragment_container' FrameLayout
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, cardFrontFragment).commit();

        } else {
            mShowingBack = (getFragmentManager().getBackStackEntryCount() > 0);
        }

        getFragmentManager().addOnBackStackChangedListener(this);

        //Initializing viewPager
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(4);
        setupViewPager(viewPager);


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == total_item)
                    btnNext.setText("SUBMIT");
                else
                    btnNext.setText("NEXT");

                Log.d("track", "onPageSelected: " + position);

                if (position == total_item) {
                    flipCard();
                    backTrack = true;
                } else if (position == total_item - 1 && backTrack) {
                    flipCard();
                    backTrack = false;
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = viewPager.getCurrentItem();
                if (pos < total_item) {
                    viewPager.setCurrentItem(pos + 1);
                } else {
                    checkEntries();
                }

            }
        });


    }

    public void checkEntries() {
        cardName = nameFragment.getName();
        cardNumber = numberFragment.getCardNumber().replace(" ", "").trim();
        cardValidity = validityFragment.getValidity();
        cardCVV = secureCodeFragment.getValue();

        String[] lstValidity = cardValidity.split("/");

        if (TextUtils.isEmpty(cardName)) {
            Toast.makeText(CheckOutActivity.this, "Enter Valid Name", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(cardNumber) || !CreditCardUtils.isValid(cardNumber.replace(" ",""))) {
            Toast.makeText(CheckOutActivity.this, "Enter Valid card number", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(cardValidity)||!CreditCardUtils.isValidDate(cardValidity)) {
            Toast.makeText(CheckOutActivity.this, "Enter correct validity", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(cardCVV)||cardCVV.length()<3) {
            Toast.makeText(CheckOutActivity.this, "Enter valid security number", Toast.LENGTH_SHORT).show();
        } else
        {


            Helpers.showModal(this);
            CerrarTeclado();

            //inicializar OpenPay
            boolean productionMode = false;
          Openpay openpay = new Openpay("m8caejzcetc7ib2i44df", "pk_1011f940f8d340c4bd6fc20edd3ecb7d", productionMode);
//            Openpay openpay = new Openpay("m2hjgoktenbixd5mn6fp", "pk_19fce5f3b6814949b5f07bda4c9e1416", productionMode);

            //token para validar fraude
            String deviceIdString = openpay.getDeviceCollectorDefaultImpl().setup(this);



            Log.d("Tarjeta",cardNumber + ": " + CardValidator.validateNumber(cardNumber));
            Log.d("Tarjeta",Integer.parseInt(lstValidity[0]) + "/" + Integer.parseInt(lstValidity[1]) + ": " + CardValidator.validateExpiryDate(Integer.parseInt(lstValidity[0]), Integer.parseInt(lstValidity[1])));
            Log.d("Tarjeta",cardCVV + ": " + CardValidator.validateCVV(cardCVV, cardNumber));

            //Validar Tarjeta openPay
            if(!CardValidator.validateNumber(cardNumber) ||
                !CardValidator.validateExpiryDate(Integer.parseInt(lstValidity[0]), Integer.parseInt(lstValidity[1])) ||
                !CardValidator.validateCVV(cardCVV, cardNumber)) {
                //tarjeta no valida para openpay

                Toast.makeText(CheckOutActivity.this, "Tarjeta no valida", Toast.LENGTH_SHORT).show();
                return;
            }



            //generar token
            Card card = new Card();
            card.holderName(cardName);
            card.cardNumber(cardNumber);
            card.expirationMonth(Integer.parseInt(lstValidity[0]));
            card.expirationYear(Integer.parseInt(lstValidity[1]));
            card.cvv2(cardCVV);


            openpay.createToken(card, new OperationCallBack() {

                @Override
                public void onSuccess(OperationResult arg0) {
                    //Handlo in successx
                    Log.d("createToken","onSuccess" + ", " + gson.toJson(arg0.getResult()));

                    Response_AddCardOpenPay response = gson.fromJson(gson.toJson(arg0.getResult()),Response_AddCardOpenPay.class);
                    Helpers.setPrefe("token_openpay",response.id);
                    Helpers.setPrefe("response_openpay",gson.toJson(response));

                    //Toast.makeText(CheckOutActivity.this, response.id, Toast.LENGTH_SHORT).show();


                    DTO_Perfil perfil = gson.fromJson(Helpers.getPrefe("perfil","{}"),DTO_Perfil.class);


                    customer customer = new customer(perfil.cNombres,"Pizzeta",perfil.cTelefono,perfil.cMail);
                    JSON_createChargeGuest body = new JSON_createChargeGuest(response.id,"card",Integer.parseInt(Helpers.getPrefe("nTotal","0")),"MXN","Compra Pizzeta App","",deviceIdString,customer);
                    Helpers.setPrefe("JSON_createChargeGuest",gson.toJson(body));




                    Helpers.setPrefe("clv_tipopago","2");
                    Intent intent =  new Intent(CheckOutActivity.this, CarritoActivity.class);
                    startActivity(intent);
                    Helpers.closeModal(CheckOutActivity.this);



                    //finish();
                    //next
                }

                @Override
                public void onError(OpenpayServiceException arg0) {
                    //Handle Error
                    Log.d("createToken","onError" + ", "+ arg0.description);
                }

                @Override
                public void onCommunicationError(ServiceUnavailableException arg0) {
                    //Handle communication error
//                    Log.d("createToken","onCommunicationError" + ", "+ arg0.getMessage());
                }
            });

        }


    }

    @Override
    public void onBackStackChanged() {
        mShowingBack = (getFragmentManager().getBackStackEntryCount() > 0);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        numberFragment = new CCNumberFragment();
        nameFragment = new CCNameFragment();
        validityFragment = new CCValidityFragment();
        secureCodeFragment = new CCSecureCodeFragment();
        adapter.addFragment(numberFragment);
        adapter.addFragment(nameFragment);
        adapter.addFragment(validityFragment);
        adapter.addFragment(secureCodeFragment);

        total_item = adapter.getCount() - 1;
        viewPager.setAdapter(adapter);

    }

    private void flipCard() {
        if (mShowingBack) {
            getFragmentManager().popBackStack();
            return;
        }
        // Flip to the back.
        //setCustomAnimations(int enter, int exit, int popEnter, int popExit)

        mShowingBack = true;

        getFragmentManager()
                .beginTransaction()
                .setCustomAnimations(
                        R.animator.card_flip_right_in,
                        R.animator.card_flip_right_out,
                        R.animator.card_flip_left_in,
                        R.animator.card_flip_left_out)
                .replace(R.id.fragment_container, cardBackFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onBackPressed() {
        int pos = viewPager.getCurrentItem();
        if (pos > 0) {
            viewPager.setCurrentItem(pos - 1);
        } else
            super.onBackPressed();
    }

    public void nextClick() {
        btnNext.performClick();
    }



    public void CerrarTeclado()
    {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
