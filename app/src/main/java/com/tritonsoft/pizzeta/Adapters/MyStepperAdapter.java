package com.tritonsoft.pizzeta.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;

import com.stepstone.stepper.Step;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;
import com.tritonsoft.pizzeta.Fragments.StepFragmentSample;
import com.tritonsoft.pizzeta.R;

public class MyStepperAdapter extends AbstractFragmentStepAdapter {

    public MyStepperAdapter(FragmentManager fm, Context context) {
        super(fm, context);
    }

    @Override
    public Step createStep(int position) {
        final StepFragmentSample step = new StepFragmentSample();
        Bundle b = new Bundle();
        b.putInt("tab", position);
        step.setArguments(b);
        return step;
    }

    @Override
    public int getCount() {
        return 3;
    }

//    @NonNull
//    @Override
//    public StepViewModel getViewModel(@IntRange(from = 0) int position) {
//        //Override this method to set Step title for the Tabs, not necessary for other stepper types
//        return new StepViewModel.Builder(context)
//                .setTitle("holis") //can be a CharSequence instead
//                .create();
//    }

    @NonNull
    @Override
    public StepViewModel getViewModel(@IntRange(from = 0) int position) {
        StepViewModel.Builder builder = new StepViewModel.Builder(context)
                .setTitle("holis");
        switch (position) {
            case 0:
                builder
                        .setEndButtonLabel("Siguiente")
                        .setBackButtonLabel("Cancelar");
//                        .setNextButtonEndDrawableResId(R.drawable.ic_arrow_forward)
//                        .setBackButtonStartDrawableResId(StepViewModel.NULL_DRAWABLE);
                break;
            case 1:
                builder
                        .setEndButtonLabel("Siguiente")
                        .setBackButtonLabel("Regresar");
//                        .setBackButtonStartDrawableResId(R.drawable.ic_arrow_back);
                break;
            case 2:
                builder
                        .setBackButtonLabel("Regresar")
                        .setEndButtonLabel("Listo!");
                break;
            default:
                throw new IllegalArgumentException("Unsupported position: " + position);
        }
        return builder.create();
    }
}