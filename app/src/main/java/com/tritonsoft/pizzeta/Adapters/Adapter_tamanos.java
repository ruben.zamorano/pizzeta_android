package com.tritonsoft.pizzeta.Adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tritonsoft.pizzeta.Activities.ComplementosActivity;
import com.tritonsoft.pizzeta.Helpers.Helpers;
import com.tritonsoft.pizzeta.R;
import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.producto;
import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.tamano;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


/**


 **/
public class Adapter_tamanos extends RecyclerView.Adapter<Adapter_tamanos.ViewHolder> {

    private Context mContext;
    private Context mContextactivity;
    public List<tamano> mItems;
    public final Gson gson = new Gson();
    //public AdaptadorPizza esta = this;
    //public  OnClickDomicilio listener;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item
        public TextView tv_pizza;
        public ImageView img_pizza;
        public CardView btn_extra;

        public ViewHolder(View v) {
            super(v);



        }
    }



    public Adapter_tamanos(Context context, List<tamano> items)
    {
        this.mContext = context;
        this.mItems = items;
        //this.listener = listener;
    }



    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_tamano, viewGroup, false);

        mContextactivity = viewGroup.getContext();
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {
        final tamano item = mItems.get(i);




    }




}