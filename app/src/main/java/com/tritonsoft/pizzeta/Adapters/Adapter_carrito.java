package com.tritonsoft.pizzeta.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.tritonsoft.pizzeta.Activities.CarritoActivity;
import com.tritonsoft.pizzeta.Activities.FormaPagoActivity;
import com.tritonsoft.pizzeta.Helpers.Helpers;
import com.tritonsoft.pizzeta.R;
import com.tritonsoft.pizzeta.Retrofit.GetDetallePizza.Response.ingrediente;
import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.producto;

import java.util.List;


/**


 **/
public class Adapter_carrito extends RecyclerView.Adapter<Adapter_carrito.ViewHolder> {

    private Context mContext;
    private Context mContextactivity;
    public List<producto> mItems;
    public final Gson gson = new Gson();
    //public AdaptadorPizza esta = this;
    //public  OnClickDomicilio listener;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item
        public TextView lvl_pizza;
        public TextView lvl_descripcion_pizza;
        public TextView lvl_cantidad;
        public ImageView img_pizza;
        public ImageView btn_remove;

        public ViewHolder(View v) {
            super(v);
            lvl_pizza = v.findViewById(R.id.lvl_pizza);
            lvl_descripcion_pizza = v.findViewById(R.id.lvl_descripcion_pizza);
            lvl_cantidad = v.findViewById(R.id.lvl_cantidad);
            img_pizza = v.findViewById(R.id.img_pizza);
            btn_remove = v.findViewById(R.id.btn_remove);


        }
    }



    public Adapter_carrito(Context context, List<producto> items)
    {
        this.mContext = context;
        this.mItems = items;
        //this.listener = listener;
    }



    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_carrito, viewGroup, false);

        mContextactivity = viewGroup.getContext();
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {
        final producto item = mItems.get(i);

        boolean bCarrito =  Helpers.getPrefeBool("bCarrito",false);

        viewHolder.lvl_pizza.setText(item.nom_producto);
        viewHolder.lvl_descripcion_pizza.setText(item.des_producto);
        viewHolder.lvl_cantidad.setText("x" +item.getTamanios().get(0).num_cantidad + "");


        Glide.with(mContext)
                .load("https://new.pizzeta.com.mx/assets/images/products-300/" + item.des_img)
                .thumbnail(0.1f)
                .into(viewHolder.img_pizza);


        if(bCarrito)
        {
            viewHolder.btn_remove.setVisibility(View.VISIBLE);
            viewHolder.btn_remove.setOnClickListener(v ->{
                mItems.remove(i);
                Helpers.setPrefe("lstPizzas",gson.toJson(mItems));
                notifyDataSetChanged();
                ((CarritoActivity)mContext).CalcularTotal(mItems);
            });
        }
        else
        {
            viewHolder.btn_remove.setVisibility(View.VISIBLE);
            viewHolder.btn_remove.setVisibility(View.VISIBLE);
            viewHolder.btn_remove.setOnClickListener(v ->{
                mItems.remove(i);
                Helpers.setPrefe("lstPizzas",gson.toJson(mItems));
                notifyDataSetChanged();
                ((FormaPagoActivity)mContext).CalcularTotal(mItems);
            });

        }


    }




}