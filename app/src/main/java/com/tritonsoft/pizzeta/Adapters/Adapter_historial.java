package com.tritonsoft.pizzeta.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.tritonsoft.pizzeta.Activities.CarritoActivity;
import com.tritonsoft.pizzeta.Activities.TrackActivity;
import com.tritonsoft.pizzeta.DTO.DTO_Carrito;
import com.tritonsoft.pizzeta.Helpers.Helpers;
import com.tritonsoft.pizzeta.R;
import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.producto;

import java.util.List;


/**


 **/
public class Adapter_historial extends RecyclerView.Adapter<Adapter_historial.ViewHolder> {

    private Context mContext;
    private Context mContextactivity;
    public List<DTO_Carrito> mItems;
    public final Gson gson = new Gson();
    //public AdaptadorPizza esta = this;
    //public  OnClickDomicilio listener;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item
        public TextView lvl_productos;
        public TextView lvl_total;
        public TextView lvl_fecha;
        public ImageView img_pizza;
        public CardView card_historial;


        public ViewHolder(View v) {
            super(v);
            lvl_productos = v.findViewById(R.id.lvl_productos);
            lvl_total = v.findViewById(R.id.lvl_total);
            img_pizza = v.findViewById(R.id.img_pizza);
            lvl_fecha = v.findViewById(R.id.lvl_fecha);
            card_historial = v.findViewById(R.id.card_historial);


        }
    }



    public Adapter_historial(Context context, List<DTO_Carrito> items)
    {
        this.mContext = context;
        this.mItems = items;
        //this.listener = listener;
    }



    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_historial, viewGroup, false);

        mContextactivity = viewGroup.getContext();
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {
        final DTO_Carrito item = mItems.get(i);






        viewHolder.lvl_total.setText("Total de pedido: $" + item.imp_totalpedido);
        viewHolder.lvl_productos.setText("Folio: " + item.idu_pedidoEnLinea);
        viewHolder.lvl_fecha.setText(item.dFechaPedido);

        Glide.with(mContext)
                .load("https://new.pizzeta.com.mx/assets/images/products-300/" + item.productos.get(0).des_img)
                .thumbnail(0.1f)
                .into(viewHolder.img_pizza);


        viewHolder.card_historial.setOnClickListener(v ->{
            //intent
            Helpers.setPrefe("dFecha",item.dFechaPedido);
            Helpers.setPrefe("folioOrden",item.idu_pedidoEnLinea);
            mContext.startActivity(new Intent(mContext, TrackActivity.class));
        });


    }




}