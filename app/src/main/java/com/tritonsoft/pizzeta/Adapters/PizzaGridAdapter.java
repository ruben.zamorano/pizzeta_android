package com.tritonsoft.pizzeta.Adapters;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tritonsoft.pizzeta.Activities.ComplementosActivity;
import com.tritonsoft.pizzeta.Activities.PizzaActivity;
import com.tritonsoft.pizzeta.Helpers.Helpers;
import com.tritonsoft.pizzeta.R;
import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.producto;


import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ruben on 28/12/17.
 */

public class PizzaGridAdapter extends BaseAdapter {
    private Context mContext;
    private List<producto> mItems;
    public final Gson gson = new Gson();


    public PizzaGridAdapter(Context c, List<producto> mItems) {
        mContext = c;
        this.mItems = mItems;
    }

    public int getCount() {
        return mItems.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        //componentes del view
        ImageView img_pizza;
        TextView tv_pizza;
        CardView card_pizza;



        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout fullView =  (LinearLayout)inflater.inflate(R.layout.item_pizza_grid,null);
//        fullView.setLayoutParams(new GridView.LayoutParams(85, 85));

//        fullView.setLayoutParams(new GridView.LayoutParams(GridView.AUTO_FIT, GridView.AUTO_FIT));

        img_pizza = fullView.findViewById(R.id.img_pizza);
        Log.d("img_pizzeta","https://new.pizzeta.com.mx/assets/images/products-300/" + mItems.get(position).des_img);
        Glide.with(mContext)
                .load("https://new.pizzeta.com.mx/assets/images/products-300/" + mItems.get(position).des_img)
                .thumbnail(0.1f)
                .into(img_pizza);
        tv_pizza = fullView.findViewById(R.id.tv_pizza);
        tv_pizza.setText(mItems.get(position).nom_producto);
        card_pizza = fullView.findViewById(R.id.card_pizza);
        card_pizza.setOnClickListener(v -> {
            Helpers.setPrefe("pizza",gson.toJson(mItems.get(position)));
            ((PizzaActivity)mContext).onClickPizza();
        });
//        if(mItems.get(position).tamanos.size() == 1)
//        {
//
//            List<producto> lstPizzas = new ArrayList<>();
//            try {
//                Type listType = new TypeToken<ArrayList<producto>>(){}.getType();
//                String clstPizzas = Helpers.getPrefe("lstPizzas");
//                if(clstPizzas == null || clstPizzas.length() == 0)
//                {
//                    lstPizzas = new ArrayList<>();
//                }
//                else
//                {
//                    lstPizzas = gson.fromJson(clstPizzas, listType);
//                    mItems.get(position).tamanos.get(0).setNum_cantidad(1);
//                    mItems.get(position).tamanos.get(0).setImp_total(Integer.parseInt(mItems.get(position).tamanos.get(0).imp_precio));
//                    lstPizzas.add(mItems.get(position));
//                }
//
//            }
//            catch (Exception e)
//            {
//                lstPizzas = new ArrayList<>();
//            }
//            finally {
//                Helpers.setPrefe("lstPizzas",gson.toJson(lstPizzas));
//                //mostrar alerta de agregado
//
//            }
//        }
//        else
//        {
//
//        }



//        imageView = fullView.findViewById(R.id.img_categoria);





        return fullView;
    }

    public String ObtenerDescripcion(String Descripcion)
    {
        String cDescripcion = "";

        if(Descripcion.length() >= 30)
        {
            cDescripcion = Descripcion.substring(0,30);
            cDescripcion += "...";
        }
        else
        {
            cDescripcion = Descripcion;
        }

        return cDescripcion;
    }



//    public void AgregarItems(List<catalogEntryView> lstItems)
//    {
//        for (catalogEntryView i: lstItems)
//        {
//            mItems.add(i);
//        }
//        this.notifyDataSetChanged();
//    }
//
//
//    public void DeleteAll()
//    {
//        mItems.clear();
//        this.notifyDataSetChanged();
//    }



}