package com.tritonsoft.pizzeta.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.tritonsoft.pizzeta.Activities.CarritoActivity;
import com.tritonsoft.pizzeta.Activities.SeleccionarDomicilioActivity;
import com.tritonsoft.pizzeta.DTO.DTO_Domicilio;
import com.tritonsoft.pizzeta.Helpers.Helpers;
import com.tritonsoft.pizzeta.R;
import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.producto;

import java.util.List;


/**


 **/
public class Adapter_direcciones extends RecyclerView.Adapter<Adapter_direcciones.ViewHolder> {

    private Context mContext;
    private Context mContextactivity;
    public List<DTO_Domicilio> mItems;
    public final Gson gson = new Gson();
    //public AdaptadorPizza esta = this;
    //public  OnClickDomicilio listener;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item
        public TextView lvl_titulo;
        public TextView lvl_direccion;
        public ImageView btn_check;
        public ImageView btn_uncheck;
        public CardView btn_direccion;
        public ImageView btn_borrar_domicilio;

        public ViewHolder(View v) {
            super(v);
            lvl_titulo = v.findViewById(R.id.lvl_titulo);
            lvl_direccion = v.findViewById(R.id.lvl_direccion);
            btn_check = v.findViewById(R.id.btn_check);
            btn_uncheck = v.findViewById(R.id.btn_uncheck);
            btn_direccion = v.findViewById(R.id.btn_direccion);
            btn_borrar_domicilio = v.findViewById(R.id.btn_borrar_domicilio);


        }
    }



    public Adapter_direcciones(Context context, List<DTO_Domicilio> items)
    {
        this.mContext = context;
        this.mItems = items;
        //this.listener = listener;
    }



    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_direcciones, viewGroup, false);

        mContextactivity = viewGroup.getContext();
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {
        final DTO_Domicilio item = mItems.get(i);

        viewHolder.lvl_titulo.setText(item.cTitulo);
        viewHolder.lvl_direccion.setText(item.cDireccion);

        if(item.bActivo)
        {
            viewHolder.btn_check.setVisibility(View.VISIBLE);
            viewHolder.btn_uncheck.setVisibility(View.GONE);
        }
        else
        {
            viewHolder.btn_check.setVisibility(View.GONE);
            viewHolder.btn_uncheck.setVisibility(View.VISIBLE);
        }

        boolean bMenuPerfil = Helpers.getPrefeBool("bMenuPerfil",false);

        if(!bMenuPerfil)
        {
            viewHolder.btn_direccion.setOnClickListener(v ->{
                Helpers.setPrefe("direccion",gson.toJson(item));

                ((SeleccionarDomicilioActivity)mContext).BuscarSucursales(item.lat,item.lng);
                //mContext.startActivity(new Intent(mContext, CarritoActivity.class));
//            for (DTO_Domicilio m: mItems)
//            {
//                m.bActivo = false;
//            }
//            item.bActivo = true;
//            notifyDataSetChanged();
            });
        }


        viewHolder.btn_borrar_domicilio.setOnClickListener(v ->{
            mItems.remove(i);
            Helpers.setPrefe("lstDirecciones",gson.toJson(mItems));
            notifyDataSetChanged();
        });




//        viewHolder.btn_remove.setOnClickListener(v ->{
//            mItems.remove(i);
//            Helpers.setPrefe("lstPizzas",gson.toJson(mItems));
//            notifyDataSetChanged();
//            ((CarritoActivity)mContext).CalcularTotal(mItems);
//        });
    }




}