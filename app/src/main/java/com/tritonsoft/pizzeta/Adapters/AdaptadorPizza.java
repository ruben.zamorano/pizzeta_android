package com.tritonsoft.pizzeta.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tritonsoft.pizzeta.DTO.DTO_Pizza;
import com.tritonsoft.pizzeta.R;

import java.util.List;


/**


 **/
public class AdaptadorPizza extends RecyclerView.Adapter<AdaptadorPizza.ViewHolder> {

    private Context mContext;
    private Context mContextactivity;
    public List<DTO_Pizza> mItems;
    //public AdaptadorPizza esta = this;
    //public  OnClickDomicilio listener;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item
        public TextView cNombreDomicilio;
        public TextView cDireccion;
        public ImageView iTipoDomicilio;
        public ImageView iCheck;

        public ViewHolder(View v) {
            super(v);
//            cNombreDomicilio = (TextView)v.findViewById(R.id.tv_cNombreDomicilio);
//            cDireccion = (TextView)v.findViewById(R.id.tv_cDireccion);
//            iTipoDomicilio = (ImageView)v.findViewById(R.id.img_lugar);
//            iCheck = (ImageView)v.findViewById(R.id.img_check);

        }
    }



    public AdaptadorPizza(Context context, List<DTO_Pizza> items)
    {
        this.mContext = context;
        this.mItems = items;
        //this.listener = listener;
    }



    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_pizza, viewGroup, false);

        mContextactivity = viewGroup.getContext();
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {
        final DTO_Pizza item = mItems.get(i);
        //viewHolder.cNombreDomicilio.setText(item.getcNombreDomicilio());
    }




}