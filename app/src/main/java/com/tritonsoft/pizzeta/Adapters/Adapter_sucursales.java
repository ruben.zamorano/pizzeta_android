package com.tritonsoft.pizzeta.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.tritonsoft.pizzeta.Activities.CarritoActivity;
import com.tritonsoft.pizzeta.Helpers.Helpers;
import com.tritonsoft.pizzeta.R;
import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.producto;
import com.tritonsoft.pizzeta.Retrofit.GetSucursales.Response.marker;

import java.util.ArrayList;
import java.util.List;


/**


 **/
public class Adapter_sucursales extends RecyclerView.Adapter<Adapter_sucursales.ViewHolder> {

    private Context mContext;
    private Context mContextactivity;
    public List<marker> mItems;
    public List<marker> mItemsCopia;
    public final Gson gson = new Gson();


    public static class ViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item
        public TextView lvl_sucursal;
        public TextView lvl_direccion;
        public TextView lvl_distance;
        public CardView btn_sucursal;
        public RelativeLayout ly_cerrado;


        public ViewHolder(View v) {
            super(v);
            lvl_sucursal = v.findViewById(R.id.lvl_sucursal);
            lvl_direccion = v.findViewById(R.id.lvl_direccion);
            lvl_distance = v.findViewById(R.id.lvl_distance);
            btn_sucursal = v.findViewById(R.id.btn_sucursal);
            ly_cerrado = v.findViewById(R.id.ly_cerrado);


        }
    }



    public Adapter_sucursales(Context context, List<marker> items)
    {
        this.mContext = context;
        this.mItems = items;
        this.mItemsCopia = new ArrayList<>();
        for (marker item: mItems)
        {
            mItemsCopia.add(gson.fromJson(gson.toJson(item),marker.class));
        }
        //this.listener = listener;
    }



    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_sucursales, viewGroup, false);

        mContextactivity = viewGroup.getContext();
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {
        final marker item = mItems.get(i);

        //item.sucabierta = "1";
        if(Integer.parseInt(item.sucabierta) == 0)
        {
            //viewHolder.ly_cerrado.setVisibility(View.VISIBLE);

        }
        else
        {
            Helpers.setPrefe("envio","0");
            viewHolder.ly_cerrado.setVisibility(View.GONE);
            viewHolder.btn_sucursal.setOnClickListener(v -> {
                Helpers.setPrefe("clv_tipopago","1");
                Helpers.setPrefe("sucursal",gson.toJson(item));
                Helpers.setPrefe("idu_tipopedido","1");
                Helpers.setPrefe("idu_sucursal",item.id);
                Helpers.setPrefeBool("bCarritoVacio",false);
                mContext.startActivity(new Intent(mContext, CarritoActivity.class));

            });

        }




        viewHolder.lvl_direccion.setText(item.address);
        viewHolder.lvl_sucursal.setText(item.name);
        viewHolder.lvl_distance.setText(AjustarTexto(item.distance));



    }

    public void filter(String text) {

        Log.d("filter_",text + " - " + mItemsCopia.size() );
        mItems.clear();
        if(text.isEmpty()){
            Log.d("filter_","Super vacio");
            mItems.addAll(mItemsCopia);
        } else{
            text = text.toLowerCase();
            for(marker item: mItemsCopia){
                if(item.name.toLowerCase().contains(text) || item.name.toLowerCase().contains(text)){
                    mItems.add(item);
                }
            }
        }
        this.notifyDataSetChanged();
    }

    public String AjustarTexto(String cTexo)
    {
        //0000000000000000000...



        if(cTexo.length() > 5)
        {
            cTexo = cTexo.substring(0,3);
        }

        String last = String.valueOf(cTexo.charAt(cTexo.length()-1));
        if(last.contains("."))
        {
            cTexo = cTexo.replace(".","");
        }




        return cTexo;
    }




}