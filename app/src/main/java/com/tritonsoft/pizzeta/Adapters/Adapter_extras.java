package com.tritonsoft.pizzeta.Adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tritonsoft.pizzeta.Activities.CarritoActivity;
import com.tritonsoft.pizzeta.Activities.ComplementosActivity;
import com.tritonsoft.pizzeta.Helpers.Helpers;
import com.tritonsoft.pizzeta.R;
import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.producto;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


/**


 **/
public class Adapter_extras extends RecyclerView.Adapter<Adapter_extras.ViewHolder> {

    private Context mContext;
    private Context mContextactivity;
    public List<producto> mItems;
    public final Gson gson = new Gson();
    //public AdaptadorPizza esta = this;
    //public  OnClickDomicilio listener;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item
        public TextView tv_pizza;
        public ImageView img_pizza;
        public CardView btn_extra;

        public ViewHolder(View v) {
            super(v);
            tv_pizza = v.findViewById(R.id.tv_pizza);
            img_pizza = v.findViewById(R.id.img_pizza);
            btn_extra = v.findViewById(R.id.btn_extra);


        }
    }



    public Adapter_extras(Context context, List<producto> items)
    {
        this.mContext = context;
        this.mItems = items;
        //this.listener = listener;
    }



    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_extra, viewGroup, false);

        mContextactivity = viewGroup.getContext();
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {
        final producto item = mItems.get(i);

        viewHolder.tv_pizza.setText(item.nom_producto);
        if(item.tamanios.size() == 1)
        {
            viewHolder.tv_pizza.setText(item.nom_producto + " " + item.tamanios.get(0).nom_tamano);
            viewHolder.btn_extra.setOnClickListener(v ->{
                //agregar extra

                List<producto> lstPizzas = new ArrayList<>();
                try {
                    Type listType = new TypeToken<ArrayList<producto>>(){}.getType();
                    String clstPizzas = Helpers.getPrefe("lstPizzas");
                    if(clstPizzas == null || clstPizzas.length() == 0)
                    {
                        lstPizzas = new ArrayList<>();
                    }
                    else
                    {
                        lstPizzas = gson.fromJson(clstPizzas, listType);
                        item.tamanios.get(0).setNum_cantidad(1);
                        item.tamanios.get(0).setImp_total(Integer.parseInt(item.tamanios.get(0).imp_precio));
                        lstPizzas.add(item);
                    }

                }
                catch (Exception e)
                {
                    lstPizzas = new ArrayList<>();
                }
                finally {
                    Helpers.setPrefe("lstPizzas",gson.toJson(lstPizzas));

                    ((ComplementosActivity)mContext).BuscarPizzas();
                    ((ComplementosActivity)mContext).MostrarAlerta(item.nom_producto,"Agregado al carrito");
                }
            });
        }
        else
        {
            viewHolder.tv_pizza.setText(item.nom_producto);
            viewHolder.btn_extra.setOnClickListener(v ->{
                //bottom sheet tamanio

                ((ComplementosActivity)mContext).MostrarAlertaTamano(item);
            });
        }


        Glide.with(mContext)
                .load("https://new.pizzeta.com.mx/assets/images/products-300/" + item.des_img)
                .thumbnail(0.1f)
                .into(viewHolder.img_pizza);


    }




}