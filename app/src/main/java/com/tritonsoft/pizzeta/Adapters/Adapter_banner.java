package com.tritonsoft.pizzeta.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.tritonsoft.pizzeta.Activities.CarritoActivity;
import com.tritonsoft.pizzeta.Helpers.Helpers;
import com.tritonsoft.pizzeta.R;
import com.tritonsoft.pizzeta.Retrofit.GetBanner.Response.toda;
import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.producto;

import java.util.List;


/**


 **/
public class Adapter_banner extends RecyclerView.Adapter<Adapter_banner.ViewHolder> {

    private Context mContext;
    private Context mContextactivity;
    public List<toda> mItems;
    public final Gson gson = new Gson();
    //public AdaptadorPizza esta = this;
    //public  OnClickDomicilio listener;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item

        public ImageView img_banner;

        public ViewHolder(View v) {
            super(v);

            img_banner = v.findViewById(R.id.img_banner);


        }
    }



    public Adapter_banner(Context context, List<toda> items)
    {
        this.mContext = context;
        this.mItems = items;
        //this.listener = listener;
    }



    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_banner, viewGroup, false);

        mContextactivity = viewGroup.getContext();
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {
        final toda item = mItems.get(i);



        Glide.with(mContext)
                .load("https://www.pizzeta.com.mx/" + item.des_ruta_l)
                .thumbnail(0.1f)
                .into(viewHolder.img_banner);


    }




}