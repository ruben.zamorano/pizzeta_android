package com.tritonsoft.pizzeta.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tritonsoft.pizzeta.R;
import com.tritonsoft.pizzeta.Retrofit.GetDetallePizza.Response.ingrediente;

import java.util.List;


/**


 **/
public class Adapter_ingrediente extends RecyclerView.Adapter<Adapter_ingrediente.ViewHolder> {

    private Context mContext;
    private Context mContextactivity;
    public List<ingrediente> mItems;
    //public AdaptadorPizza esta = this;
    //public  OnClickDomicilio listener;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item
        public TextView lvl_ingrediente;

        public ViewHolder(View v) {
            super(v);
            lvl_ingrediente = v.findViewById(R.id.lvl_ingrediente);


        }
    }



    public Adapter_ingrediente(Context context, List<ingrediente> items)
    {
        this.mContext = context;
        this.mItems = items;
        //this.listener = listener;
    }



    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_ingrediente, viewGroup, false);

        mContextactivity = viewGroup.getContext();
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {
        final ingrediente item = mItems.get(i);
        viewHolder.lvl_ingrediente.setText(item.nom_ingrediente);
    }




}