package com.tritonsoft.pizzeta.DTO;

import com.tritonsoft.pizzeta.Helpers.datospedido;
import com.tritonsoft.pizzeta.Retrofit.GetPizzas.Response.producto;

import java.util.List;

public class DTO_Carrito {
    public int idu_origenpedido;
    public int idu_tipopedido;
    public int idu_sucursal;
    public int clv_tipopago;
    public int imp_cargoservicio;
    public int imp_totalpedido;
    public com.tritonsoft.pizzeta.Helpers.datospedido datospedido;
    public List<producto> productos;

    public int opc_programado;
    public String fec_programado;
    public int imp_pagacon;
    public int imp_cambio;


    //folio
    public String idu_pedidoEnLinea;

    //fecha de pedido
    public String dFechaPedido;



    public DTO_Carrito() {
    }

    public int getIdu_origenpedido() {
        return idu_origenpedido;
    }

    public void setIdu_origenpedido(int idu_origenpedido) {
        this.idu_origenpedido = idu_origenpedido;
    }

    public int getIdu_tipopedido() {
        return idu_tipopedido;
    }

    public void setIdu_tipopedido(int idu_tipopedido) {
        this.idu_tipopedido = idu_tipopedido;
    }

    public int getIdu_sucursal() {
        return idu_sucursal;
    }

    public void setIdu_sucursal(int idu_sucursal) {
        this.idu_sucursal = idu_sucursal;
    }

    public int getClv_tipopago() {
        return clv_tipopago;
    }

    public void setClv_tipopago(int clv_tipopago) {
        this.clv_tipopago = clv_tipopago;
    }

    public int getImp_cargoservicio() {
        return imp_cargoservicio;
    }

    public void setImp_cargoservicio(int imp_cargoservicio) {
        this.imp_cargoservicio = imp_cargoservicio;
    }

    public int getImp_totalpedido() {
        return imp_totalpedido;
    }

    public void setImp_totalpedido(int imp_totalpedido) {
        this.imp_totalpedido = imp_totalpedido;
    }

    public com.tritonsoft.pizzeta.Helpers.datospedido getDatospedido() {
        return datospedido;
    }

    public void setDatospedido(com.tritonsoft.pizzeta.Helpers.datospedido datospedido) {
        this.datospedido = datospedido;
    }

    public List<producto> getProductos() {
        return productos;
    }

    public void setProductos(List<producto> productos) {
        this.productos = productos;
    }

    public int getOpc_programado() {
        return opc_programado;
    }

    public void setOpc_programado(int opc_programado) {
        this.opc_programado = opc_programado;
    }

    public String getFec_programado() {
        return fec_programado;
    }

    public void setFec_programado(String fec_programado) {
        this.fec_programado = fec_programado;
    }

    public int getImp_pagacon() {
        return imp_pagacon;
    }

    public void setImp_pagacon(int imp_pagacon) {
        this.imp_pagacon = imp_pagacon;
    }

    public int getImp_cambio() {
        return imp_cambio;
    }

    public void setImp_cambio(int imp_cambio) {
        this.imp_cambio = imp_cambio;
    }

    public String getIdu_pedidoEnLinea() {
        return idu_pedidoEnLinea;
    }

    public void setIdu_pedidoEnLinea(String idu_pedidoEnLinea) {
        this.idu_pedidoEnLinea = idu_pedidoEnLinea;
    }
    public String getdFechaPedido() {
        return dFechaPedido;
    }

    public void setdFechaPedido(String dFechaPedido) {
        this.dFechaPedido = dFechaPedido;
    }



}

//        "opc_programado": 0,
//        "fec_programado":"",
//        "imp_pagacon":500,
//        "imp_cambio":163,


//
//{
//        "idu_origenpedido": 1, // 1 WEB 2 APP
//        "idu_tipopedido": 1, // 1 RECOGER 2 DOMICILIO
//        "idu_sucursal": 14, // ID DE SUCURSAL
//        "clv_tipopago": 1, // 1 EFECTIVO 2 TARJETA       --------------------------
//        "imp_cargoservicio": 0, // CARGO POR SERIVICO A DOMICILIO
//        "imp_totalpedido": 333, // MONTO TOTAL DEL PEDIDO
//        "datospedido": {
//        "des_calle": "",
//        "des_correo": "josecarlos_182_6@hotmail.com", // REQUERIDO
//        "des_linkmap": "",
//        "nom_ciudad": "",
//        "nom_cliente": "JOSE CARLOS GALLEGOS CASTRO", // REQUERIDO
//        "nom_colonia": "",
//        "nom_estado": "",
//        "num_codigopostal": "",
//        "num_ext": "",
//        "num_int": "",
//        "num_telefono": 6672048064 //REQUERIDO
//        }, // DATOS DEL PEDIDO, SI ES REQUERIDO
//        "datostarjeta": {
//        "des_mesanio": "",
//        "nom_titular": "",
//        "num_cvv2": "",
//        "num_tarjeta": ""
//        }, // TODAVIA NO SE CHECA EL PAGO CON TARJETA, APENAS TRABAJAREMOS CON EL BANCO
//        "productos": [ // ARRAY CON PRODUCTOS DE PEDIDO SI ES REQUERIDO
//        {
//        "des_producto": "",
//        "idu_agrupador": "1",
//        "idu_tipoproducto": "1",
//        "imp_producto": 95,
//        "nom_producto": "Champiñón",
//        "tamanios": [
//        {
//        "idu_producto": "1",
//        "idu_tamanio": "1",
//        "imp_importe": 0,
//        "imp_precio": "95",
//        "imp_precioMod": 0,
//        "imp_total": 95,
//        "nom_tamanio": "Compañera",
//        "num_cantidad": 1
//        }
//        ]
//        },
//        {
//        "des_producto": "Chilorio",
//        "idu_agrupador": "2",
//        "idu_tipoproducto": "1",
//        "imp_producto": 238,
//        "nom_producto": "Sinaloense",
//        "tamanios": [
//        {
//        "idu_producto": "5",
//        "idu_tamanio": "2",
//        "imp_importe": 0,
//        "imp_precio": "119",
//        "imp_precioMod": 0,
//        "imp_total": 238,
//        "nom_tamanio": "Familiar",
//        "num_cantidad": 2
//        }
//        ]
//        }
//        ]
//        }