package com.tritonsoft.pizzeta.DTO;

import java.util.List;

/**
 * Created by ruben on 29/01/18.
 */

public class DTO_Pizza {
    public int nIdPizza;
    public String cNombrePizza;
    public String cDescripcion;
    public double nPrecio;
    public List<String>lstIngredientes;

    public DTO_Pizza(int nIdPizza, String cNombrePizza, String cDescripcion, double nPrecio, List<String> lstIngredientes) {
        this.nIdPizza = nIdPizza;
        this.cNombrePizza = cNombrePizza;
        this.cDescripcion = cDescripcion;
        this.nPrecio = nPrecio;
        this.lstIngredientes = lstIngredientes;
    }

    public int getnIdPizza() {
        return nIdPizza;
    }

    public void setnIdPizza(int nIdPizza) {
        this.nIdPizza = nIdPizza;
    }

    public String getcNombrePizza() {
        return cNombrePizza;
    }

    public void setcNombrePizza(String cNombrePizza) {
        this.cNombrePizza = cNombrePizza;
    }

    public String getcDescripcion() {
        return cDescripcion;
    }

    public void setcDescripcion(String cDescripcion) {
        this.cDescripcion = cDescripcion;
    }

    public double getnPrecio() {
        return nPrecio;
    }

    public void setnPrecio(double nPrecio) {
        this.nPrecio = nPrecio;
    }

    public List<String> getLstIngredientes() {
        return lstIngredientes;
    }

    public void setLstIngredientes(List<String> lstIngredientes) {
        this.lstIngredientes = lstIngredientes;
    }
}
