package com.tritonsoft.pizzeta.DTO;

import android.location.Address;

public class DTO_Domicilio {
    public String cTitulo;
    public String cNumeroInt;
    public String cNumeroExt;
    public String cDireccion;
    public String cp;
    public double lat;
    public double lng;
    public boolean bActivo;

    public DTO_Domicilio(String cTitulo, String cNumeroInt, String cNumeroExt, String cDireccion, String cp, double lat, double lng, Address geo) {
        this.cTitulo = cTitulo;
        this.cNumeroInt = cNumeroInt;
        this.cNumeroExt = cNumeroExt;
        this.cDireccion = cDireccion;
        this.cp = cp;
        this.lat = lat;
        this.lng = lng;
        this.bActivo = false;
        this.geo = geo;
    }

    public DTO_Domicilio() {
        this.bActivo = false;
    }

    public Address geo;

    public String getcTitulo() {
        return cTitulo;
    }

    public void setcTitulo(String cTitulo) {
        this.cTitulo = cTitulo;
    }

    public String getcNumeroInt() {
        return cNumeroInt;
    }

    public void setcNumeroInt(String cNumeroInt) {
        this.cNumeroInt = cNumeroInt;
    }

    public String getcNumeroExt() {
        return cNumeroExt;
    }

    public void setcNumeroExt(String cNumeroExt) {
        this.cNumeroExt = cNumeroExt;
    }

    public String getcDireccion() {
        return cDireccion;
    }

    public void setcDireccion(String cDireccion) {
        this.cDireccion = cDireccion;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public boolean isbActivo() {
        return bActivo;
    }

    public void setbActivo(boolean bActivo) {
        this.bActivo = bActivo;
    }

    public Address getGeo() {
        return geo;
    }

    public void setGeo(Address geo) {
        this.geo = geo;
    }
}
