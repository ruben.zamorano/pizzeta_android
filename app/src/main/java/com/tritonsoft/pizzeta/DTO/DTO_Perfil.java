package com.tritonsoft.pizzeta.DTO;

public class DTO_Perfil {
    public String cTelefono;
    public String cMail;
    public String cNombres;

    public DTO_Perfil(String cTelefono, String cMail, String cNombres) {
        this.cTelefono = cTelefono;
        this.cMail = cMail;
        this.cNombres = cNombres;
    }

    public String getcTelefono() {
        return cTelefono;
    }

    public void setcTelefono(String cTelefono) {
        this.cTelefono = cTelefono;
    }

    public String getcMail() {
        return cMail;
    }

    public void setcMail(String cMail) {
        this.cMail = cMail;
    }

    public String getcNombres() {
        return cNombres;
    }

    public void setcNombres(String cNombres) {
        this.cNombres = cNombres;
    }
}
